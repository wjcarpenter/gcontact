# gcontact

This software gives you the ability to access your Google contacts via an LDAP interface.
By contacts information,
we don't mean the contact information of the calling user.
We mean information for that calling user's many Google contacts.
Those are the same records the user would see on an Android phone or in a Google web application.
For example, many email applications allow for configuring an LDAP server as an address book.
With this software, you can access the same contacts that you would on an Android phone.

The entire application consists of these conceptual components:

* A web application, implemented in PHP, that users interact with, to set up and manage their participation.
* An instance of MyVirtualDirectory, which provides infrastructure for handling LDAP queries.
* A local database of contact information retrieved via Google APIs.
* A Java package, gcontact, that manages the local database and interacts with Google APIs to keep it up-to-date.
* The Java package, in turn, has these conceptual parts:
  * A library called by MyVirtualDirectory as part of fulfilling LDAP requests.
  * A background refresher service that periodically checks for contact updates on the Google side.
  * A responder service that responds to HTTP requests from the PHP script.
  * A standalone application invoked from time to time by the PHP script (used when the HTTP listener is not available).


Although the components are complete, you will have to do a little bit of work to put it together.
You can find more details in the [INSTALL.md](INSTALL.md) file, but the essence is:

*  Somewhere, you must have a server that acts as an intermediary between your LDAP client and Google servers.
*  You must register your copy of this application with Google and obtain client credentials.
*  You (and any other users) must interactively give consent so that the application can access Google contacts.
*  A local copy of the users' contacts data is kept in a relational database (MySQL or similar).
*  When your LDAP client does a lookup, the local copy will be refreshed if it is older than a configurable amount of time.
*  LDAP results are returned in a format that is expected by most applications using LDAP lookups.

Though the typical setup is a server somewhere, 
it would be possible to set this up on a personal machine 
as long as it is able to run the necessary components (PHP, Java, MySQL). 
In that case, you would just be connecting to the LDAP server as "localhost".
The main difficulty with doing that is that the initial OAuth2 handshake for consent
requires a redirect URI, and it might be tricky to organize that on a personal machine.

> ___The privacy of users' data depends heavily on the confidentiality of the database passwords.
> That, in turn, depends heavily on locking down access to the configuration files which
> contain those passwords. Both the configuration file for the gcontact Java application and the
> configuration file for MyVirtualDirectory application should have the most restricted
> permissions possible. The configuration file for the PHP script contains a "proof secret"
> used when it communicates with the Java application. That's less sensitive than the database
> passwords, but it should be similarly protected.___

The repository for this is at https://gitlab.com/wjcarpenter/gcontact.
I am interested in any feedback, which you can provide by opening an issue on the project page.
I am especially interested if you know or suspect a security vulnerability.

Release numbering follows the semantic versioning guidelines (https://semver.org/).
