The LDAP mechanics are handled by an open source project called MyVirtualDirectory,
https://github.com/TremoloSecurity/MyVirtualDirectory, aka MyVD. Some of the customizations
require MyVD plugins (which MyVD calls "inserts") that can be found at
https://gitlab.com/wjcarpenter/myvd-custom-inserts.

The file myvd.conf is a configuration file for MyVirtualDirectory suitable for use
with gcontact. I have included many comments in the config file to illustrate how
things work. You may want to tweak some things (like the various namespaces). You
will definitely have to supply database credentials, and you will probably want to
make a few other changes. myvd.conf is read as a Java properties file, so be careful
of your syntax as you edit.

Of course, if you are already using MyVD for something else, you will have to blend
these settings in with your current configuration. You should consult MyVD documentation
for guidance on that.
