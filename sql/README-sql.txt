You should be able to import this schema directly if you are using MySQL. However,
there are some statements near the end that pertain specifically to PhpMyAdmin. 
If you are not using PMA, you probably won't have those tables, and those inserts
will fail. You can simply remove the "pma" references before you use the schema.
They are fairly obvious and easy to find in the file.

If you are not using MySQL, you will have to translate this into your DBs
dialect. (If you do that, I would be interested in receiving a pull request with
your changes.) The logic in the applications depends on the cascade-delete 
behavior, so don't overlook those constraints in your translation. 

For convenience, there is one file "gcontact" that contains the schema for
the entire database, and there is a separate (redundant) file for each table.
The individual table files are more convenient when upgrading.
