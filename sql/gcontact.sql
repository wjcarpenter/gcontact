-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2020 at 04:28 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gcontact`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.',
  `display_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `given_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `family_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `canonical_form` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_bytes_b64` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `contacts`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `local_id`
--       `identities` -> `local_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_groups`
--

CREATE TABLE `contact_groups` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name, scoped to a Google account',
  `contact_group_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_group_formatted_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_group_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `contact_groups`:
--   `contact_group_id`
--       `contact_group_members` -> `contact_group_id`
--   `local_id`
--       `identities` -> `local_id`
--   `local_id`
--       `identities` -> `local_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_group_members`
--

CREATE TABLE `contact_group_members` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name, scoped to a Google account',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name, must also be listed in the contacts table',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `contact_group_members`:
--   `contact_group_id`
--       `contact_groups` -> `contact_group_id`
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `display_names`
--

CREATE TABLE `display_names` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `is_primary` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `prefix` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `given_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `family_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suffix` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `display_names`:
--   `contact_id`
--       `contacts` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_addresses`
--

CREATE TABLE `email_addresses` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `is_primary` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `email_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `email_addresses`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `identities`
--

CREATE TABLE `identities` (
  `local_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `google_token_blob` varchar(10000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Used for Google API calls; mostly treated opaquely',
  `contacts_sync_token` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For incremental refreshes, Google uses this to know what changes to send',
  `contact_groups_sync_token` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For incremental refreshes, Google uses this to know what changes to send',
  `token_timestamp` datetime DEFAULT NULL COMMENT 'The token blob needs occasional refreshing. This is a timestamp from that last refresh. That''s unrelated to when contact data was refreshed.',
  `fetch_timestamp` datetime DEFAULT NULL COMMENT 'Timestamp for when we last refreshed contact data, whether incremental or full refresh.',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `random` int(11) NOT NULL COMMENT 'random integer used for Refresher sorting',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `identities`:
--   `local_id`
--       `contacts` -> `local_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `is_current` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `is_primary` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `organizations`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `phone_numbers`
--

CREATE TABLE `phone_numbers` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `is_primary` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `canonical_form` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `phone_numbers`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `is_primary` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `is_default` tinyint(1) NOT NULL COMMENT 'Is this a default photo or user-provided?',
  `photo_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The URL provided for a Google "photo" object',
  `content_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'content-type, possibly after format conversion',
  `original_content_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Content-type returned when the photo-url is resolved, before any conversion',
  `photo_bytes_b64` text COLLATE utf8_unicode_ci COMMENT 'the bytes of the photo image, encoded as base64',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `photos`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE `shares` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user sharing the contact group',
  `share_to` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'share the contact group with this user',
  `contact_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'the contact group to share, owned by local_id',
  `share_to_opt_in` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'has the shared-to user opted in for this share?',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `shares`:
--   `contact_group_id`
--       `contact_groups` -> `contact_group_id`
--   `local_id`
--       `identities` -> `local_id`
--   `share_to`
--       `identities` -> `local_id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`ser`),
  ADD UNIQUE KEY `contact_id` (`contact_id`),
  ADD KEY `local_id` (`local_id`);

--
-- Indexes for table `contact_groups`
--
ALTER TABLE `contact_groups`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `contact_local_id` (`local_id`),
  ADD KEY `contact_group_id` (`contact_group_id`) USING BTREE,
  ADD KEY `local_id` (`local_id`);

--
-- Indexes for table `contact_group_members`
--
ALTER TABLE `contact_group_members`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `contact_group_id` (`contact_group_id`),
  ADD KEY `contact_id` (`contact_id`);

--
-- Indexes for table `display_names`
--
ALTER TABLE `display_names`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `local_id` (`local_id`);

--
-- Indexes for table `email_addresses`
--
ALTER TABLE `email_addresses`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `email_address` (`email_address`);

--
-- Indexes for table `identities`
--
ALTER TABLE `identities`
  ADD PRIMARY KEY (`local_id`),
  ADD UNIQUE KEY `ser` (`ser`) USING BTREE,
  ADD KEY `local_id` (`local_id`),
  ADD KEY `Random` (`random`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `contact_id` (`contact_id`);

--
-- Indexes for table `phone_numbers`
--
ALTER TABLE `phone_numbers`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `phone_number` (`phone_number`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `contact_id` (`contact_id`);

--
-- Indexes for table `shares`
--
ALTER TABLE `shares`
  ADD PRIMARY KEY (`ser`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `share_to` (`share_to`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `contact_groups`
--
ALTER TABLE `contact_groups`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `contact_group_members`
--
ALTER TABLE `contact_group_members`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `display_names`
--
ALTER TABLE `display_names`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `email_addresses`
--
ALTER TABLE `email_addresses`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `identities`
--
ALTER TABLE `identities`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `phone_numbers`
--
ALTER TABLE `phone_numbers`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';
--
-- AUTO_INCREMENT for table `shares`
--
ALTER TABLE `shares`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contact_local_id_identities` FOREIGN KEY (`local_id`) REFERENCES `identities` (`local_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact_groups`
--
ALTER TABLE `contact_groups`
  ADD CONSTRAINT `contact_groups_local_id_identities` FOREIGN KEY (`local_id`) REFERENCES `identities` (`local_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact_group_members`
--
ALTER TABLE `contact_group_members`
  ADD CONSTRAINT `contact_group_members_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `display_names`
--
ALTER TABLE `display_names`
  ADD CONSTRAINT `display_names_contact_id_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `email_addresses`
--
ALTER TABLE `email_addresses`
  ADD CONSTRAINT `email_addresses_contact_id_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_contact_id_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phone_numbers`
--
ALTER TABLE `phone_numbers`
  ADD CONSTRAINT `phone_numbers_contact_id_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table contacts
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'contacts', '{\"sorted_col\":\"`contacts`.`ser`  DESC\"}', '2020-03-08 02:34:02');

--
-- Metadata for table contact_groups
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'contact_groups', '{\"sorted_col\":\"`contact_groups`.`local_id` ASC\"}', '2020-01-20 04:00:26');

--
-- Metadata for table contact_group_members
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'contact_group_members', '{\"sorted_col\":\"`contact_group_id` ASC\"}', '2020-01-14 00:01:01');

--
-- Metadata for table display_names
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'display_names', '{\"sorted_col\":\"`display_names`.`display_name` ASC\"}', '2020-01-15 00:19:49');

--
-- Metadata for table email_addresses
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'email_addresses', '{\"sorted_col\":\"`email_addresses`.`email_address` ASC\"}', '2020-03-03 03:19:11');

--
-- Metadata for table identities
--

--
-- Metadata for table organizations
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'organizations', '{\"sorted_col\":\"`organizations`.`name` ASC\"}', '2020-01-11 22:38:20');

--
-- Metadata for table phone_numbers
--

--
-- Metadata for table photos
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'photos', '{\"sorted_col\":\"`photos`.`local_id`  DESC\"}', '2020-03-09 01:55:42');

--
-- Metadata for table shares
--

--
-- Metadata for database gcontact
--

--
-- Dumping data for table `pma__relation`
--

INSERT INTO `pma__relation` (`master_db`, `master_table`, `master_field`, `foreign_db`, `foreign_table`, `foreign_field`) VALUES
('gcontact', 'contact_group_members', 'contact_group_id', 'gcontact', 'contact_groups', 'contact_group_id'),
('gcontact', 'contact_group_members', 'contact_id', 'gcontact', 'display_names', 'contact_id'),
('gcontact', 'contact_group_members', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'contact_groups', 'contact_group_id', 'gcontact', 'contact_group_members', 'contact_group_id'),
('gcontact', 'contact_groups', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'contacts', 'contact_id', 'gcontact', 'display_names', 'contact_id'),
('gcontact', 'contacts', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'display_names', 'contact_id', 'gcontact', 'contacts', 'contact_id'),
('gcontact', 'display_names', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'email_addresses', 'contact_id', 'gcontact', 'display_names', 'contact_id'),
('gcontact', 'email_addresses', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'identities', 'local_id', 'gcontact', 'contacts', 'local_id'),
('gcontact', 'organizations', 'contact_id', 'gcontact', 'display_names', 'contact_id'),
('gcontact', 'organizations', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'phone_numbers', 'contact_id', 'gcontact', 'display_names', 'contact_id'),
('gcontact', 'phone_numbers', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'photos', 'contact_id', 'gcontact', 'display_names', 'contact_id'),
('gcontact', 'photos', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'shares', 'contact_group_id', 'gcontact', 'contact_groups', 'contact_group_id'),
('gcontact', 'shares', 'local_id', 'gcontact', 'identities', 'local_id'),
('gcontact', 'shares', 'share_to', 'gcontact', 'identities', 'local_id');

--
-- Dumping data for table `pma__central_columns`
--

INSERT INTO `pma__central_columns` (`db_name`, `col_name`, `col_type`, `col_length`, `col_collation`, `col_isNull`, `col_extra`, `col_default`) VALUES
('gcontact', 'canonical_form', 'varchar', '100', 'utf8_unicode_ci', 1, ',', ''),
('gcontact', 'contact_group_id', 'varchar', '100', 'utf8_unicode_ci', 0, ',', ''),
('gcontact', 'contact_id', 'varchar', '100', 'utf8_unicode_ci', 0, ',', ''),
('gcontact', 'department', 'varchar', '100', 'utf8_unicode_ci', 1, ',', ''),
('gcontact', 'display_name', 'varchar', '100', 'utf8_unicode_ci', 0, ',', ''),
('gcontact', 'email_address', 'varchar', '100', 'utf8_unicode_ci', 0, ',', ''),
('gcontact', 'family_name', 'varchar', '100', 'utf8_unicode_ci', 1, ',', ''),
('gcontact', 'given_name', 'varchar', '100', 'utf8_unicode_ci', 1, ',', ''),
('gcontact', 'local_id', 'varchar', '100', 'utf8_unicode_ci', 0, ',', ''),
('gcontact', 'phone_number', 'varchar', '100', 'utf8_unicode_ci', 0, ',', ''),
('gcontact', 'photo_bytes_b64', 'text', '', 'utf8_unicode_ci', 1, ',', ''),
('gcontact', 'ser', 'int', '11', '', 0, 'auto_increment,', ''),
('gcontact', 'timestamp', 'datetime', '', '', 0, 'on update CURRENT_TIMESTAMP,', 'CURRENT_TIMESTAMP');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
