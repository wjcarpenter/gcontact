-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2020 at 01:10 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gcontact`
--

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE IF NOT EXISTS `shares` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user sharing the contact group',
  `share_to` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'share the contact group with this user',
  `contact_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'the contact group to share, owned by local_id',
  `share_to_opt_in` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'has the shared-to user opted in for this share?',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ser` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ser`),
  KEY `local_id` (`local_id`),
  KEY `share_to` (`share_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `shares`:
--   `contact_group_id`
--       `contact_groups` -> `contact_group_id`
--   `local_id`
--       `identities` -> `local_id`
--   `share_to`
--       `identities` -> `local_id`
--


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table shares
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
