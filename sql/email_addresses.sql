-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2020 at 01:07 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gcontact`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_addresses`
--

CREATE TABLE IF NOT EXISTS `email_addresses` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `is_primary` tinyint(1) NOT NULL COMMENT 'Google-provided flag value',
  `email_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.',
  PRIMARY KEY (`ser`),
  KEY `local_id` (`local_id`),
  KEY `contact_id` (`contact_id`),
  KEY `email_address` (`email_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `email_addresses`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `contact_id`
--       `contacts` -> `contact_id`
--

--
-- Constraints for dumped tables
--

--
-- Constraints for table `email_addresses`
--
ALTER TABLE `email_addresses`
  ADD CONSTRAINT `email_addresses_contact_id_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table email_addresses
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'email_addresses', '{\"sorted_col\":\"`email_addresses`.`email_address` ASC\"}', '2020-03-03 03:19:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
