-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2020 at 01:06 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gcontact`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_groups`
--

CREATE TABLE IF NOT EXISTS `contact_groups` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name, scoped to a Google account',
  `contact_group_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_group_formatted_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_group_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.',
  PRIMARY KEY (`ser`),
  KEY `contact_local_id` (`local_id`),
  KEY `contact_group_id` (`contact_group_id`) USING BTREE,
  KEY `local_id` (`local_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `contact_groups`:
--   `contact_group_id`
--       `contact_group_members` -> `contact_group_id`
--   `local_id`
--       `identities` -> `local_id`
--   `local_id`
--       `identities` -> `local_id`
--

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_groups`
--
ALTER TABLE `contact_groups`
  ADD CONSTRAINT `contact_groups_local_id_identities` FOREIGN KEY (`local_id`) REFERENCES `identities` (`local_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table contact_groups
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'contact_groups', '{\"sorted_col\":\"`contact_groups`.`local_id` ASC\"}', '2020-01-20 04:00:26');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
