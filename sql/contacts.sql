-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2020 at 01:03 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gcontact`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `local_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `contact_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Google resource name',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.',
  `display_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `given_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `family_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `canonical_form` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_bytes_b64` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ser`),
  UNIQUE KEY `contact_id` (`contact_id`),
  KEY `local_id` (`local_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `contacts`:
--   `contact_id`
--       `display_names` -> `contact_id`
--   `local_id`
--       `identities` -> `local_id`
--   `local_id`
--       `identities` -> `local_id`
--

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contact_local_id_identities` FOREIGN KEY (`local_id`) REFERENCES `identities` (`local_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table contacts
--

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'gcontact', 'contacts', '{\"sorted_col\":\"`contacts`.`ser`  DESC\"}', '2020-03-08 02:34:02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
