-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2020 at 04:29 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gcontact`
--

-- --------------------------------------------------------

--
-- Table structure for table `identities`
--

CREATE TABLE `identities` (
  `local_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'The local identity for the user',
  `google_token_blob` varchar(10000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Used for Google API calls; mostly treated opaquely',
  `contacts_sync_token` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For incremental refreshes, Google uses this to know what changes to send',
  `contact_groups_sync_token` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For incremental refreshes, Google uses this to know what changes to send',
  `token_timestamp` datetime DEFAULT NULL COMMENT 'The token blob needs occasional refreshing. This is a timestamp from that last refresh. That''s unrelated to when contact data was refreshed.',
  `fetch_timestamp` datetime DEFAULT NULL COMMENT 'Timestamp for when we last refreshed contact data, whether incremental or full refresh.',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Automatically updated timestamp for this record.',
  `random` int(11) NOT NULL COMMENT 'random integer used for Refresher sorting',
  `ser` int(11) NOT NULL COMMENT 'Simply for uniqueness.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `identities`:
--   `local_id`
--       `contacts` -> `local_id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `identities`
--
ALTER TABLE `identities`
  ADD PRIMARY KEY (`local_id`),
  ADD UNIQUE KEY `ser` (`ser`) USING BTREE,
  ADD KEY `local_id` (`local_id`),
  ADD KEY `Random` (`random`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `identities`
--
ALTER TABLE `identities`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Simply for uniqueness.';

--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table identities
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
