You need the PHP piece to give users the opportunity to interactively consent
to gcontact's later access to their contact information. It's up to you to 
arrange for this PHP page to be displayed so the user can interact with it.

YOU CANNOT USE THE PHP PIECE WITHOUT AT LEAST A LITTLE BIT OF CONFIGURATION
CUSTOMIZATION.

All of the configuration items are explicitly documented at
the top of gcontact-auth.php. You apply your customizations to the companion
gcontact-auth.cfg. Anything you don't explicitly configure in gcontact-auth.cfg
will take the default value given in gcontact-auth.php.

If you are already running PHP things, that will be easy. If you would prefer
to use some non-PHP framework to obtain the user's interactive OAuth2 consent,
you will have to provide that yourself, either by porting this PHP logic or
by starting from scratch. In either case, I would be interested in receiving
either a pull request with those changes or a pointer to them.

The implementation of the OAuth2 pieces uses Google PHP client libraries
and the corresponding documentation. However, some details are tempered by
my observations of what I actually saw when interacting with the Google 
APIs. Google provides a lot of documentation for its APIs, but it's a
bit scattered and not all of it is crystal clear. It's entirely possible that
I have misunderstood some of it, and I don't mind having it pointed out
to me when that is the case.

If you have worked with OAuth2 before, you will find a lot of familiar
territory. If you have not, you may find the road a little challenging.
OAuth2 is not too tricky, but there is a lot of jargon and a few concepts
that you may not have encountered before. Using respected 3rd party libraries
is the best way to go. You might find the document tree starting here to
be helpful: https://developers.google.com/identity
