<?php
// wondering where this came from?
// https://gitlab.com/wjcarpenter/gcontact
// See LICENSE file for license (BSD 2-Clause simplified License)

/****************************************************************************/
/* BEGINNING OF CONFIGURATION ITEMS.                                        */
/****************************************************************************/

// I run this embedded within a web CMS framework, so I'm already
// "included", and global variables need some extra help.

// These config items are roughly in the order of likelihood that you
// will need to customize them, but you should look them all over to
// make sure they are all good for you.

// A simple way to make customizations without changing this file is
// to put a companion file in the same directory, but with the ".php"
// extension changed to ".cfg" (e.g., "gcontact-auth.cfg"). That file
// will automatically be loaded just after all of these configuration
// items. My advice is to copy this section (but not the rest of the
// file) to that companion ".cfg" file. Then edit to suit. It is not
// an error if that file does not exist, but the defaults are unlikely
// to work for you completely as-is. (Don't forget to include the php
// stuff at top and bottom of your copy so it's not treated as a very
// long text literal.)

// Text appearing in the UI is subject to customization to replace
// placeholders with configuration values. See the function
// customizeString(), near the bottom, for the replacements that
// can be made. The idea is to introduce a layer of abstraction so
// that chunks of text can be used in a form-letter sort of way.

// Since your ".cfg" file contains a shared secret value, you probably
// want to lock down permissions so that it can only be ready by your
// web server. And definitely put it outside of your document root so
// it can't be accessed directly. Check the INSATLL document for
// suggestions about this.

  global $gcfConfig;
  $gcfConfig = [];

  /**
   * Google APIs for PHP are installed via "compose" and use the
   * "compose" autoload mechanism. To kick that all off, we need a
   * pointer to autoload.php, which can be wherever you decide to put
   * it.
   */
  $gcfConfig["autoload_php"] = '/some/path/vendor/autoload.php';

  /**
   * This is a secret shared between the PHP page and the Java
   * application so that the Java application trusts its caller. You
   * must set the same value in the configuration for the Java
   * program. For best security, change this from the default. It can
   * be any string, but sticking to ASCII probably gives the least
   * risk of incompatibility.
   */
  $gcfConfig["proof_secret"] = "YOU SHOULD CERTAINLY CHANGE THIS";

  /**
   * This word or phrase is substituted in various displayed messages
   * to make it more comforting :-) to the users. It is not used for
   * any configuration or other functional purpose, though it is
   * passed as part of the application name in the Google REST API.
   *
   * Replaces LOCAL_SITE_NAME in messages.
   */
  $gcfConfig["local_site_name"] = 'My Lovely Site';

  /**
   * You can call this feature anything you want, and that will be
   * used throughout the text on the web page. If you want to include
   * your site name in the feature name (recommended), you can either
   * include it literally, or you can include the placeholder
   * LOCAL_SITE_NAME.
   *
   * Replaces LOCAL_FEATURE_NAME in messages.
   */
  $gcfConfig["local_feature_name"] = "LOCAL_SITE_NAME Contacts Fetcher";

  /**
   * This is information that users will need for configuring clients.
   * It's displayed on the LDAP how-to tab and is specific to your
   * site.  Items are presented verbatim, except perhaps wrapped in a
   * "div" tag with some style info. Each item is treated as text,
   * including things like port numbers. Most of this info has to
   * match up with configuration for MyVirtualDirectory. Items are
   * themselves arrays of text/value/comment triplets. All items are
   * optional, and the names of the top-level items only matter in
   * case you want to overlay them.
   */
  $gcfConfig["ldap_client_config_info"] =
  [
    "table_header" =>
    [
      "text" => "LDAP client configuration for LOCAL_FEATURE_NAME",
      // "value" is not used for this element; this is the header for the entire table
      "comment" => NULL,    //  this "comment" is rendered below the table of LDAP config items
    ],
    "server_name" =>
    [
      "text" => "Name of the LDAP server",
      "value" => "ldap.example.com",
    ],
    "secure_port" =>
    [
      "text" => "Secure LDAP port number",
      "value" => "636 (the default ldaps port)",
    ],
    "nonsecure_port" =>
    [
      "text" => "Non-secure LDAP port number",
      "value" => "not available",
    ],
    "base_dn_for_contacts" =>
    [
      "text" => "Base DN for contacts",
      "value" => "ou=contacts,o=gcontact",
    ],
    "base_dn_for_groups" =>
    [
      "text" => "Base DN for contact groups",
      "value" => "ou=contactgroups,o=gcontact",
    ],
    "bind_dn_sample" =>
    [
      "text" => "Bind DN example",
      "value" => "mail=<i>someuser@example.com</i>,ou=users,dc=example,dc=com",
      "comment" => "where <i>someuser@example.com</i> is your usual user account at LOCAL_SITE_NAME",
    ],
    "bind_password" =>
    [
      "text" => "Bind password",
      "value" => "********",
      "comment" => "Use the same password that you use for other things at LOCAL_SITE_NAME",
    ],
    "contact_class" =>
    [
      "text" => "LDAP object class for contacts",
      "value" => "inetOrgPerson",
    ],
    "group_class" =>
    [
      "text" => "LDAP object class for contact groups",
      "value" => "groupOfUniqueNames",
    ],
  ];

  /**
   * To use Google APIs, you have to register your application (see
   * Google developer help for details). You will then be able to
   * download a small JSON file containing the client ID and secret.
   *
   * If you don't dabble in this area much, it can be a little
   * confusing to understand what you need. You are looking for a
   * download with contents that look like this (possibly with
   * different JSON formatting):
   *
   * {
   *   "installed": {
   *     "client_id": "some-really-long-string",
   *     "client_secret": "some-other-string"
   *   }
   * }
   */
  $gcfConfig["credentials_json_file"] = '/some/path/gcontact-client-secrets.json';

  /**
   * This value will be used as the redirection URI in the OAuth2
   * conversation with Google, where the user provides consent. It
   * must exactly match a redirection URI configured in your project
   * settings on the Google side. That includes any path and query
   * parts.
   */
  $gcfConfig["oauth2_redirect_uri"] = 'https://www.example.com/gcontact';

  /**
   * This application name is used when redirecting to Google for
   * authorization.
   */
  $gcfConfig["oauth2_application_name"] = 'LOCAL_SITE_NAME Google contacts fetcher';

  /*
   * Although this is a ridiculously long PHP script, the main logic of
   * the feature is implemented in Java. For a number of things,
   * this script needs to call that Java application to do some work.
   * That call can be made as an HTTP request to a running service,
   * or it can be made by invoking the Java application from the command line.
   *
   * The preferred method is via HTTP because it's more efficient. The
   * server that responds is already running as an offshoot of the
   * MyVD service that is already running. The script will first
   * try HTTP and will automatically fall back to the pipe if HTTP
   * doesn't work. We recommend that you configure both so you don't
   * have to think about it.
   */

  /**
   * This set of configs is for the HTTP connection. It is an array
   * with the values shown as defaults. The values you configure
   * must agree with similar items in the Java configuration properties.
   * FYI, we use GuzzleHttp to make the connections.
   *
   * HTTP calls can use DIGEST authentication, which we recommend.
   * If you turn that off for some reason, it will fall back to using
   * the same simple proof mechanism used by piped calls.
   */
   $gcfConfig["java_companion_http"] =
   [
     "hostname" => "localhost",       // if you are using TLS, does the certificate name match?
     "port" => 7373,                  // specify the port even if you want the default of 80 or 443
     "path" => "/gcontact/actions",   // must start with "/"

     "digest_auth" => TRUE,           // use DIGEST authentication (better) instead of simple proof
     "use_tls" => FALSE,              // if you are not using localhost, we recommend using TLS
     "validate_cert" => TRUE,         // Should the server certificate be validated for TLS?
                                      // If you need to use a custom certificate, set this to
                                      // the path of that certificate authority file.
   ];

  /**
   * This config item is for the command line case.
   * The value of this variable should be an invocation of that Java
   * application in your local environment.
   *
   * If you are using the unpacked distribution TAR or ZIP from the
   * git repository, you can just invoke the "gcontact" or "gcontact.bat"
   * script, which takes care of setting up the Java classpath and
   * other details. (Those handy scripts are provided courtesy of the
   * gradle "distribution" plugin.) Use the full path to the script,
   * and include the Java config properties file path as shown in
   * the example.
   *
   * For backward compatibility reasons, the value may contain
   * a single "%s", which will be ignored and removed. We recommend
   * not supplying that "%s" in case the logic changes in the future.
   */
  $gcfConfig["java_companion_command_line"] = "/some/path/gcontact --config /path/to/gcontact.properties";


  /**
   * We want to associate the Google identity with the user's local
   * identity. You must provide a PHP function to tell us that *unique*
   * local identity. If it returns NULL, the user is instructed to
   * login before they can go further. You definitely want to
   * identify the user in some trustworthy way, but that's up to you.
   *
   * This function is called often, under the assumption that it
   * is relatively cheap. If any caching is needed to make it actually
   * be cheap, that should be handled inside the function or some
   * lower layer. In other words, it's up to you. We don't cache the
   * value because the user's session (or whatever the function
   * consults) might time out during user "think time".
   *
   * Put the name of that function in this config item as a string.
   *
   * In my environment, I integrate this with Joomla, which is already
   * customized for my local authentication scheme. That makes it easy
   * to get the local user identity. If you happen to be inside
   * Joomla, you can probably use my function as-is, but no promises.
   *
   * function getLocalIdentity()
   * {
   *     $user = JFactory::getUser();
   *     if ($user->guest) {
   *         return NULL;
   *     } else {
   *       return $user->username;
   *     }
   * }
   */
  $gcfConfig["local_identity_function"] = NULL;

  /**
   * I can't know how you are invoking this PHP script, whether you
   * can use external styles, etc, so I have resorted to using inline
   * styles for several special items.  This is far from complete, but
   * it should give you a way to render the things you really care
   * about in a way that is different from what I have chosen.
   *
   * Any item whose style is NULL will be rendered without an inline
   * style attribute.  You can use quoted items in your style
   * attribute values, but you must choose to use either single or
   * double quotes in any particular item. If you make a mistake and
   * use both, the style attribute is omitted completely.
   */
  $gcfConfig["cssStyles"] =
  [
    // used for the text of the optional ack footer
    "acknowledgment_footer" =>
      "font-size:80%;" .
      "font-style:italic;" .
      "text-align:center;" .
      "",

    // For the message to tell a user they have to login first
    "not_logged_in" =>
      "font-style:italic;" .
      "font-weight:bold;" .
      "text-align:center;" .
      "",

    // When the user does a refresh, the results are reported in a <div> section
    // with this style. It's a brief intro and then a <pre> section for
    // the actual results.
    "refresh_result_div" =>
      "border-style:dotted;" .
      "",

    // and this is the style for that <pre> section of refresh results itself
    "refresh_result_pre" => NULL,

    // when showing the table sharing items, these styles are used
    "sharing_th" =>
      "border:1px solid #ddd;" .
      "padding-left:5px;" .
      "padding-right:5px;" .
      "text-align:left;" .
      "",

    // when showing the table sharing items, these styles are used
    "sharing_td" =>
      "border-bottom:1px solid #ddd;" .
      "padding-left:5px;" .
      "padding-right:5px;" .
      "",

    // when showing the table of LDAP config items, these styles are used
    "ldap_config_th" =>
      "border:1px solid #ddd;" .
      "padding-left:5px;" .
      "padding-right:5px;" .
      "text-align:left;" .
      "",

    // when showing the table of LDAP config items, these styles are used
    "ldap_config_td" =>
      "border-bottom:1px solid #ddd;" .
      "padding-left:5px;" .
      "padding-right:5px;" .
      "",

    // When an error message displays an incident ID and a soothing message, this
    // style is used. It should stand out pretty well.
    "incident" =>
      "background-color:powderblue;" .
      "color:darkorange;" .
      "font-size:115%;" .
      "font-style:italic;" .
      "",

    // various buttons
    "button" => NULL,

    // section headers titles
    "section_header" =>
      "font-weight:bold;" .
      "text-align:left;" .
      "",

    // styling for a link to a section in text
    "section_header_reference" =>
      "cursor:pointer;" .
      "font-style:italic;" .
      "",

    // The next few items are for fancy schmancy tabbed sections
    // This is for the <div> that surrounds the complete set of tabs
    "section_tabs_div" =>
      "background-color: inherit;" .
      "border-style:none;" .
      "overflow:auto;" .
      "",

    // This one is "normal", when the tab can be selected (class: "sectionlink")
    "section_tab_nonselected" =>
      "background-color:inherit;" .
      "border-style:none solid;" .
      "cursor:pointer;" .  // only for "available", not for "selected" or "disabled"
      "float:left;" .
      "font-family:inherit;" .
      "font-size:110%;" .
      "font-weight:normal;" .
      "padding:12px 12px;" .
      "transition:0.3s;" .
      "",

    // This one is "normal", when the tab actually is selected (class: "sectionlink-selected")
    "section_tab_selected" =>
      "background-color:inherit;" .
      "border:2px solid;" .
      "float:left;" .
      "font-family:inherit;" .
      "font-size:110%;" .
      "font-weight:bold;" .
      "font-style:italic;" .
      "padding:12px 12px;" .
      "",
  ];

  /**
   * After the Google consent screen, the user is redirected back (via
   * the redirectUrl) so that we can pick up the OAuth2 authorization
   * code. That's delivered via a URL query parameter, "code=". The
   * redirection also includes our own "state=" anti-forgery
   * token. There are also several other query parameters that clutter
   * up the URL string in the browser address bar.
   *
   * It is the presence of the "code=" and "state=" parameters that
   * lets this script know what's going on. After processing, we want
   * to get rid of those (and redirect back to ourselves).
   *
   * If this variable is TRUE, all query parameters are discarded. If
   * it's FALSE, only the "code=" and "state=" query parameters are
   * discarded.
   */
  $gcfConfig["nuke_all_query_parameters"] = TRUE;

  /**
   * This piece of text is displayed at the very bottom of the
   * page. If you don't want to show it, set it to null.  It's
   * rendered within a DIV block and uses style
   * "acknowledgment_footer".
   */
  $gcfConfig["acknowledgment_footer"] = <<<END
LOCAL_FEATURE_NAME is powered in whole or in part by software from the open source 
<a target='_blank' rel='noopener' href='https://gitlab.com/wjcarpenter/gcontact'>
gcontact
EXTERNAL_LINK_ICON
</a>
project. 
<br/>
Local operation of LOCAL_FEATURE_NAME is the responsibility of the LOCAL_SITE_NAME support contacts.
END;

  /**
   * Various strings show up in isolation in the UI. You can change these if you want.
   * They all get customizations.
   */
  $gcfConfig["strings"] =
  [
    /**
     * If the user is not currently authenticated, this message is shown
     * to them. If you have a URL for a login form, it would be good to
     * add it to this message.
     */
    "not_logged_in_message" => "You must log in to the LOCAL_SITE_NAME site before you can use " .
                               "LOCAL_FEATURE_NAME. Look for the Login item in the site menu, and " . "
                               then return to this page.",
    /**
     * If the user is sharing at least one contact group, we display a list of the shares.
     * Otherwise, we display this text.
     */
    "sharing_you_have_none_text" => "You are not currently sharing any of your contact groups.",

    "local_identity_is" =>             "Your LOCAL_SITE_NAME user identity is",
    "local_identity_unknown" =>        "Your LOCAL_SITE_NAME user identity is unknown until you are logged in at LOCAL_SITE_NAME.",
    "google_identity_is" =>            "Your Google identity is",
    "google_identity_unknown_auth" =>  "Your Google identity is unknown; see REF_SECTION_HEADER_CONSENT.",
    "google_identity_unknown_login" => "Your Google identity is unknown until you are logged in at LOCAL_SITE_NAME.",
    "google_resource_name_is" =>       "Your Google profile resource name is",
    "google_resource_name_explain" =>  "(You don't normally need to know your Google resource name. " .
                                       "It's an internal Google ID that stays the same, " .
                                       "even if you change your Google account email address. " .
                                       "It's also how we associate your Google identity with your LOCAL_SITE_NAME identity.)",

    "refresh_cant_local" =>   "You can't refresh your data until you are logged in at LOCAL_SITE_NAME.",
    "refresh_cant_google" =>  "You don't have an associated Google account, so there is no data to refresh yet.",
    "refresh_previous_was" => "The result of your previous manual refresh was:",

    "sharing_cant_local" =>   "We can't show your contact group sharing until you are logged in at LOCAL_SITE_NAME.",
    "sharing_cant_google" =>  "You don't have an associated Google account, so you have no contact groups to share.",
    "sharing_table_group" =>  "Contact group",
    "sharing_table_with" =>   "You shared with &hellip;",
    "sharing_table_by" =>     "Shared with you by &hellip;",
    "sharing_table_optin" =>  "Opt in?",
    "sharing_table_shadow" => "space-separated list of LOCAL_SITE_NAME users",

    "auth_cant_local" =>    "You can't do this step until you are logged in at LOCAL_SITE_NAME.",
    "auth_cant_google" =>   "You have already done the consent step.",
    "deauth_cant_local" =>  "You can't do this step until you are logged in at LOCAL_SITE_NAME.",
    "deauth_cant_google" => "You have not done the above consent step, so this step does not apply.",

    "incident" =>
        "An unexpected problem occurred and has been logged. " .
        "If this problem persists, please notify LOCAL_SITE_NAME support, " .
        "and mention your unique Incident ID.",
  ];

  /**
   * As the length of this display PHP page has grown, it's become
   * easier and easier for a user to get lost in it. These section
   * headers should provide a little bit of navigation help. They are
   * also anchors, so when referenced in other text, they'll be
   * clickable links. You can see examples of referencing them in some
   * of the default text defined elsewhere.
   *
   * I put the full structure of the "content" array here to establish
   * the ordering.  The names of the array elements don't actually
   * matter except if you want to override anything in your site
   * configuration. The names with "function" in them are a hint, but
   * the rendering logic actually looks to see if the value is a
   * function. If it is, it is simply called and the return value is
   * ignored. If it's not, it's a string to be rendered.
   *
   * Items in the "content" array can be:
   *   -- NULL, in which case they are ignored
   *   -- the name of a PHP function, in which case that function
   *      will be called with two arguments: (a) an array
   *      containing various poorly documented context items, and
   *      (b) a string naming the section. The function can either
   *      emit page content directly, or it can return a string
   *      that will be emitted by us, or it can do both.
   *   -- an array whose 0th element is the name of a PHP function,
   *      in which case that function will be called with three
   *      arguments: (a) and (b) as described just above, and (c)
   *      that array itself, from which the function can take
   *      additional information (it's used, for example, to call
   *      emitImage(). The function can either emit page content
   *      directly, or it can return a string that will be emitted
   *      by us, or it can do both.
   *   -- a string that will be emitted as page content
   *
   * The large content pieces are defined below and pushed into the
   * applicable NULL slots. Trying to do it all inline was too wacky
   * and made it easy to lose track of what went where.
   */
  $gcfConfig["section"] =
  [
    "overview" =>
    [
      "title" => "Overview",
      "content" =>
      [
        "svg-symbol-table" => NULL, // this isn't overview stuff, but it has to be emitted somewhere just once
        "text-1" => NULL,
        "clear-1" => "<div style='clear:both;display:table'></div>",
        "image-android" => ["emitImage", "image/android-contacts.jpg", "style='float:left;margin:15px'", "image/jpg"],
        "image-rc" => ["emitImage", "image/rc-contacts.jpg", "style='float:right;margin:15px'", "image/jpg"],
        "text-2" => NULL,
        "clear-2" => "<div style='clear:both;display:table'></div>",
        "text-3" => NULL,
      ],
    ],
    "consent" =>
    [
      "title" => "Consent on Google",
      "content" =>
      [
        "intro_auth" => NULL,
        "hr1" => "<hr/>",
        "specifics_auth_function" => "emitConsentAuthSpecifics",
        "hr2" => "<hr/>",
        "intro_deauth" => NULL,
        "specifics_deauth_function" => "emitConsentDeauthSpecifics",
      ],
    ],
    "identities" =>
    [
      "title" => "Your identities",
      "content" =>
      [
        "intro" => NULL,
        "hr1" => "<hr/>",
        "specifics_identities_function" => "emitIdentitiesSpecifics",
      ],
    ],
    "refresh" =>
    [
      "title" => "Refresh your data",
      "content" =>
      [
        "intro" => NULL,
        "hr1" => "<hr/>",
        "specifics_refresh_function" => "emitRefreshSpecifics",
      ],
    ],
    "sharing" =>
    [
      "title" => "Share contact groups",
      "content" =>
      [
        "intro" => NULL,
        "hr1" => "<hr/>",
        "specifics_sharing_function" => "emitSharingSpecifics",
        "hr2" => "<hr/>",
        "details" => NULL,
      ],
    ],
    "ldaphowto" =>
    [
      "title" => "Configure LDAP client apps",
      "content" =>
      [
        "intro" => NULL,
        "hr-1" => "<hr/>",
        "image-tb1" => ["emitImage", "image/tbird-cfg.jpg", "style='float:right;margin:15px'", "image/jpg"],
        "config_table_function" => "emitLdapConfigTable",
        "clear-1" => "<div style='clear:both;display:table'></div>", // prevents image overflowing
        "hr-2" => "<hr/>",
        "terminology" => NULL,
        "hr-3" => "<hr/>",
        "clients" => NULL,

        "hr-4" => "<hr/>",

        "image-rc" => ["emitImage", "image/rc-contacts.jpg", "style='float:left;margin:15px'", "image/jpg"],
        "clients-rc" => NULL,
        "clear-2" => "<div style='clear:both;display:table'></div>", // prevents image overflowing

        "hr-6" => "<hr/>",

        "image-tb2" => ["emitImage", "image/tbird-cfg.jpg", "style='float:right;margin:15px'", "image/jpg"],
        "clients-tbird" => NULL,
        "clear-3" => "<div style='clear:both;display:table'></div>", // prevents image overflowing

        "hr-5" => "<hr/>",


        "div-3a" => "<div>",
        "image-voip" => ["emitImage", "image/voip-cfg.jpg", "style='float:left;margin:15px'", "image/jpg"],
        "clients-voip" => NULL,
        "clear-4" => "<div style='clear:both;display:table'></div>", // prevents image overflowing
        "div-3b" => "</div>",
      ],
    ],
  ];

  /**
   * When buttons are rendered, there can be an optional "click here
   * to do whatever" message, which is the "click_message" element in
   * the button definition. The "text" element is what appears on the
   * button itself.  The URL associated with the button, alas, has to
   * come from code, so it isn't part of these button definitions.
   */
  $gcfConfig["button"] =
  [
    // Buttons to redirect to Google for authorization consent or to revoke consent as part of OAuth2.
    "auth" =>
    [
      "click_message" => "Click the button to redirect to Google to give consent for LOCAL_FEATURE_NAME:",
      "text" => "Go to Google to give consent",
    ],
    "deauth" =>
    [
      "click_message" => "Click the button to immediately revoke your Google consent for LOCAL_FEATURE_NAME:",
      "text" => "Revoke consent and forget my Google data",
    ],

    // Buttons to trigger a manual incremental or full refresh.
    "refresh" =>
    [
      "click_message" => "Click one of the buttons to manually refresh your Google contacts information:",
      "text" => "Refresh my Google data (incremental; recommended)",
    ],
    "restore" =>
    [
      "text" => "Refresh my Google data (full; takes more time)",
    ],

    // After an error or when displaying transient info, the user can clear it away with this button.
    "continue" =>
    [
      "click_message" => "Click here to clear the above message and continue:",
      "text" => "Continue",
    ],

    // These buttons are associated with the form in the sharing section
    "sharing_edit" =>
    [
      "text" => "Edit contact group sharing",
    ],
    "sharing_save" =>
    [
      "text" => "Save contact group sharing updates",
    ],
    "sharing_reset" =>
    [
      "text" => "Reset form, discard changes",
    ],
    "sharing_cancel" =>
    [
      "text" => "Cancel editing",
    ],
  ];

// SVG icon taken from this blog article:
// https://medium.com/@svinkle/why-let-someone-know-when-a-link-opens-a-new-window-8699d20ed3b1
  $gcfConfig["section"]["overview"]["content"]["svg-symbol-table"] = <<<END

<svg version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: none;"
     xmlns:xlink="http://www.w3.org/1999/xlink" hidden>
    <symbol id="new-window" viewBox="0 0 24 24">
        <g transform="scale(0.0234375 0.0234375)"> 
            <path
                d="M598 128h298v298h-86v-152l-418 418-60-60 
                    418-418h-152v-86zM810 810v-298h86v298c0 46-40 
                    86-86 86h-596c-48 0-86-40-86-86v-596c0-46 
                    38-86 86-86h298v86h-298v596h596z">
            </path>
        </g>
    </symbol>
</svg>

END;

  $gcfConfig["section"]["overview"]["content"]["text-1"] = <<<END
Would you like to use your Google contacts as an address book for your
email program? That's simple with an email app on your Android phone
(as in the screenshot on the left, below)
or when using gmail, but LOCAL_SITE_NAME users can also do that with
many desktop or web mail applications (as in the screenshot on the right, below).
That's possible because we
provide a bridge between Google's contact protocols and the standard LDAP protocol. LDAP (
<a target='_blank' rel='noopener' href='https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol'>
Lightweight Directory Access Protocol
EXTERNAL_LINK_ICON
</a>
) is an industry standard for accessing address books, and it is an option for
many popular email programs. LDAP can also be used by other types of
applications. For example, some phone systems can use LDAP lookups to
supply caller names to go with CallerID numbers, or to make calls by
looking up names.
END;

  $gcfConfig["section"]["overview"]["content"]["text-2"] = <<<END
<p/>
The sections of this page that you are currently viewing is for setting up the
necessary access controls and permissions so that you can use
LOCAL_FEATURE_NAME. This is mostly a one-time process:
<ul>
<li>Log in to the LOCAL_SITE_NAME site</li>
<li>Give consent for LOCAL_SITE_NAME to access some of your Google data</li>
<li>Use your Google data in your email program or other application
(see the section REF_SECTION_HEADER_LDAPHOWTO)</li>
</ul>
<p/>
You will be authorizing LOCAL_SITE_NAME to access your Google
contacts, as well as your own profile details tied to your Google
account. (See the section REF_SECTION_HEADER_CONSENT.) We use the
profile details to display your Google identity on this page so that
you can be sure you've associated your LOCAL_SITE_NAME identity with
the appropriate Google identity (in case you have multiple Google
accounts). In any case, LOCAL_SITE_NAME only reads from that
information and never modifies it in any way.
<p/>
Repeating for clarity: LOCAL_SITE_NAME does not add, delete, or modify
any of your Google contacts or profile data.
END;

  $gcfConfig["section"]["overview"]["content"]["text-3"] = <<<END
<p/>
There are a few other things you can do from tabs on this page:
<ul>
<li>Manually refresh our local copy of your Google contacts 
    (see the section REF_SECTION_HEADER_REFRESH)</li>
<li>Share some of your Google contacts with other LOCAL_SITE_NAME users 
    or accept what they have shared to you 
    (see the section REF_SECTION_HEADER_SHARING)</li>
<li>Withdraw consent to access the Google data that you have previously granted. 
    (see the section REF_SECTION_HEADER_CONSENT.)</li>
</ul>
<p/>
If you are just getting started, go to the REF_SECTION_HEADER_CONSENT section, 
and then verify your identity match-up in the REF_SECTION_HEADER_IDENTITIES section.
END;

  /**
   * When we need to redirect for authorization, this text is displayed to
   * the user to explain what's happening. It doesn't include the actual
   * button and accompanying label, which are rendered separately.
   */
  $gcfConfig["section"]["consent"]["content"]["intro_auth"] = <<<END
Get started by going to Google so that you can give your consent to
allow LOCAL_FEATURE_NAME to access your data. You will be giving consent
(also known as authorization) for LOCAL_SITE_NAME to access your
contacts (including your contact groups) and your profile information.
If you have more than one Google account, you will be asked to choose
one. LOCAL_FEATURE_NAME can only associate a single Google identity
with your LOCAL_SITE_NAME identity. We use the profile information on the
REF_SECTION_HEADER_IDENTITIES section so that you know you have
associated the right Google account.
<p/>
You normally only have to go through this process once, but we may ask
you to repeat it if something has gotten out of sync between
LOCAL_SITE_NAME and Google. (If that happens, please report it so
that we can improve the service for everyone.)
<p/>
Google will redirect you back to this page, to the
REF_SECTION_HEADER_IDENTITIES section, after you've consented. We
recommend that you manually refresh your Google contacts on
LOCAL_SITE_NAME after you have been redirected back from Google (go to
the REF_SECTION_HEADER_REFRESH section), though that is an optional
step.
END;

  /**
   * The user is given a way to revoke their Google authorization.
   * It revokes the token from the Google side, and it purges all
   * the data that we have on our side. This text is displayed before
   * the button that they would click to deauthorize.
   */
  $gcfConfig["section"]["consent"]["content"]["intro_deauth"] = <<<END
You can tell LOCAL_SITE_NAME to discard all data fetched from your
Google profile and contacts (our local cache). We also notify Google
that you have withdrawn your consent. Once you have gone
through this step, you must go through the Google consent process
again if you wish to continue using LOCAL_FEATURE_NAME.
<p/>
You can, if you wish, verify that the consent has been withdrawn by
visiting your
<a target='_blank' rel='noopener' href='https://myaccount.google.com/permissions'>
Google account settings
EXTERNAL_LINK_ICON
</a>
page.
You are welcome to instead withdraw your consent directly on that
Google settings page. If you do that, we won't know about it, and we won't
automatically delete your data from our local cache until you visit
this page again.
END;

  /**
   * Text displayed at the top of the identities section.
   */
  $gcfConfig["section"]["identities"]["content"]["intro"] = <<<END
This section exists so that you can verify that you have associated
the correct Google account with your LOCAL_SITE_NAME account. It's
easy to make a mistake if you have multiple Google accounts. If you
are ever in doubt, you can always check it here.
<p/>
If you have just arrived here after giving your consent on Google,
you may wish to manually refresh our local copy of your contact
data (see the section REF_SECTION_HEADER_REFRESH). That's optional,
but it may make it easier to understand certain details.
END;

  /**
   * The user is given a way to manually fetch Google contacts.
   * This text is displayed before the manual refresh button.
   */
  $gcfConfig["section"]["refresh"]["content"]["intro"] = <<<END
LOCAL_FEATURE_NAME normally fetches Google contact data automatically,
when you actually try to use it. However, for efficiency, we won't do
that fetch if we have done so recently. In the meantime, we consult
that previously-fetched data, which we say is "cached" locally. That
means you might not see any recent additions or changes you've made to
your Google contacts. You can use the button below to cause an
immediate refresh of your Google contacts. That will replenish our
local cache with your current Google contacts information.
<p/>
You can do an incremental refresh (recommended), which will just fetch
the differences since the last manual or automatic refresh.
Or, you can do a full refresh, which will delete currently cached data and
re-fetch everything. A full refresh takes longer, and we don't
recommend it in general. However, a full refresh might be useful if
you are seeing errors reported on this page or if you suspect something isn't quite right.
(You should still report any errors, even if a full refresh resolves it for you.)
<p/>
Don't be too concerned if the numbers reported by a refresh don't add up to
exactly what you expect. There is some overlap, double counting,
and rare items accounted in the short report.
Still, it should be in general area of the numbers you expect.
END;

/**
   * This text introduces and describes what contact group sharing is all about.
   * It's used in the section that allows a user to edit their sharing.
   * The intro text is a short introduction, then the table sharing is shown,
   * and then the details text is shown.
   */
  $gcfConfig["section"]["sharing"]["content"]["intro"] = <<<END
You can share some or all of your Google contact information with one
or more LOCAL_SITE_NAME users. What that means is that when they use
LOCAL_FEATURE_NAME, they will see not only their own Google contacts,
but also any of yours that you have shared with them. You are still
the owner of those shared contacts, and you can stop sharing at any
time. In this section, you can see and edit which contacts you share
and with whom.
<p/>
On the Google contacts web page, you can create labels and attach
those labels to any number of your contacts. All of your contacts
that have the same particular label are called a "contact group". You
can have as many labels as you would like, and you can attach any
number of labels (including none) to any Google contact. (Google also
defines a handful of "system" labels. For example, if you "star" a
contact, Google attaches the label "Starred" to that contact, even
though "Starred" does not show up in the list of labels on the Google
web page. "Starred" will show up in your list of contact groups at
LOCAL_SITE_NAME.)
<p/>
<b>When a contact group is shared with you, you must explicitly opt-in
if you want to accept it. You should not opt-in if you don't
recognize the user sharing a contact group with you.</b>
<p/>
There is more information further below about how LOCAL_FEATURE_NAME does sharing.
END;

  $gcfConfig["section"]["sharing"]["content"]["details"] = <<<END
At LOCAL_SITE_NAME, sharing is based on contact groups that you create
by using labels on the Google web page (or any of the pre-defined
"system" contact groups that Google provides). For example, if
LOCAL_SITE_NAME user Alice has a contact group with the label "Ducks
and Geese" that Alice wants to share with LOCAL_SITE_NAME user Bob,
Alice could do so simply by sharing that contact group with Bob on this page.
If Alice does not already have a contact group with the right set of
contacts for sharing with Bob, Alice could create a new Google label like
"Contacts I want to share with Bob", and then Alice could share that
new contact group with Bob on this page. (If you are adding new labels or
otherwise rearranging your contact group memberships on the Google
contacts page, you might have to manually refresh your data to get it
show up here without a delay. See REF_SECTION_HEADER_REFRESH. An
incremental refresh should be sufficient.)
<p/>
There are a few things to know about your contact group sharing:
<ul>
<li>Unlike most other contacts-related data, your sharing information
is strictly local. It is not obtained from Google. When you
purge your data at LOCAL_SITE_NAME by revoking your Google
consent, the sharing information is not deleted.
If you want to also delete your sharing information, use the edit
button to remove all sharing.</li>
<li>You are sharing the contact group. If you add or remove contacts
from that group, the users you are sharing with will
automatically see those changes reflected in the group
memberships.</li>
<li>Google allows you to change the value of a label that defines a
contact group. That does not adversely affect the sharing of
that contact group at LOCAL_SITE_NAME. We will see the name
change for the contact group and update things accordingly. In
other words, you don't have to worry about it. It should work the
way you want it to work.</li>
<li>We don't validate the LOCAL_SITE_NAME user identities that you use
in your sharing information. When it comes to actually sharing
contact records, only LOCAL_SITE_NAME users can use the
LOCAL_FEATURE_NAME. That allows you to set up sharing a contact
group with another LOCAL_SITE_NAME user before they actually
start using LOCAL_FEATURE_NAME. It's still up to you to enter
their identities correctly.</li>
<li>When you start or stop sharing a contact group with another
LOCAL_SITE_NAME user, they don't receive any explicit
notification about it. However, they will see it in the "shared by" part of the table when
they view this page. You should notify them to avoid confusion.</li>
<li>The user you are sharing with might have a contact group with the
same name as your contact group. In fact, that's guaranteed when
it comes to "system" contacts groups. You don't have to worry
about it. If Alice shares a contact group named "Friendly
ghosts" with Bob, who also has a "Friendly ghosts" contact group,
Bob will see entries from both groups when actually looking up
contacts. Alice won't see entries from Bob's "Friendly ghosts"
contact group unless Bob shares that group with Alice.</li>
</ul>
END;

  $gcfConfig["sharing_edit_intro_text"] = <<<END
Your current contact group sharing, by you or to you, if any, is shown
above.  Only contact groups actually shared are shown.  To modify what
you are sharing or with whom, click the "Edit" button below.  You
can't edit sharing by another LOCAL_SITE_NAME user to you, but you can
chose to opt in to accepting it or not.
END;

  $gcfConfig["sharing_save_intro_text"] =
    "Your contact group sharing changes are not saved until you click the \"Save\" button below.";

  $gcfConfig["section"]["ldaphowto"]["content"]["intro"] = <<<END
This section describes how to configure your email or other application to use
LDAP.
Since you might be referring back to this section from time to time,
we'll start off with a table summarizing the particulars specific to LOCAL_FEATURE_NAME.
That's likely to be different in specifics from information you might be given for
using some other LDAP server.
Then, we'll provide some explanation of what those items are,
and we'll describe how to configure a few actual clients as examples.
<p/>
Here is that table of configuration items along with a generic image of a typical configuration screen.
END;

  $gcfConfig["section"]["ldaphowto"]["content"]["terminology"] = <<<END
If this is your first exposure to LDAP,
you might be surprised to learn how many applications support it as a
mechanism for accessing address book information.
(It's also used for many other purposes.)
The technical details of LDAP are complex,
and the jargon is not especially intuitive to the average user.
As so often happens, many application authors try to make things friendlier
by describing LDAP concepts with alternative terminology.
That's well-intentioned, but it leads to inconsistencies among applications.
Even if you consider yourself to be a fairly technical person,
there is no shame in being confused by an application's LDAP configuration screen.
<p/>
Here is a brief description of the things you may have to configure for an application.
You will see many similarities to things you are used to for browsing web pages,
but there are some important differences.
<ul>
<li>
<i><b>Server and port</b></i>.
You will be contacting a particular server where LDAP is running.
You will also be using a specific TCP/IP port number,
though there are default port numbers for non-secure and secure LDAP connections.
</li>
<p/>
<li>
<i><b>Secure or non-secure</b></i>.
Connections to LDAP servers may be encrypted or unencrypted,
just as is the case for web pages.
Consult the table to see which connection type or types is available for LOCAL_FEATURE_NAME.
<p/>
An application may refer to secure connections as "ldaps", "encrypted",
"secure", or some other term entirely.
(Note: If your application asks if you want to use "StartTLS",
you should know that LOCAL_FEATURE_NAME does not support that security option.)
The default port number for secure LDAP connections is 636.
The default port number for non-secure LDAP connections is 389.
LOCAL_FEATURE_NAME might have been configured to use different port numbers.
<p/>
If LOCAL_FEATURE_NAME has been configured to give you a choice between
secure and non-secure connections, you should choose to use secure connections.
You will be sending login and password information over the connection, and you want to protect it.
</li>
<p/>
<li>
<i><b>Contacts and groups</b></i>.
You can use LOCAL_FEATURE_NAME to look up individual contacts or groups of contacts.
(You can find more explanation of contact groups in the REF_SECTION_HEADER_SHARING section.)
It's less common for applications to understand how to use groups than individual contacts,
but some certainly do.
<p/>
The information in an LDAP directory is arranged hierarchically.
You would not be far wrong in thinking about it the same way you think about
nested folders on your local computer.
When your application sends a request to the LDAP server,
it has to tell the server where to start looking.
The LDAP terminology for that is "base distinguished name" or Base DN.
There are different Base DNs for the set of individual contacts and for the set of contact groups.
Your application might refer to the starting point as "Base DN", "directory context",
or some other term.
</li>
<p/>
<li>
<i><b>Login credentials</b></i>.
LDAP refers to the process of logging in (authenticating) as "binding".
For technical reasons that aren't that interesting,
you can't just give your userid and password in the usual way.
Instead, you must present your userid in the form of an LDAP distinguished name.
And, because it's used to "bind" the user to a place in the LDAP server's information,
it's called a "Bind DN".
(Don't mistake it for the similar-sounding "Base DN" described earlier.)
If you are not used to LDAP, the format shown in the table above might look quite peculiar.
Your application might refer to this in one of many different ways,
but you should in any case follow the format shown in the table when you enter your userid.
<p/>
The password is a more normal-seeming item.
It need not be wrapped in any peculiar LDAP dressing,
but it might be referred to as a "bind password".
It might be the same as you use for other purposes at LOCAL_SITE_NAME,
or it might be different.
There should be a note in the table to clarify that for you.
Your application may offer to save it for you when you are doing the configuration,
it may prompt you for it the first time you need to do an LDAP request,
or it may ask you for it every time you do an LDAP request.
</li>
<p/>
<li>
<i><b>Object classes</b></i>.
LDAP can return information in various ways,
and the LDAP terminology for the type of information returned is "object class".
You will not normally have to provide this information when configuring your application,
but the values are provided on the off chance that you are asked.
<p/>
There are different object classes for individual contacts and contact groups
because the forms of information returned are different for each.
</li>
</ul>
END;

  $gcfConfig["section"]["ldaphowto"]["content"]["clients"] = <<<END
Here are a few examples of specific LDAP clients.
Terminology can vary widely in LDAP clients,
even for different releases of the same application,
so what you actually see might be slightly different.
Remember, these are only examples.
Use the explanations at the top of this section to guide you in configuring the applications.
<p/>
A Google contact can have more than one email address, phone number, street addresses, and similar things.
LOCAL_FEATURE_NAME, likewise, has all that are defined in your Google contacts.
Some LDAP clients, unfortunately, don't deal very well with multiple email addresses or phone numbers.
They will usually pick one and ignore the others.
In common cases, the one they pick is the first one listed in the LDAP response from the server.
Google will let you designate an email address or phone number as the default or primary choice for a contact.
In the Google Contacts app on Android, you can select the email or phone number and choose "Set default".
We haven't found a way to designate the default item in the Google Contacts web application,
but we have noticed that the first item in the list seems to be used as the default.
(That is, if you designate a default in the phone app,
you will soon see it moved to the top of the list in the web app.)
LOCAL_FEATURE_NAME can't force an LDAP client to pick the default item out of a list,
but it does make sure that the default item is the first item in the list in the LDAP server response.
That usually does exactly what's needed.
<p/>
One final note: Some applications will have options for adding or editing contacts
or modifying contact group memberships.
Regardless of what the application user interface says,
LOCAL_FEATURE_NAME does not provide any way to modify any of the data.
That's intentional, and any changes must be made via a Google web page or app.
END;

  $gcfConfig["section"]["ldaphowto"]["content"]["clients-rc"] = <<<END
<p/>
<h3>Roundcube</h3>
<a target='_blank' rel='noopener' href='https://Roundcube.net/'>
Roundcube
EXTERNAL_LINK_ICON
</a>
is a popular open source software package for providing browser-based access to email.
It is philosophically similar to Gmail and other webmail interfaces,
but it is not a service you can sign up for.
Instead, it is a software package that you can install to provide access to an existing email server.
We're not suggesting that you download Roundcube and install it,
though you certainly might choose to do so if you already operate an email server.
<p/>
Whoever administers the Roundcube server would be responsible for configuring access to any LDAP directories.
We're not showing the configuration since it's simply a text file with appropriate information supplied.
We're showing Roundcube as an example of a client application because it has a fairly complete LDAP client implementation.
<p/>
In addition to searching for individual users in LOCAL_FEATURE_NAME,
it is one of the few email clients we've seen that will let you
search for a contact group and use it to address a message to all
of the members of that group.
In the screenshot, you can see an example of selecting the user's "authors" contact group.
The user interface expands to show the members of that group.
When addressing an email message,
the expanded list of individuals would be added to the TO:, CC:, or BCC: line.
No special support is needed by the email server.
That's very handy if there is a group of people you regularly exchange email with.
<p/>
END;

  $gcfConfig["section"]["ldaphowto"]["content"]["clients-tbird"] = <<<END
<p/>
<h3>Thunderbird</h3>
<a target='_blank' rel='noopener' href='https://www.thunderbird.net/'>
Thunderbird
EXTERNAL_LINK_ICON
</a>
is a popular open source, cross-platform (Windows, Mac, and Linux) email application.
Since you install it on your local computer,
it's up to you to configure it to use LOCAL_FEATURE_NAME as an address book.
<p/>
The configuration screen you see in the screenshot is fairly typical
for an LDAP client application.
You are asked a few questions, but most low-level nitty-gritty details
are pushed off to an "advanced" screen.
You probably do not ever need to worry about the "advanced" screen,
since Thunderbird, like most LDAP client applications,
uses common settings and conventions that work well with LOCAL_FEATURE_NAME.
<p/>
Thunderbird's LDAP client implementation has a few limitations.
<ul>
<li>
Although you can define as many LDAP address books as you want,
only one of them can be active at any given time
(that's in addition to the local address book built into Thunderbird).
</li>
<li>
You can set up an LDAP configuration for searching for contact groups,
but Thunderbird doesn't know what to do with the results.
It treats them like malformed individual contacts, so it's not useful.
</li>
<li>
Finally, Thunderbird is one of those LDAP clients that does not expect
a contact to have more than one email address.
See the information above about such clients.
</li>
</ul>
END;

  $gcfConfig["section"]["ldaphowto"]["content"]["clients-voip"] = <<<END
<p/>
<h3>Phone number to name</h3>
You are probably familiar with the
<a target='_blank' rel='noopener' href='https://en.wikipedia.org/wiki/Caller_ID'>
CallerID
EXTERNAL_LINK_ICON
</a>
feature
that can tell you the phone number of the calling party.
Most modern phone equipment can also show you the name of that caller.
Many people don't know that the name comes from a completely unrelated feature called
<a target='_blank' rel='noopener' href='https://en.wikipedia.org/wiki/Calling_Name_Presentation'>
CNAM
EXTERNAL_LINK_ICON
</a>
that is not part of CallerID.
When someone calls you, the <i>number</i> is supplied by <i>their</i> phone company.
The <i>name</i> is provided by <i>your</i> phone company.
Your phone company uses the CallerID to look for the name in a database.
The bad news is that databases of names is woefully incomplete,
so your phone company often cannot provide a number's matching name.
That's why you often see just the name of a city or a generic term
like "cellular caller" or even "unavailable" instead of the caller's name.
<p/>
If you have the right kind of phone system on your end,
you can ask it look up the caller's information
instead of relying on what your phone company does.
The technique is the same: you start with the CallerID number and look for the name.
If your phone system will let you do an LDAP look-up,
you can tell it to search in LOCAL_FEATURE_NAME.
If it's one of your contacts calling you, the name will be found.
Your phone might also display other information from the contact record.
<p/>
In this screenshot, you see the configuration screen of a digital telephone set.
It's the web configuration for a Grandstream GXV-3240 VoIP phone,
but there are many other brands that have similar features.
(If you have a digital phone system in a company,
the configuration is probably done centrally by an administrator.)
Since this is just an example, we won't explain all the fields and syntax in detail.
It would be better to consult the specific equipment's manual.
You should be able to see that most things are simple variations
of the generic configuration options given at the top of this page.
Since this particular phone is also able to send email and text messages,
it offers a few different ways of looking things up.
It also lets you spell out a name and then provides a button to dial the number returned by LDAP.
END;

/****************************************************************************/
/* END OF CONFIGURATION ITEMS. You usually won't change anything below here.*/
/****************************************************************************/

try {
    $cfg_file = preg_replace('/.php$/i', ".cfg", __FILE__);
    if (is_file($cfg_file)) {
        require $cfg_file;
    }
    if ($gcfConfig["autoload_php"]) {
        require_once $gcfConfig["autoload_php"];
    }
    main();
} catch (Throwable $t) {
    // I'm not using set_exception_handler because its sudden exit screws up the page formatting.
    exceptionHandler($t);
}

function main()
{
    global $gcfConfig;
    $ctx = initContextItems();
    if (redirectables($ctx)) {
        return;
    }

    callJavaForInfo($ctx);

    emitSectionTabs($ctx);
    emitLoginSuggestion($ctx);

    global $gcfConfig;
    foreach ($gcfConfig["section"] AS $slug => $section) {
        emitSection($ctx, $slug, $section);
    }

    $activateSection = $ctx["gcf_section"];
    if (! $activateSection) {
        $activateSection = "overview";
    }
    emitScripting($activateSection);
    emitAckFooter($ctx);
}

function initContextItems()
{
    global $gcfConfig;
    $ctx = [];

    get2ctxNull($ctx, "code");
    get2ctxNull($ctx, "state");
    get2ctxNull($ctx, "gcf_refresh_result");
    get2ctxNull($ctx, "gcf_section");

    get2ctxBool($ctx, "gcf_deauth");
    get2ctxBool($ctx, "gcf_refresh");
    get2ctxBool($ctx, "gcf_full");
    get2ctxBool($ctx, "gcf_update_sharing");
    get2ctxBool($ctx, "gcf_edit_sharing");

    if ($gcfConfig["local_identity_function"] == NULL) {
        throw new Exception("Administrator: You must set a value for \$gcfConfig['local_identity_function']");
    }

    return $ctx;
}

function get2ctxNull(&$ctx, $key)
{
   $ctx[$key] = (array_key_exists($key, $_GET)) ? $_GET[$key] : NULL;
}

function get2ctxBool(&$ctx, $key)
{
   $ctx[$key] = array_key_exists($key, $_GET);
}

function callJavaForInfo(&$ctx)
{
    global $gcfConfig;
    $lid = $gcfConfig["local_identity_function"]();
    if ($lid == NULL) {
        return;
    }

    loadOauth2TokenBlob($ctx);
    getGoogleIdentity($ctx);

    $result = callJavaApplication($ctx, "REPORT_SHARES");
    if ($result == NULL) {
        return;
    }
    if (! isSuccessJavaCall($ctx, $result)) {
        throw new Exception("There was a problem finding your sharing information.");
    }
    unset($result[0]);
    $ctx["reportShares"] = $result;
}

function emitLoginSuggestion(&$ctx)
{
    global $gcfConfig;
    $lid = $gcfConfig["local_identity_function"]();
    if ($lid == NULL) {
        $loginSuggestion = "<div " . stylize("not_logged_in") . "><hr/>" . customizeString($gcfConfig["strings"]["not_logged_in_message"]) . "</div>";
        echo $loginSuggestion;
    }
}

function emitAckFooter(&$ctx)
{
    global $gcfConfig;
    if ($gcfConfig["acknowledgment_footer"]) {
        echo "<div " . stylize("acknowledgment_footer") . ">";
        echo "<hr/>";
        echo customizeString($gcfConfig["acknowledgment_footer"]);
        echo "</div>";
    }
}

function emitSectionTabs(&$ctx)
{
    global $gcfConfig;

    $tabRow = NULL;
    $tabRow .= "\n<div class='tab' " . stylize("section_tabs_div") . ">\n";
    foreach ($gcfConfig["section"] as $name => $blob) {
        $text = customizeString($blob["title"]);
        $tabRow .= "<button" . stylize("section_tab_nonselected") . "class='sectionlinks' ";
        $tabRow .= " id='" . $name . "Button' ";
        $tabRow .= "onclick='openSection(\"" . $name . "\")' ";
        $tabRow .= "onmouseover=\"invert(this)\" ";
        $tabRow .= "onmouseout= \"invert(this)\" ";
        $tabRow .= ">" . $text .  "</button>\n";
    }
    $tabRow .= "</div>\n";
    echo $tabRow;
}

function emitSection(&$ctx, $sectionName, &$section)
{
    global $gcfConfig;
    echo "<div class='sectioncontent' id='{$sectionName}'>";
    $header = customizeString($section["title"]);
    echo "<br/><a NAME='{$sectionName}'<span " . stylize("section_header") . ">{$header}</span></a><hr/>";

    $content = $section["content"];
    foreach ($content AS $key => $value) {
        if (is_array($value) && is_callable($value[0])) {
            echo $value[0]($ctx, $sectionName, $value);
        } else if (is_callable($value)) {
            echo $value($ctx, $sectionName);
        } else {
            echo customizeString($value);
        }
    }
    echo "</div>";
}

function emitLdapConfigTable(&$ctx, $sectionName)
{
    global $gcfConfig;
    $lc = $gcfConfig["ldap_client_config_info"];
    $thStyle = stylize("ldap_config_th");
    $tdStyle = stylize("ldap_config_td");

    echo "<center><table>\n";
    $item = $lc["table_header"];
    $thText = customizeString($item["text"]);
    $thComment = customizeString($item["comment"]);
    echo "<tr><th colspan='2' {$thStyle}>{$thText}</th></tr>\n";

    foreach ($lc AS $key => $item) {
        if ($key == "table_header") {
            continue;
        }
        $text    = customizeString($item["text"]);
        $value   = customizeString($item["value"]);
        $comment = (isset($item["comment"])) ? customizeString($item["comment"]) : NULL;
        if ($comment) {
            echo "<tr><td rowspan='2' {$tdStyle}>{$text}</td><td {$tdStyle}>{$value}</td></tr>\n";
            echo "<tr><td {$tdStyle}>{$comment}</td></tr>\n";
        } else {
            echo "<tr><td {$tdStyle}>{$text}</td><td {$tdStyle}>{$value}</td></tr>\n";
        }
    }

    if ($thComment) {
        echo "<tr><td colspan='2' {$tdStyle}>{$thComment}</td></tr>\n";
    }
    echo "</table></center>\n";
}

/**
 * These are things that might perform an action and then redirect the browser.
 * Returns TRUE if the redirection is happening, else FALSE.
 */
function redirectables(&$ctx)
{
    global $gcfConfig;
    if ($ctx["code"] && $ctx["state"]) {
        actionReceiveAuthorizationCode($ctx);
        if ($gcfConfig["nuke_all_query_parameters"]) {
            $redirectionQueryString = NULL;
        } else {
            // Remove code and state params, keep any others, and redirect
            $redirectionQueryString = $_SERVER['QUERY_STRING'];
            $redirectionQueryString = httpStripQueryParam($ctx, $redirectionQueryString, 'code');
            $redirectionQueryString = httpStripQueryParam($ctx, $redirectionQueryString, 'state');
            $redirectionQueryString = httpStripQueryParam($ctx, $redirectionQueryString, 'gcf_');
        }
        if ($redirectionQueryString) {
            $redirectionQueryString .= "&gcf_section=identities";
        } else {
            $redirectionQueryString .= "gcf_section=identities";
        }
        // and now for the redirection to clean up the URL
        header("Location: " . $_SERVER["SCRIPT_URI"] . "?" . $redirectionQueryString);
        return TRUE;
    } else if ($ctx["gcf_deauth"]) {
        actionDeauthorize($ctx);
        if ($gcfConfig["nuke_all_query_parameters"]) {
            $redirectionQueryString = NULL;
        } else {
            // Remove code and state params, keep any others, and redirect
            $redirectionQueryString = $_SERVER['QUERY_STRING'];
            $redirectionQueryString = httpStripQueryParam($ctx, $redirectionQueryString, 'gcf_');
        }
        if ($redirectionQueryString) {
            $redirectionQueryString .= "&gcf_section=identities";
        } else {
            $redirectionQueryString .= "gcf_section=identities";
        }
        header("Location: " . $_SERVER["SCRIPT_URI"] . "?" . $redirectionQueryString);
        return TRUE;
    } else if ($ctx["gcf_refresh"]) {
        $gcf_refresh_result = actionManualRefresh($ctx);
        if ($gcfConfig["nuke_all_query_parameters"]) {
            $redirectionQueryString = NULL;
        } else {
            // Remove code and state params, keep any others, and redirect
            $redirectionQueryString = $_SERVER['QUERY_STRING'];
            $redirectionQueryString = httpStripQueryParam($ctx, $redirectionQueryString, 'gcf_');
        }
        if ($redirectionQueryString) {
            $redirectionQueryString .= "&gcf_section=refresh";
        } else {
            $redirectionQueryString .= "gcf_section=refresh";
        }
        if ($gcf_refresh_result) {
            $redirectionQueryString = $redirectionQueryString . "&gcf_refresh_result=" . base64urlEncode($gcf_refresh_result);
        }
        header("Location: " . $_SERVER["SCRIPT_URI"] . "?" . $redirectionQueryString);
        return TRUE;
    } else if ($ctx["gcf_update_sharing"]) {
        actionProcessSharingUpdates($ctx);
        if ($gcfConfig["nuke_all_query_parameters"]) {
            $redirectionQueryString = NULL;
        } else {
            // Remove code and state params, keep any others, and redirect
            $redirectionQueryString = $_SERVER['QUERY_STRING'];
            $redirectionQueryString = httpStripQueryParam($ctx, $redirectionQueryString, 'gcf_');
        }
        if ($redirectionQueryString) {
            $redirectionQueryString .= "&gcf_section=sharing";
        } else {
            $redirectionQueryString .= "gcf_section=sharing";
        }
        header("Location: " . $_SERVER["SCRIPT_URI"] . "?" . $redirectionQueryString);
        return TRUE;
    }
    return FALSE;
}

function getReferenceToSectionHeader($sectionName)
{
    global $gcfConfig;
    $sectionBlob = $gcfConfig["section"][$sectionName];
    $header = $sectionBlob["title"];
    return "<a onclick='openSection(\"{$sectionName}\")' " . stylize("section_header_reference") . " >{$header}</a>";
}

function emitIdentitiesSpecifics(&$ctx, $sectionName)
{
    global $gcfConfig;

    echo "<ul><li>";
    $lid = $gcfConfig["local_identity_function"]();
    if ($lid) {
        echo customizeString($gcfConfig["strings"]["local_identity_is"]) . " <i>&ldquo;" . htmlspecialchars($lid) . "&rdquo;</i>";
    } else {
        echo "<i>" . customizeString($gcfConfig["strings"]["local_identity_unknown"]) . "</i>";
    }
    echo "</li>";

    if (isset($ctx["googleResourceName"])) {
        $grnNote  = "<li>" . $gcfConfig["strings"]["google_identity_is"] . " <i>&ldquo;";
        $grnNote .= htmlspecialchars($ctx["googleName"]);
        $grnNote .= "</i> <i>&lt;" . htmlspecialchars($ctx["googleEmailAddress"]);
        $grnNote .= "&gt;&rdquo;</i></li>";
        $grnNote .= "<li>" . $gcfConfig["strings"]["google_resource_name_is"] . "<i>&ldquo;";
        $grnNote .= htmlspecialchars($ctx["googleResourceName"]);
        $grnNote .= "&rdquo;</i></li>";
        $grnNote .= "</ul>";
        if ($ctx["googlePictureUrl"]) {
            $grnNote .= "<p/><center><img src='";
            $grnNote .= $ctx["googlePictureUrl"];
            $grnNote .= "' alt='";
            $grnNote .= htmlspecialchars($ctx["googleName"]);
            $grnNote .= "'/></center><p/>";
        }
        $grnNote .= $gcfConfig["strings"]["google_resource_name_explain"];
        echo customizeString($grnNote);
    } else {
        if ($lid != NULL) {
            echo "<li><i>" . customizeString($gcfConfig["strings"]["google_identity_unknown_auth"]) . "</i></li>";
        } else {
            echo "<li><i>" . customizeString($gcfConfig["strings"]["google_identity_unknown_login"]) . "</i></li>";
        }
        echo "</ul>";
    }
}

function emitRefreshSpecifics(&$ctx, $sectionName)
{
    global $gcfConfig;

    $sorryButNo = sorryButNo($ctx, FALSE, $gcfConfig["strings"]["refresh_cant_local"], $gcfConfig["strings"]["refresh_cant_google"]);
    if ($sorryButNo) {
        echo customizeString($sorryButNo);
    } else {
        $qs = $_SERVER['QUERY_STRING'];
        if ($qs) {
            $qs = $qs . "&";
        }
        $qs = $qs . "gcf_refresh=true";
        $refreshUrl = $_SERVER["SCRIPT_URI"] . "?" . $qs . "&gcf_section=refresh";
        $restoreUrl = $_SERVER["SCRIPT_URI"] . "?" . $qs . "&gcf_full=true&gcf_section=refresh";

        emitButton("refresh", $refreshUrl);
        emitButton("restore", $restoreUrl);

        if (isset($ctx["gcf_refresh_result"])) {
            echo "<div " . stylize("refresh_result_div") . "><i>";
            echo customizeString($gcfConfig["strings"]["refresh_previous_was"]);
            echo "</i><p/>";
            echo "<pre " . stylize("refresh_result_pre") . ">";
            $gcf_refresh_result = base64urlDecode($ctx["gcf_refresh_result"]);
            $lines = explode("\n", $gcf_refresh_result);
            foreach ($lines as $line) {
                echo htmlspecialchars($line, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401) . "\n";
            }
            echo "</pre></div>";
            emitContinueButton();
        }
    }
}

function emitFormButton($buttonName, $formName, $buttonType)
{
    global $gcfConfig;
    $button = $gcfConfig["button"][$buttonName];
    if (! $button) return;

    echo "<div><center>";
    $clickMessage = (isset($button["click_message"])) ? customizeString($button["click_message"]) : NULL;
    $buttonText = customizeString($button["text"]);
    if ($clickMessage) {
        echo "<p/>";
        echo $clickMessage;
    }
    echo "<p/>";
    echo "<a><button " . stylize("button") . " type='{$buttonType}' form='{$formName}'>";
    echo $buttonText;
    echo "</button></a>\n";
    echo "</center></div>";
}

function emitImage($ctx, $sectionName, $args)
{
    $imageFileName = $args[1];
    if (strpos($imageFileName, '/') != 0) {
        $imageFileName = __DIR__ . "/" . $imageFileName;
    }
    $attributes = $args[2];
    $mimeType = $args[3];
    echo "<img {$attributes}\n";
    echo "src=\"data:image/png;base64,";
    $imageB64 = base64_encode(file_get_contents($imageFileName));
    echo $imageB64;
    echo "\"\n>";
}

function emitButton($buttonName, $url)
{
    global $gcfConfig;

    $button = $gcfConfig["button"][$buttonName];
    if (! $button) return;

    echo "<div><center>";
    $clickMessage = (isset($button["click_message"])) ? customizeString($button["click_message"]) : NULL;
    $buttonText = customizeString($button["text"]);
    if ($clickMessage) {
        echo "<p/>";
        echo $clickMessage;
    }
    echo "<p/>";
    echo "<a><button " . stylize("button") . " onclick=\"location.href='{$url}'\">";
    echo $buttonText;
    echo "</button></a>\n";

    echo "</center></div>";
}

function emitContinueButton()
{
    global $gcfConfig;
    $continueUrl = $_SERVER["SCRIPT_URI"];
    emitButton("continue", $continueUrl);
}

function emitSharingSpecifics(&$ctx, $sectionHeader)
{
    global $gcfConfig;

    $sorryButNo = sorryButNo($ctx, FALSE, $gcfConfig["strings"]["sharing_cant_local"], $gcfConfig["strings"]["sharing_cant_google"]);
    if ($sorryButNo) {
        echo customizeString($sorryButNo);
    } else {
        emitShareTableForm($ctx);
    }
}

function emitShareTableForm(&$ctx)
{
    global $gcfConfig;
    $r = NULL;
    $result = $ctx["reportShares"];
    $thStyle = stylize("sharing_th");
    $tdStyle = stylize("sharing_td");

    $divider = "\n\n\t\t\t\n\n";

    $rows = getShareTableRows($ctx, $result, $tdStyle, $divider);
    $tableRowSets = explode($divider, $rows);
    $rowsShareTo = $tableRowSets[0];
    if (count($tableRowSets) > 1) {
        $rowsShareFrom = $tableRowSets[1];
    } else {
        $rowsShareFrom = NULL;
    }
    if (count($result) > 0 && $rows) {
        if ($ctx["gcf_edit_sharing"]) {
            echo "<form id='editSharing' name='editSharing' method='post' action='";
            echo $_SERVER["SCRIPT_URI"] . "?gcf_update_sharing&gcf_section=sharing'>";
        }
        echo "<center><table>";
        $tableHeaderShareTo =   "<tr><th {$thStyle}>" .
                                customizeString($gcfConfig["strings"]["sharing_table_group"]) .
                                "</th><th {$thStyle}>" .
                                customizeString($gcfConfig["strings"]["sharing_table_with"]) .
                                "</th></tr>";
        $tableHeaderShareFrom = "<tr><th {$thStyle}>" .
                                customizeString($gcfConfig["strings"]["sharing_table_group"]) .
                                "</th><th {$thStyle}>" .
                                customizeString($gcfConfig["strings"]["sharing_table_by"]) .
                                "</th><th {$thStyle}>" .
                                customizeString($gcfConfig["strings"]["sharing_table_optin"]) .
                                "</th></tr>";
        $tableFooter = "</table></center>";
        echo $tableHeaderShareTo;
        echo $rowsShareTo;
        if ($rowsShareFrom) {
            echo "<tr><td>&nbsp;</td></tr>";
            echo $tableHeaderShareFrom;
            echo $rowsShareFrom;
        }
        echo $tableFooter;
    } else {
        if (! $ctx["gcf_edit_sharing"]) {
            global $gcfConfig;
            echo "<center><i>" . customizeString($gcfConfig["strings"]["sharing_you_have_none_text"]) . "</i></center>";
        }
    }
    echo "<p/>";
    if ($ctx["gcf_edit_sharing"]) {
        echo customizeString($gcfConfig["sharing_save_intro_text"]);
        emitFormButton("sharing_save", "editSharing", "submit");
        $baseUrl = $_SERVER["SCRIPT_URI"] . "?gcf_section=sharing";
        emitButton("sharing_cancel", $baseUrl);
        emitFormButton("sharing_reset", "editSharing", "reset");
        echo "</form>";
    } else {
        $sharingEditUrl = $_SERVER["SCRIPT_URI"] . "?gcf_edit_sharing&gcf_section=sharing";
        echo customizeString($gcfConfig["sharing_edit_intro_text"]);
        emitButton("sharing_edit", $sharingEditUrl);
    }
}

function getShareTableRows(&$ctx, $result, $tdStyle, $divider)
{
    global $gcfConfig;
    $rows = NULL;
    $isSharedFrom = FALSE;
    foreach ($result as $share) {
        if ($share == "") {
            continue;
        }
        $columns = explode("\t", $share);
        $fromTo = $columns[0];
        unset($columns[0]);
        $optIn = $columns[1];
        unset($columns[1]);
        $gId = $columns[2];
        unset($columns[2]);
        $gCount = $columns[3];
        unset($columns[3]);
        $gFname = $columns[4];
        unset($columns[4]);
        $shareFromUser =  (isset($columns[5])) ? $columns[5] : NULL; // might be null if share-to case

        if ($fromTo == "<") {
            if (! $isSharedFrom) {
                // first shared-from record ... add the great divider
                $rows .= $divider;
            }
            $isSharedFrom = TRUE;
        }
        $isOptedIn = ($optIn == "t");

        if ($ctx["gcf_edit_sharing"] || $isSharedFrom || count($columns) > 0) {
            if ($gCount == "1") {
                $s = "";
            } else {
                $s = "s";
            }
            $rows .= "<tr>";
            $countString = NULL;
            if (! $isSharedFrom) {
                $countString = " <i>(" . $gCount . " contact{$s})</i>";
            }
            $rows .= "<td {$tdStyle}>" . htmlspecialchars($gFname) . $countString . "</td>";
            $rows .= "<td {$tdStyle}>";
            $sharePartners = "";
            foreach ($columns as $column) {
                $sharePartners .= htmlspecialchars($column) . " ";
            }
            if ($ctx["gcf_edit_sharing"]) {
                global $gcfConfig;
                $inputItem = "<input type='text' size='60' placeholder='";
                $inputItem .= customizeString($gcfConfig["strings"]["sharing_table_shadow"]);
                $inputItem .= "' name='GCF_T:" . base64urlEncode($gId) . ":x' ";
                $inputItem .= "value='" . $sharePartners . "' ";
                $inputItem .= "/>";
            }
            if ($ctx["gcf_edit_sharing"] && ! $isSharedFrom) {
                $rows .= $inputItem;
            } else {
                $rows .= $sharePartners;
            }
            $rows .= "</td>";
            if ($isSharedFrom) {
                $box  = "<td {$tdStyle}><input type='checkbox' name='GCF_F:";
                $box .= base64urlEncode($gId) . ":" . base64urlEncode($shareFromUser);
                $box .= "' value='t' ";
                if ($isOptedIn) {
                    $box .= "checked ";
                }
                if (! $ctx["gcf_edit_sharing"]) {
                    $box .= "disabled ";
                }
                $box .= "></td>";
                $rows .= $box;
            }
            $rows .= "</tr>";
        }
    }
    return $rows;
}

function isSuccessJavaCall(&$ctx, $result)
{
    $javaExitCode = $result[0];
    return (stristr($javaExitCode, '<<*>>ERROR') == NULL);
}

/**
 * Call Java to do something. Return the Java exit code and output
 * as an array of lines. Array item 0 is the (English language) exit code phrase.
 */
function callJavaApplication(&$ctx, $actions)
{
    global $gcfConfig;
    $lid = $gcfConfig["local_identity_function"]();
    if ($lid == NULL) {
        // login session probably timed out during user "think time"; start over
	return NULL;
    }
    $proof = getLocalProof($ctx, $lid);
    // Obviously, these command line options have to be coordinated with the Java application.
    $unproofedBody = $actions . "\n";
    return callJavaApplicationHttp($ctx, $lid, $proof, $unproofedBody);
}

use GuzzleHttp\Client;
function callJavaApplicationHttp(&$ctx, $lid, $proof, $unproofedBody)
{
    global $gcfConfig;
    $proofedBody = "--localid " . $lid . "\n--proof " . $proof . "\n" . $unproofedBody;
    $http = $gcfConfig["java_companion_http"];
    $clientOptions =
    [
        'timeout'  => 2.0,
        'http_errors' => FALSE,
        'synchronous' => TRUE,
        'verify' => $http["validate_cert"],
    ];
    if ($http["digest_auth"] == TRUE) {
        $clientOptions['auth'] = [$lid, $gcfConfig["proof_secret"], 'digest'];
	$body = $unproofedBody;
    } else {
        $body = $proofedBody;
    }
    $client = new Client($clientOptions);
    if ($http["use_tls"] == TRUE) {
        $url = "https://";
    } else {
        $url = "http://";
    }
    $url .= $http["hostname"] . ":" . $http["port"] . $http["path"];
    try {
        $response = $client->request('POST', $url, ['body' => $body]);
    } catch (Throwable $t) {
        error_log("Is MyVD service running? Falling back to pipe invocation. " . $t->getMessage());
        // fall back to pipe invocation
        return callJavaApplicationPipe($ctx, $proofedBody);
    }
    $code = $response->getStatusCode(); // 200
    if ($code == 200) {
        $body = $response->getBody();
        $contents = $body->getContents();
        $contentsArray = explode("\n", $contents);
        $output = array_merge(array("0"=>"Successful"), $contentsArray);
    } else {
        $reason = $response->getReasonPhrase(); // OK
        $output[0] = "<<*>>ERROR: Failed operation. HTTP Error Code " . $code . " " . $reason;
    }
    return $output;
}

function callJavaApplicationPipe(&$ctx, $proofedBody)
{
    global $gcfConfig;
    $cmd = sprintf($gcfConfig["java_companion_command_line"], "") . " -";
    $output = [0 => 'dummy']; // placeholder to reserve the 0 slot

    $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("pipe", "w") // companion does not write to stderr
    );

    $process = proc_open($cmd, $descriptorspec, $pipes, NULL, NULL);
    fwrite($pipes[0], $proofedBody);
    fclose($pipes[0]);

    fclose($pipes[2]);

    while (($line = stream_get_line($pipes[1], 9999999, "\n")) !== FALSE) {
      $output[] = $line;
    }
    fclose($pipes[1]);

    $exitCode = proc_close($process);
    if ($exitCode == 0) {
        $output[0] = "Successful";
    } else {
        $output[0] = "<<*>>ERROR: Failed operation. Error Code " . $exitCode;
    }
    return $output;
}

/**
 * When doing the OAuth2 handshake, there is an optional "state"
 * parameter that is used to defeat a few kinds of security
 * attacks. The basic idea is that you send the "state" parameter with
 * the request, and you compare what you get back with what you
 * sent.
 *
 * In this implementation, we generate a random string and store a
 * copy in the PHP _SESSION array. We start a PHP session if there
 * isn't already one active. If your PHP sessions are disabled, or if
 * the response might come back to a server that doesn't necessarily
 * share the same session info, you will have to do something
 * else. Search for the use of this variable below to see where you
 * will have to change things.
 *
 * If you don't know what I am talking about, you are probably OK.
 */
global $GCF_GASS;
$GCF_GASS = 'gcontact_oauth2_state';

function emitConsentAuthSpecifics(&$ctx, $sectionName)
{
    global $gcfConfig, $GCF_GASS;

    $sorryButNo = sorryButNo($ctx, TRUE, $gcfConfig["strings"]["auth_cant_local"], $gcfConfig["strings"]["auth_cant_google"]);
    if ($sorryButNo) {
        echo customizeString($sorryButNo);
    } else {
        // Request authorization from the user.
        $state = sha1(rand());
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION[$GCF_GASS] = $state;
        $client = getGoogleClient($ctx);
        $client->setState($state);

        $authUrl = $client->createAuthUrl();
        emitButton("auth", $authUrl);
    }
}

function emitConsentDeauthSpecifics(&$ctx, $sectionName)
{
    global $gcfConfig;

    $sorryButNo = sorryButNo($ctx, FALSE, $gcfConfig["strings"]["deauth_cant_local"], $gcfConfig["strings"]["deauth_cant_google"]);
    if ($sorryButNo) {
        echo customizeString($sorryButNo);
    } else {
        $qs = $_SERVER['QUERY_STRING'];
        if ($qs) {
            $qs = $qs . "&";
        }
        $qs = $qs . "gcf_deauth=true";
        $deauthUrl = $_SERVER["SCRIPT_URI"] . "?" . $qs;
        emitButton("deauth", $deauthUrl);
    }
}

/**
 * This function is called when OAuth2 redirects back to us and has
 * provided an authorization code.  An authorization code is
 * short-lived and has to be exchanged for an access token and refresh
 * token.  Google APIs handle the details of that last part.
 */
function actionReceiveAuthorizationCode(&$ctx)
{
    global $GCF_GASS;
    // PHP session was already started when we stored $GCF_GASS into it
    if (! array_key_exists($GCF_GASS, $_SESSION)) {
        throw new Exception("No OAuth2 state token in session." . $GCF_GASS . ".");
    }
    $state_from_session = $_SESSION[$GCF_GASS];
    unset($_SESSION[$GCF_GASS]);
    if (strcmp($ctx["state"], $state_from_session) !== 0) {
        throw new Exception("OAuth2 state validation failed.");
    }
    $result = callJavaApplication($ctx, "CREATE_IDENTITY " . base64_encode($ctx["code"]));
    if ($result == NULL) {
        return;
    }
    if (! isSuccessJavaCall($ctx, $result)) {
        $lid = $gcfConfig["local_identity_function"]();
        throw new Exception("Unable to create a new local entry for " . $lid . "\n" . $result);
    }
    return TRUE;
}

/**
 * Tells us to purge all of the data we have fetched and destroy
 * the access token.
 */
function actionDeauthorize(&$ctx)
{
    if (! isset($ctx["tokenBlob"])) {
        loadOauth2TokenBlob($ctx);
    }
    $tokenBlob = $ctx["tokenBlob"];
    if ($tokenBlob) {
        $result = callJavaApplication($ctx, "PURGE");
        if ($result == NULL) {
            return;
        }
        if (! isSuccessJavaCall($ctx, $result)) {
            throw new Exception("Unable to purge your local data."); // shouldn't happen
        }
        $accessTokenRevoker = new Google_AccessToken_Revoke();
        try {
            $revokeResult = $accessTokenRevoker->revokeToken(json_decode($tokenBlob, true));
        } catch (Throwable $t) {
            // we ignore the revoke failure since we have already purged local records
        }
    }
}

function actionManualRefresh(&$ctx)
{
    if ($ctx["gcf_full"]) {
        $result = callJavaApplication($ctx, "RESTORE\nREPORT");
    } else {
        $result = callJavaApplication($ctx, "REFRESH\nREPORT");
    }
    if ($result == NULL) {
        return;
    }
    if (! isSuccessJavaCall($ctx, $result)) {
        throw new Exception("There was a problem refreshing your data.");
    }
    unset($result[0]);
    $resultString = NULL;
    foreach ($result as $line) {
        $resultString = $resultString . $line . "\n";
    }
    return $resultString;
}

function actionProcessSharingUpdates(&$ctx)
{
    $sharingBlob = "";
    foreach ($_POST as $field => $value) {
        $fieldParts = explode(":", $field);
        if (count($fieldParts) == 3) {
            $gcfPrefix = $fieldParts[0];
            $gIdEncoded = $fieldParts[1];
            $sharedFromEncoded = $fieldParts[2];
            if ("GCF_T" == $gcfPrefix) {
                $toFrom = "T";
            } else if ("GCF_F" == $gcfPrefix) {
                $toFrom = "F";
            } else {
                error_log("Unrecognized sharing form field {$field} => {$value}. Ignored.");
                continue;
            }
            $sharingBlob .= $toFrom . "\t";
            $sharingBlob .= $gIdEncoded . "\t";
            $sharingBlob .= $sharedFromEncoded . "\t";
            $sharingBlob .= trim($value) . "\n";
        } else {
            error_log("Unrecognized sharing form field {$field} => {$value}. Ignored.");
            continue;
        }
    }
    $sharingBlob = base64urlEncode($sharingBlob);
    $result = callJavaApplication($ctx, "UPDATE_SHARES " . $sharingBlob);
    if ($result == NULL) {
        return;
    }
    if (! isSuccessJavaCall($ctx, $result)) {
        throw new Exception("There was a problem updating your contract group sharing.");
    }
}

function sorryButNo(&$ctx, $reverse, $localIdMessage, $googleIdMessage)
{
    global $gcfConfig;
    $lid = $gcfConfig["local_identity_function"]();
    if ($lid == NULL) {
        return "<center><i>{$localIdMessage}</i></center>";
    } else if ((! $reverse)  &&  (! $ctx["googleResourceName"])) {
        return "<center><i>{$googleIdMessage}</i></center>";
    } else if ($reverse && $ctx["googleResourceName"]) {
        return "<center><i>{$googleIdMessage}</i></center>";
    }
    return NULL;
}

function getGoogleClient(&$ctx)
{
    $client = new Google_Client();
    global $gcfConfig;
    $client->setApplicationName(customizeString($gcfConfig["oauth2_application_name"]));

    $client->addScope(Google_Service_PeopleService::CONTACTS_READONLY);
    $client->addScope(Google_Service_PeopleService::USERINFO_PROFILE);
    $client->addScope(Google_Service_PeopleService::USERINFO_EMAIL);

    global $gcfConfig;
    $client->setAuthConfig($gcfConfig["credentials_json_file"]);
    $client->setAccessType("offline");
    $client->setPrompt("select_account consent");
    $client->setRedirectUri($gcfConfig["oauth2_redirect_uri"]);

    return $client;
}

/*
 * We treat the access token object as an opaque blob because Google APIs
 * do all the parsing. But, for reference, it's some JSON that looks
 * like this:
 *
 * {
 * "access_token": "at-long-string",
 * "expires_in": 3600,
 * "refresh_token": "its-a-long-string",
 * "scope": "openid https://www.googleapis.com/auth/contacts.readonly https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email",
 * "token_type": "Bearer",
 * "id_token": "its-a-very-very-long-string",
 * "created": 1572725633
 * }
 */

/**
 * Load a previously saved access token blob from persistent storage
 * This function is not responsible for validating the token blob in any way.
 */
function loadOauth2TokenBlob(&$ctx)
{
    $result = callJavaApplication($ctx, "LOAD_TOKEN_BLOB");
    if ($result == NULL) {
        return;
    }
    if (! isSuccessJavaCall($ctx, $result)) {
        throw new Exception("Unable to obtain token blob."); // shouldn't happen
    }
    unset($result[0]);
    foreach ($result as $line) {
        if ($line[0] == '=') {
            $encodedTokenBlob = substr($line, 1);
            break;
        }
    }

    $tokenBlob = base64_decode($encodedTokenBlob);
    if (strcmp($tokenBlob, "??") == 0) {
        $ctx["tokenBlob"] = FALSE;
    } else {
        $ctx["tokenBlob"] = $tokenBlob;
    }
}

function getGoogleIdentity(&$ctx)
{
    $ctx["googleName"] = NULL;
    $ctx["googleEmailAddress"] = NULL;
    $ctx["googleResourceName"] = NULL;
    $ctx["googlePictureUrl"] = NULL;

    $client = getGoogleClient($ctx);
    if (! $client) {
        return;
    }
    $tokenBlob = $ctx["tokenBlob"];
    if ($tokenBlob) {
        $client->setAccessToken(json_decode($tokenBlob, true));
    } else {
        return;
    }

    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $creds = $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            if (array_key_exists('error', $creds)) {
                logIncident("Error during Oauth2 access token refresh: " . json_encode($creds));
                // This most likely happens when we have a token that has been revoked, such
                // as when the user revokes authorization on Google instead of here.
                actionDeauthorize($ctx);
                return;
            }
        }
    }

    $service = new Google_Service_PeopleService($client);
    $optParams = ['personFields' => 'names,emailAddresses,photos'];
    // throws if not properly authorized
    try {
        $person = $service->people->get('people/me', $optParams);
    } catch (Throwable $t) {
        actionDeauthorize($ctx);
        return;
    }
    $ctx["googleResourceName"] = $person->getResourceName();

    foreach ($person->getNames() as $name) {
        $ctx["googleName"] = $name->getDisplayName();
        $isPrimary = $name->getMetadata()->getPrimary();
        if ($isPrimary) {
            break;
        }
    }

    foreach ($person->getEmailAddresses() as $emailAddress) {
        $ctx["googleEmailAddress"] = $emailAddress->getValue();
        $isPrimary = $emailAddress->getMetadata()->getPrimary();
        if ($isPrimary) {
            break;
        }
    }

    foreach ($person->getPhotos() as $photo) {
        $ctx["googlePictureUrl"] = $photo->getUrl();
        $isPrimary = $photo->getMetadata()->getPrimary();
        if ($isPrimary) {
            break;
        }
    }
}

/**
 * This value is used when we call the Java process to prove we are
 * allowed to do so, because we know the shared secret.  It's not
 * air-tight (replayable), but the danger that it protects against is not very
 * great. If you change this, make sure it's a single token
 * (must avoid white space). It also has to match what the companion
 * Java application does to validate it. Only used for calling Java
 * over a pipe. For HTTP calls, we authenticate a different way.
 */
function getLocalProof(&$ctx, $lid)
{
    global $gcfConfig;
    $proof = hash('sha256', $gcfConfig["proof_secret"] . $lid);
    return $proof;
}

/**
 * Given the name of an inline style string, return am HTML CSS style attribute.
 * If the style string does not contain a double quote, the attribute is
 * returned double-quoted. Else, if the style string does not contain a single
 * quote, the attribute is returned single-quoted. If it contains both, we
 * can't cope and return null (so there will be no style attribute).
 */
function stylize($styleName)
{
    if (! $styleName) return NULL;
    global $gcfConfig;
    $cssStyles = $gcfConfig["cssStyles"];
    if (! $cssStyles) return NULL;
    $style = $cssStyles[$styleName];
    if (! $style) return NULL;
    if (strpos($style, "\"") === FALSE) return " style=\"" . $style . "\" ";
    if (strpos($style, "\'") === FALSE) return " style=\'" . $style . "\' ";
    return NULL;
}

function externalLinkIcon()
{
    return "<svg style='border: 1px solid #aaa' " .
               "height=10 width=10 focusable='false'>" .
               "<use xlink:href='#new-window'></use></svg>";
}

/**
 * Because of the processing order of the stuff at the top of this
 * file and the stuff in the companion config file, we have to defer
 * string replacements.  There is probably some much more clever PHP
 * way of handling this situation, but I am not a much more clever PHP
 * guy.
 */
function customizeString($target)
{
    if (! $target) return NULL;
    global $gcfConfig;
    $tokens =
    [
      "REF_SECTION_HEADER_OVERVIEW"   => getReferenceToSectionHeader("overview"),
      "REF_SECTION_HEADER_IDENTITIES" => getReferenceToSectionHeader("identities"),
      "REF_SECTION_HEADER_REFRESH"    => getReferenceToSectionHeader("refresh"),
      "REF_SECTION_HEADER_SHARING"    => getReferenceToSectionHeader("sharing"),
      "REF_SECTION_HEADER_CONSENT"    => getReferenceToSectionHeader("consent"),
      "REF_SECTION_HEADER_LDAPHOWTO"  => getReferenceToSectionHeader("ldaphowto"),
      "EXTERNAL_LINK_ICON"            => externalLinkIcon(),
      "LOCAL_FEATURE_NAME"            => $gcfConfig["local_feature_name"],
      "LOCAL_SITE_NAME"               => $gcfConfig["local_site_name"], // do last so it can replace into earlier ones
    ];
    foreach ($tokens as $token => $value) {
        $target = str_replace($token, $value, $target);
    }
    return $target;
}

/**
 * adapted from https://maxchadwick.xyz/blog/stripping-a-query-parameter-from-a-url-in-php
 * Takes a URI query string part, removes the named item, and returns
 * the reconstructed URI query string.
 */
function httpStripQueryParam(&$ctx, $queryString, $parameterName)
{
    $query = [];
    parse_str($queryString, $query);
    if ($parameterName == "gcf_") {
        foreach ($query AS $key => $value) {
            if (substr_compare("gcf_", $key, 0, 4) != 0) {
                $q[$key] = $value;
            }
        }
        $query = $q;
    } else {
        if (! isset($query[$parameterName])) {
            return $queryString;
        }
        unset($query[$parameterName]);
    }
    return http_build_query($query);
}

/**
 * copied from https://www.php.net/manual/en/function.base64-encode.php#103849
 */
function base64urlEncode($data)
{
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64urlDecode($data)
{
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function logIncident($arg)
{
    // just trying to generate a unique string that will be suitable for user communications
    $incident = random_int(1000, 9999) . "-" . strtoupper(base64urlEncode(random_int(1000, 9999)));

    error_log($incident . " Logged Incident ID; " . date(DATE_COOKIE));

    if (is_string($arg)) {
        error_log($incident . " " . $arg);
    } else { // exception
        error_log($incident . ' Caught exception: ' . $arg->getMessage());
        error_log($incident . ' ' . $arg->getTraceAsString());
    }

    return $incident;
}

function exceptionHandler($e)
{
    $incident = logIncident($e);
    echo "<p " . stylize("incident") . ">";
    global $gcfConfig;
    echo customizeString($gcfConfig["strings"]["incident"]);
    echo "<b>{$incident}</b>.</p>";
    emitContinueButton();
}

/**
 * I put this way down here because the heredoc format makes funky indentation.
 */
function emitScripting($activeSection)
{
    global $gcfConfig;
    $s = $gcfConfig["cssStyles"];
    $abs = ($s ? $s["section_tab_selected"] : NULL);
    $ibs = ($s ? $s["section_tab_nonselected"] : NULL);
    echo "<script>" .
         "gcfActiveButtonStyle = \"" . $abs . "\";\n" .
         "gcfInactiveButtonStyle = \"" . $ibs . "\";\n" .
         "";

    print <<<END
      function openSection(sectionName) {
        var i, sectioncontent, sectionlinks, sectionButtonId;
        sectionButtonId = sectionName + "Button";
        sectioncontent = document.getElementsByClassName("sectioncontent");
        for (i = 0; i < sectioncontent.length; i++) {
          sectioncontent[i].style.display = "none";
        }
        sectionlinks = document.getElementsByClassName("sectionlinks");
        for (i = 0; i < sectionlinks.length; i++) {
          sectionlinks[i].className = sectionlinks[i].className.replace(" active", "");
          sectionlinks[i].style = gcfInactiveButtonStyle;
        }
        document.getElementById(sectionName).style.display = "block";
        document.getElementById(sectionButtonId).className += " active";
        document.getElementById(sectionButtonId).style = gcfActiveButtonStyle;
      }
      function invert(that) {
        if (that.className.indexOf(" active") >= 0) {
          return;
        }
        var style = that.style;
        var temp;
        temp = style.color;
        style.color = style.backgroundColor;
        style.backgroundColor = temp;
      }
END;

    echo "\nopenSection(\"" . $activeSection . "\");\n</script>\n";
}

?>
