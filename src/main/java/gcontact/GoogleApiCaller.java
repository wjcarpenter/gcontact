package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.model.ContactGroup;
import com.google.api.services.people.v1.model.ContactGroupMembership;
import com.google.api.services.people.v1.model.ContactGroupMetadata;
import com.google.api.services.people.v1.model.EmailAddress;
import com.google.api.services.people.v1.model.ListConnectionsResponse;
import com.google.api.services.people.v1.model.ListContactGroupsResponse;
import com.google.api.services.people.v1.model.Membership;
import com.google.api.services.people.v1.model.Name;
import com.google.api.services.people.v1.model.Organization;
import com.google.api.services.people.v1.model.Person;
import com.google.api.services.people.v1.model.PersonMetadata;
import com.google.api.services.people.v1.model.PhoneNumber;
import com.google.api.services.people.v1.model.Photo;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.OAuth2Credentials;
import com.google.auth.oauth2.OAuth2Credentials.CredentialsChangedListener;
import com.google.auth.oauth2.UserCredentials;
import gcontact.Database.GoogleApiNecessities;
import gcontact.Database.PrimaryItems;
import gcontact.Database.Tx;
import gcontact.Utils.Config;
import gcontact.Utils.TxGranularity;

class GoogleApiCaller {
  private static final String CN = GoogleApiCaller.class.getName();
  private static final Logger logger = Logger.getLogger(GoogleApiCaller.CN);
  private static Map<String,GoogleApiCaller> callers = new HashMap<>();
  static GoogleApiCaller getInstance(GoogleApiNecessities gan) {
    final String localId = gan.getLocalId();
    GoogleApiCaller gac = GoogleApiCaller.callers.get(localId);
    if (gac == null) {
      gac = new GoogleApiCaller(gan);
      GoogleApiCaller.callers.put(localId, gac);
    }
    return gac;
  }

  private final String localId;
  private final GoogleApiNecessities gan;
  private Database db = null;
  private Tx tx = null;
  private TxGranularity txGranularity = TxGranularity.LARGE;
  private FetchSummary fetchSummary = null;
  private final Config config = Config.getInstance(null);
  private UserCredentials gCreds = null;
  private GoogleApiCaller(GoogleApiNecessities gan) {
    this.localId = gan.getLocalId();  // for convenience, since we use it a lot
    this.gan = gan;
  }

  String exchangeAuthorizationCode(String authCode) throws IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "exchangeAuthorizationCode", authCode);
    try {
      final String googleClientId = config.getGoogleClientId();
      final String googleClientSecret = config.getGoogleClientSecret();
      final GoogleAuthorizationCodeTokenRequest gactr = new GoogleAuthorizationCodeTokenRequest(
          new NetHttpTransport(), JacksonFactory.getDefaultInstance(), googleClientId, googleClientSecret,
          authCode, config.getOauth2RedirectUrl());
      gactr.setClientAuthentication(new ClientParametersAuthentication(googleClientId, googleClientSecret));
      final TokenResponse response = gactr.execute();
      if (response.get("created") == null) {
        final long nowInSeconds = System.currentTimeMillis() / 1000L;
        response.set("created", nowInSeconds);
      }
      final String jsonString = GoogleApiCaller.objectToJsonString(response);
      GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "exchangeAuthorizationCode", jsonString);
      return jsonString;
    } catch (final TokenResponseException e) {
      Utils.logProblemAndPunt(e);
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "exchangeAuthorizationCode-NOT-REACHED");
    return null; //not reached
  }

  /**
   * Creates an authorized Credential object.
   */
  private UserCredentials getCredentials(NetHttpTransport HTTP_TRANSPORT) throws IOException, GeneralSecurityException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "getCredentials");
    final String tokenBlob = gan.getTokenBlob();
    if (tokenBlob == null) {
      throw new IllegalStateException("The user was not found in the gcontact database.");
    }
    final GoogleTokenResponse tokenResponse = GoogleApiCaller.jsonToTokenResponse(tokenBlob);
    final AccessToken accessToken = new AccessToken(tokenResponse.getAccessToken(), GoogleApiCaller.trueExpirationAsDate(tokenResponse));
    final UserCredentials gCreds = UserCredentials.newBuilder()
        .setAccessToken(accessToken)
        .setRefreshToken(tokenResponse.getRefreshToken())
        .setClientId(config.getGoogleClientId())
        .setClientSecret(config.getGoogleClientSecret())
        .build();
    gCreds.addChangeListener(new GcCredentialsRefreshListener(localId, tokenResponse));
    gCreds.refreshIfExpired();
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "getCredentials", gCreds);
    return gCreds;
  }

  private PeopleService getPeopleService() throws GeneralSecurityException, IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "getPeopleService");
    // Build a new authorized API client service.
    final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    final PeopleService peopleService = new PeopleService.Builder(httpTransport, JacksonFactory.getDefaultInstance(), null)
        .setApplicationName(config.getApplicationName())
        .build();
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "getPeopleService", peopleService);
    return peopleService;
  }


  /**
   * Although we and LDAP model a group as a collection of people, Google
   * models a group as as sort of annotation on an person. You can get a list of
   * members in a group from Google APIs, but you can also get the list of
   * groups when you get the Google person object. There is a separate group
   * definition, which holds the group name and sundry details. It is referenced
   * by the memberships collection hanging off of a person.
   *
   * A few observations and side effects of the models and how we process
   * things:
   *
   * * We get an update to a group when there is a change to the group
   *   definition (for example, a name change). We do not get an update
   *   for the group simply because its membership changed.
   *
   * * When we fetch the list of groups, it would be possible to receive a
   *   record for an empty group. I have only seen this happen for the
   *   special group "All", which I filter out in code. (I filter it out
   *   because of the reasonable expectation that it would contain all of
   *   the group members, which it does if you fetch memberships from the
   *   group object. However, because of the way we process things, it's
   *   empty, and that would surprise most users.) For other groups, if they
   *   are actually empty, it matches what the user sees in the Google UI.
   *
   * * When a group's membership changes, we get updated contact records for
   *   the contacts added or removed from the group. Since we get a complete
   *   set of memberships with those contact records, we simply populate them
   *   all (except for the special "All" group, which is not in the list).
   *
   * * Google UI has a thing called "hide from contacts", which I believe means
   *   the contact is removed from the "MyContacts" system group. If the
   *   contact is a member of some other group, they still show up with
   *   that membership. However, if they are not a member of any other groups,
   *   they show up in the Google UI in "other contacts", which can include
   *   various other people not in your contacts list.
   */
  FetchSummary fetchContactsAndGroups(boolean doIncrementalRefresh) throws GeneralSecurityException, IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "fetchContactsAndGroups", doIncrementalRefresh);
    fetchSummary = new FetchSummary();
    db = Database.getInstance();
    final String tokenBlob = gan.getTokenBlob();
    if (tokenBlob == null) {
      throw new IllegalStateException("The user '" + localId + "' was not found in the gcontact database.");
    }
    txGranularity = config.getTxGranularity();
    tx = (txGranularity == TxGranularity.NONE ? null : db.getTx());
    if (!doIncrementalRefresh  ||  gan.getContactsSyncToken() == null) {
      // full refresh of contacts
      doIncrementalRefresh = false;
    }
    if (!doIncrementalRefresh  ||  gan.getContactGroupsSyncToken() == null) {
      // full refresh of contact groups
      doIncrementalRefresh = false;
    }
    if (!doIncrementalRefresh) {
      db.deleteSubordinateData(tx, localId);
    }

    fetchSummary.setIncrementalRefresh(doIncrementalRefresh);
    final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    gCreds = getCredentials(httpTransport);
    final PeopleService peopleService = getPeopleService();

    final String contactsGroupSyncToken = fetchContactGroups(peopleService, doIncrementalRefresh);
    final String contactsSyncToken = fetchContacts(peopleService, doIncrementalRefresh);

    db.updateFetchInformation(tx, localId, contactsSyncToken, contactsGroupSyncToken);
    try {
      if (tx != null  &&  !tx.isClosed()) tx.commit();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "fetchContactsAndGroups", fetchSummary);
    return fetchSummary;
  }

  private String fetchContacts(PeopleService peopleService, boolean doIncrementalRefresh) throws IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "fetchContacts", doIncrementalRefresh);
    final com.google.api.services.people.v1.PeopleService.People.Connections.List contactsRequest = peopleService.people().connections()
        .list("people/me")
        .setRequestSyncToken(true)
        .setPageSize(config.getFetchPageSize())
        .setAccessToken(gCreds.getAccessToken().getTokenValue())
        .setPersonFields("names,emailAddresses,phoneNumbers,organizations,memberships,photos,metadata")
        ;
    String contactsSyncToken = gan.getContactsSyncToken();
    String pageToken = null;
    if (doIncrementalRefresh) {
      contactsRequest.setSyncToken(contactsSyncToken);
    }
    do {
      contactsRequest.setPageToken(pageToken);
      ListConnectionsResponse response;
      try {
        response = contactsRequest.execute();
        contactsSyncToken = response.getNextSyncToken();
        pageToken = response.getNextPageToken();
      } catch (final Throwable t) {
        // sync token could be expired or otherwise corrupt;
        // do a full refresh instead
        if (doIncrementalRefresh) {
          GoogleApiCaller.logger.info("Ignoring expired or corrupt contacts sync token for '" + localId + "': " + t);
          return fetchContacts(peopleService, false);
        } else {
          throw t;
        }
      }
      try {
        processOnePageOfContacts(response);
        // for SMALL tx granularity, we commit after each fetched page and then
        // start a new transaction (we're not using savepoints).
        if (txGranularity == TxGranularity.SMALL) {
          tx.commit();
          tx = db.getTx();
        }
        // we already checked this before we started, but what if this takes a long time?
        gCreds.refreshIfExpired();
      } catch (final SQLException e) {
        try {
          if (tx != null) tx.rollback();
        } catch (final SQLException ee) {
          Utils.logDbProblemAndPunt(ee);
        }
        Utils.logDbProblemAndPunt(e);
      }
    } while (pageToken != null);
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "fetchContacts", contactsSyncToken);
    return contactsSyncToken;
  }

  private String fetchContactGroups(PeopleService peopleService, boolean doIncrementalRefresh) throws IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "fetchContactGroups", doIncrementalRefresh);
    // the API for getting contact groups is not quite symmetric with the API for getting contacts
    final com.google.api.services.people.v1.PeopleService.ContactGroups.List contactGroupsRequest = peopleService.contactGroups().list()
        .setPageSize(config.getFetchPageSize())
        .setAccessToken(gCreds.getAccessToken().getTokenValue())
        ;
    String contactGroupsSyncToken = gan.getContactGroupsSyncToken();
    String pageToken = null;
    if (doIncrementalRefresh) {
      contactGroupsRequest.setSyncToken(contactGroupsSyncToken);
    }
    do {
      contactGroupsRequest.setPageToken(pageToken);
      ListContactGroupsResponse response;
      try {
        response = contactGroupsRequest.execute();
        contactGroupsSyncToken = response.getNextSyncToken();
        pageToken = response.getNextPageToken();
      } catch (final Throwable t) {
        // sync token could be expired or otherwise corrupt;
        // do a full refresh instead
        if (doIncrementalRefresh) {
          GoogleApiCaller.logger.info("Ignoring expired or corrupt contact groups sync token for '" + localId + "': " + t);
          return fetchContactGroups(peopleService, false);
        } else {
          throw t;
        }
      }
      try {
        processOnePageOfContactGroups(peopleService, response);
        // for SMALL tx granularity, we commit after each fetched page and then
        // start a new transaction (we're not using savepoints).
        if (txGranularity == TxGranularity.SMALL) {
          tx.commit();
          tx = db.getTx();
        }
        // we already checked this before we started, but what if this takes a long time?
        gCreds.refreshIfExpired();
      } catch (final SQLException e) {
        try {
          if (tx != null) tx.rollback();
        } catch (final SQLException ee) {
          Utils.logDbProblemAndPunt(ee);
        }
        Utils.logDbProblemAndPunt(e);
      }
    } while (pageToken != null);
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "fetchContactGroups", contactGroupsSyncToken);
    return contactGroupsSyncToken;
  }

  private void processOnePageOfContacts(ListConnectionsResponse response) throws SQLException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processOnePageOfContacts");
    final List<Person> connections = response.getConnections();
    if (connections != null && connections.size() > 0) {
      if (GoogleApiCaller.logger.isLoggable(Level.FINE)) {
        GoogleApiCaller.logger.fine("Contacts page contains " + connections.size() + " items");
      }
      for (final Person person : connections) {
        processOneContact(person);
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processOnePageOfContacts");
  }

  private void processOnePageOfContactGroups(PeopleService peopleService, ListContactGroupsResponse response) throws SQLException, IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processOnePageOfContactGroups");
    final List<ContactGroup> contactGroups = response.getContactGroups();
    if (contactGroups != null && contactGroups.size() > 0) {
      if (GoogleApiCaller.logger.isLoggable(Level.FINE)) {
        GoogleApiCaller.logger.fine("ContactGroups page contains " + contactGroups.size() + " items");
      }
      for (final ContactGroup contactGroup : contactGroups) {
        processOneContactGroup(peopleService, contactGroup);
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processOnePageOfContactGroups");
  }

  private void processOneContactGroup(PeopleService peopleService, ContactGroup contactGroup) throws IOException {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processOneContactGroup", contactGroup);
    final String contactGroupId = contactGroup.getResourceName();
    if ("contactGroups/all".equals(contactGroupId)) {
      // We skip this special group because it doesn't get listed
      // among a contact's group memberships. With the way we process
      // things, it doesn't end up with any members in the database.
      return;
    }
    final boolean doingIncrementalRefresh = fetchSummary.isIncrementalRefresh();
    if (doingIncrementalRefresh) {
      // AFAICT, for incremental fetches, there is no way to distinguish between a
      // newly added contact group and an updated group contact. So, delete it and ignore the result.
      // The theory is that the cost is not too high.
      // We only need this for incremental, but no functional harm for full if we later need it.
      db.deleteContactGroup(tx, localId, contactGroupId);
    }
    final boolean isDeleted = isDeletedContactGroup(doingIncrementalRefresh, contactGroup);
    if (isDeleted) {
      fetchSummary.incrementContactGroupsDeleted();
    } else {
      final String contactGroupName = contactGroup.getName();
      final String contactGroupType = contactGroup.getGroupType();
      final String contactGroupFormattedName = contactGroup.getFormattedName();
      db.insertContactGroup(tx, localId, contactGroupId, contactGroupName, contactGroupFormattedName, contactGroupType);
      fetchSummary.incrementContactGroupsAdded();
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processOneContactGroup");
  }

  private void processOneContact(Person person) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processOneContact", person);
    final String contactId = person.getResourceName();
    final boolean doingIncrementalRefresh = fetchSummary.isIncrementalRefresh();
    if (doingIncrementalRefresh) {
      // AFAICT, for incremental fetches, there is no way to distinguish between a
      // newly added contact and an updated contact. So, delete it and ignore the result.
      // We only need this for incremental, but no functional harm for full.
      db.deleteContact(tx, localId, contactId);
    }
    final boolean isDeleted = isDeletedContact(doingIncrementalRefresh, person);
    if (isDeleted) {
      fetchSummary.incrementContactsDeleted();
    } else {
      db.insertContact(tx, localId, contactId);
      fetchSummary.incrementContactsAdded();
      final PrimaryItems pItems = new PrimaryItems();
      processListOfNames(person, contactId, pItems);
      processListOfOrganizations(person, contactId, pItems);
      processListOfEmailAddresses(person, contactId, pItems);
      processListOfPhoneNumbers(person, contactId, pItems);
      if (config.getResolvePhotos()) {
        processListOfPhotos(person, contactId, pItems);
      }
      processListOfMemberships(person, contactId, pItems);
      db.updatePrimaryItems(tx, localId, contactId, pItems);
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processOneContact");
  }

  private boolean isDeletedContact(boolean doingIncrementalRefresh, Person person) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "isDeletedContact", person);
    boolean isDeleted = false;
    if (!doingIncrementalRefresh) {
      isDeleted = false;
    } else {
      final PersonMetadata metadata = person.getMetadata();
      if (metadata != null) {
        final Boolean metaDeleted = metadata.getDeleted();
        if (metaDeleted != null  &&  metaDeleted) {
          isDeleted = true;
          GoogleApiCaller.logger.fine("isDeletedContact because of PersonMetadata");
        }
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "isDeletedContact", isDeleted);
    return isDeleted;
  }

  private boolean isDeletedContactGroup(boolean doingIncrementalRefresh, ContactGroup contactGroup) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "isDeletedContactGroup", contactGroup);
    // Google says I should just check the "deleted" value of the ContactGroupMetadata,
    // which is only populated for incremental fetches. That seems pretty reliable
    // for contact groups (unlike for contacts).
    boolean isDeleted = false;
    if (!doingIncrementalRefresh) {
      isDeleted = false;
    } else {
      final ContactGroupMetadata metadata = contactGroup.getMetadata();
      if (metadata != null) {
        final Boolean metaDeleted = metadata.getDeleted();
        if (metaDeleted != null  &&  metaDeleted) {
          isDeleted = true;
          GoogleApiCaller.logger.fine("isDeletedContactGroup because of ContactGroupMetadata");
        }
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "isDeletedContactGroup", isDeleted);
    return isDeleted;
  }

  private void processListOfPhoneNumbers(Person person, String contactId, PrimaryItems pItems) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processListOfPhoneNumbers", new Object[] {person, contactId});
    final List<PhoneNumber> phoneNumbers = person.getPhoneNumbers();
    int count = 0;
    if (phoneNumbers != null) {
      for (final PhoneNumber phNumber : phoneNumbers) {
        final Boolean isPrimary = phNumber.getMetadata().getPrimary();
        final String phoneNumber = phNumber.getValue();
        final String canonicalForm = phNumber.getCanonicalForm();
        final String type = phNumber.getType();
        db.insertPhoneNumber(tx, localId, contactId, phoneNumber, isPrimary, canonicalForm, type);
        if (isPrimary != null  &&  isPrimary) {
          pItems.setPhoneNumber(phoneNumber);
          pItems.setCanonicalForm(canonicalForm);
        }
        ++count;
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processListOfPhoneNumbers", count);
  }

  private void processListOfPhotos(Person person, String contactId, PrimaryItems pItems) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processListOfPhotos", new Object[] {person, contactId});
    final List<Photo> photos = person.getPhotos();
    int count = 0;
    if (photos != null) {
      for (final Photo photo : photos) {
        final String photoUrl = photo.getUrl();
        Boolean isPrimary = photo.getMetadata().getPrimary();
        if (isPrimary == null) {
          isPrimary = false;
        }
        Boolean isDefault = photo.getDefault();
        if (isDefault == null) {
          isDefault = false;
        }
        if (isDefault  &&  !isPrimary) {
          // default photos that are not also primary photos are a waste of time
          GoogleApiCaller.logger.fine("skipping '" + photoUrl + "', isDefault but not isPrimary");
          continue;
        }
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String originalContentType, contentType;
        byte[] photoBytes;
        try {
          originalContentType = resolvePhotoUrl(photoUrl, baos);
          contentType = originalContentType;
          photoBytes = baos.toByteArray();
          if (photoBytes.length == 0) {
            photoBytes = null;
          }
          final boolean isItJpeg = originalContentType.toLowerCase().contains("jpeg") || originalContentType.toLowerCase().contains("jpg");
          if (photoBytes != null  &&  config.getConvertPhotosToJpeg()  &&  !isItJpeg) {
            final byte[] jpegOutput = Utils.toJpeg(photoBytes, originalContentType);
            if (jpegOutput != null) {
              photoBytes = jpegOutput;
              contentType = "image/jpeg";
            }
          }
        } catch (final IOException e) {
          originalContentType = null;
          contentType = null;
          photoBytes = null;
          GoogleApiCaller.logger.info("Exception trying to resolve photo URL '" + photoUrl + "'; skipping: " + e);
        }
        db.insertPhoto(tx, localId, contactId, photoUrl, contentType, originalContentType, isDefault, photoBytes, isPrimary);
        if (isPrimary != null  &&  isPrimary) {
          pItems.setPhotoBytes(photoBytes);
        }
        ++count;
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processListOfPhotos", count);
  }

  private String resolvePhotoUrl(String photoUrl, ByteArrayOutputStream baos) throws IOException {
    final URL url = new URL(photoUrl);
    final long before = System.currentTimeMillis();
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    con.setConnectTimeout((int) config.getResolvePhotosConnectionTimeout().toMillis());
    con.setReadTimeout((int) config.getResolvePhotosReadTimeout().toMillis());
    try (InputStream in = con.getInputStream()) {
      int count;
      final byte[] buf = new byte[1024];
      while ((count = in.read(buf)) > 0) {
        baos.write(buf, 0, count);
      }
      return con.getContentType();
    } finally {
      final long after = System.currentTimeMillis();
      GoogleApiCaller.logger.fine("resolving '" + photoUrl + "', bytes=" + baos.size() + ", ms=" + (after-before));
    }
  }

  private void processListOfEmailAddresses(Person person, String contactId, PrimaryItems pItems) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processListOfPhoneNumbers", new Object[] {person, contactId});
    final List<EmailAddress> emailAddresses = person.getEmailAddresses();
    int count = 0;
    if (emailAddresses != null) {
      for (final EmailAddress ema : emailAddresses) {
        final Boolean isPrimary = ema.getMetadata().getPrimary();
        final String emailAddress = ema.getValue();
        final String displayName = ema.getDisplayName();
        final String type = ema.getType();
        db.insertEmailAddress(tx, localId, contactId, emailAddress, displayName, isPrimary, type);
        if (isPrimary != null  &&  isPrimary) {
          pItems.setEmailAddress(emailAddress);
        }
        ++count;
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processListOfPhoneNumbers", count);
  }

  private void processListOfOrganizations(Person person, String contactId, PrimaryItems pItems) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processListOfOrganizations", new Object[] {person, contactId});
    final List<Organization> organizations = person.getOrganizations();
    int count = 0;
    if (organizations != null) {
      for (final Organization org : organizations) {
        final Boolean isPrimary = org.getMetadata().getPrimary();
        final Boolean isCurrent = org.getCurrent();
        // doesn't make sense, but Google contacts can have completely empty
        // organization entries; maybe the result of some odd import process or something,
        // or maybe one of the fields I don't care about has something ... still weird
        // "I rode in the desert in an Org with no name."
        final String name = org.getName();
        final String department = org.getDepartment();
        final String title = org.getTitle();
        final String type = org.getType();
        db.insertOrganization(tx, localId, contactId, name, department, title, isCurrent, isPrimary, type);
        if (isPrimary != null  &&  isPrimary) {
          pItems.setDepartment(department);
        }
        ++count;
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processListOfOrganizations", count);
  }

  private void processListOfNames(Person person, String contactId, PrimaryItems pItems) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processListOfNames", new Object[] {person, contactId});
    final List<Name> names = person.getNames();
    int count = 0;
    if (names != null) {
      for (final Name name : names) {
        final Boolean isPrimary = name.getMetadata().getPrimary();
        final String displayName = name.getDisplayName();
        final String prefix = name.getHonorificPrefix();
        final String givenName = name.getGivenName();
        final String middleName = name.getMiddleName();
        final String familyName = name.getFamilyName();
        final String suffix = name.getHonorificSuffix();
        db.insertDisplayName(tx, localId, contactId, displayName, isPrimary, prefix, givenName, middleName, familyName, suffix);
        if (isPrimary != null  &&  isPrimary) {
          pItems.setDisplayName(displayName);
          pItems.setGivenName(givenName);
          pItems.setFamilyName(familyName);
        }
        ++count;
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processListOfNames", count);
  }

  private void processListOfMemberships(Person person, String contactId, PrimaryItems pItems) {
    GoogleApiCaller.logger.entering(GoogleApiCaller.CN, "processListOfMemberships", new Object[] {person, contactId});
    final List<Membership> memberships = person.getMemberships();
    int count = 0;
    if (memberships != null) {
      for (final Membership membership : memberships) {
        final ContactGroupMembership contactGroupMembership = membership.getContactGroupMembership();
        final String contactGroupId = contactGroupMembership.getContactGroupResourceName();
        db.insertContactGroupMember(tx, localId, contactGroupId, contactId);
        ++count;
      }
    }
    GoogleApiCaller.logger.exiting(GoogleApiCaller.CN, "processListOfMemberships", count);
  }

  /**
   * We have this subclass because Jackson refuses to assign what it thinks is an int
   * value to a long field. We accept it as an int and later set the long value with
   * an explicit cast.
   */
  private static class MyTokenResponse extends GoogleTokenResponse {
    @Key("expires_in")
    public int expiresInSeconds;
    @Override
    public boolean equals(Object o) {
      return super.equals(o); // just to keep spotbugs happy
    }
    @Override
    public int hashCode() {
      return super.hashCode(); // just to keep spotbugs happy
    }
  }

  private static GoogleTokenResponse jsonToTokenResponse(String jsonInString) throws JsonParseException, JsonMappingException, IOException {
    final ObjectMapper mapper = new ObjectMapper();
    final MyTokenResponse tokenResponse = mapper.readValue(jsonInString, MyTokenResponse.class);
    tokenResponse.setExpiresInSeconds((long) tokenResponse.expiresInSeconds);
    return tokenResponse;
  }

  private static String objectToJsonString(Object o) throws JsonGenerationException, JsonMappingException, IOException {
    final ObjectMapper mapper = new ObjectMapper();
    final String s = mapper.writeValueAsString(o);
    return s;
  }

  private static Date trueExpirationAsDate(TokenResponse tokenResponse) {
    final long tokExpiresInMillis = tokenResponse.getExpiresInSeconds() * 1000L;
    // this value, which has no accessor, is returned as an Integer
    // beware of integer overflow when messing with it.
    Object created = tokenResponse.get("created");
    if (created == null) {
      created = 0;
    }
    final long createdInMillis = ((int) created) * 1000L;
    final long trueExpiration = (createdInMillis + tokExpiresInMillis);
    return new Date(trueExpiration);
  }

  private static class GcCredentialsRefreshListener implements CredentialsChangedListener {
    private static final String CN = GcCredentialsRefreshListener.class.getName();
    private static Logger logger = Logger.getLogger(GcCredentialsRefreshListener.CN);
    private final String localId;
    private final GoogleTokenResponse tokenResponse;

    GcCredentialsRefreshListener(String localId, GoogleTokenResponse tokenResponse) {
      this.localId = localId;
      this.tokenResponse = tokenResponse;
    }

    @Override
    public void onChanged(OAuth2Credentials credentials) throws IOException {
      GcCredentialsRefreshListener.logger.entering(GcCredentialsRefreshListener.CN, "onChanged", new Object[] {localId, credentials});
      if (!(credentials instanceof UserCredentials)) {
        final String message = "Expected " + UserCredentials.class.getName() + " but saw " + credentials.getClass().getName();
        throw new GContactException(message, null, ExitCode.OTHER);
      }
      final UserCredentials gCreds = (UserCredentials) credentials;
      final String returnedRefreshToken = gCreds.getRefreshToken();
      if (returnedRefreshToken != null) {
        tokenResponse.setRefreshToken(returnedRefreshToken);
      }
      final AccessToken returnedAccessToken = gCreds.getAccessToken();
      tokenResponse.setAccessToken(returnedAccessToken.getTokenValue());
      final long returnedExpirationInMillis = returnedAccessToken.getExpirationTime().getTime();
      final long nowInMillis = System.currentTimeMillis();
      final long expiresInSeconds = (returnedExpirationInMillis - nowInMillis) / 1000L;
      final long createdInSeconds = nowInMillis / 1000L;
      tokenResponse.setExpiresInSeconds(expiresInSeconds);
      tokenResponse.set("created", createdInSeconds);
      final String tokenBlob = GoogleApiCaller.objectToJsonString(tokenResponse);
      final Database db = Database.getInstance();
      // This happens outside of the surrounding transaction, on a separate DB connection
      db.updateTokenBlob(null, localId, tokenBlob);
      GcCredentialsRefreshListener.logger.exiting(GcCredentialsRefreshListener.CN, "onChanged", new Object[] {localId, tokenBlob});
    }
  }

  static class FetchSummary {
    //private static final String CN = FetchSummary.class.getName();
    //private static Logger logger = Logger.getLogger(CN);
    private int contactsDeleted      = 0;
    private int contactsAdded        = 0;
    private int contactGroupsDeleted = 0;
    private int contactGroupsAdded   = 0;
    private boolean isIncrementalRefresh = true;
    private FetchSummary() {}
    int getContactsDeleted()             {return contactsDeleted;}
    void incrementContactsDeleted()      {contactsDeleted++;}
    int getContactsAdded()               {return contactsAdded;}
    void incrementContactsAdded()        {contactsAdded++;}
    int getContactGroupsDeleted()        {return contactGroupsDeleted;}
    void incrementContactGroupsDeleted() {contactGroupsDeleted++;}
    int getContactGroupsAdded()          {return contactGroupsAdded;}
    void incrementContactGroupsAdded()   {contactGroupsAdded++;}

    boolean isIncrementalRefresh() {
      return isIncrementalRefresh;
    }
    void setIncrementalRefresh(boolean isIncrementalRefresh) {
      this.isIncrementalRefresh = isIncrementalRefresh;
    }
  }
}
