package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

class GContactException extends RuntimeException {
  private static final long serialVersionUID = 2323118534160405572L;
  private final ExitCode ec;

  public GContactException(String message, Throwable cause, ExitCode ec) {
    super(message, cause);
    this.ec = ec;
  }

  ExitCode getExitCode() {
    return ec;
  }

  int getExitCodeValue() {
    if (ec == null) return 0;
    return ec.value();
  }
}
