package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import gcontact.Database.GoogleApiNecessities;
import gcontact.GoogleApiCaller.FetchSummary;

/**
 * In command line invocations, a series of actions can be requested. Every command
 * has a fixed number of arguments (usually one, but sometimes more). The first
 * argument is always the localId of the user on behalf of whom the actions are
 * requested. The command line invoker is the companion PHP script.
 *
 * This class contains static subclasses which implement those actions. By the time we get
 * to actually performing these actions, trustworthy authentication has already been done
 * for the user "localId".
 *
 * Every Action should return a string, using newlines as appropriate.
 * This will typically be displayed inside HTML "pre" tags, so don't try to
 * use any formatting goop in the output.
 */
abstract class Action {
  private static final String CN = Action.class.getName();
  private static final Logger logger = Logger.getLogger(Action.CN);
  protected final String localId;
  protected final String arg2;
  Action(String localId, String arg2) {
    this.localId = localId;
    this.arg2 = arg2;
  }
  abstract String act();

  /**
   * Reports a summary of the number of things we hold locally for the user.
   */
  static class Report extends Action {
    Report(String localId) {
      super(localId, null);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "report", localId);
      final Database db = Database.getInstance();
      final int countOfContacts = db.getCountOfContacts(null, localId);
      final int countOfDisplayNames = db.getCountOfDisplayNames(null, localId);
      final int countOfOrganizations = db.getCountOfOrganizations(null, localId);
      final int countOfEmailAddresses = db.getCountOfEmailAddresses(null, localId);
      final int countOfPhoneNumbers = db.getCountOfPhoneNumbers(null, localId);
      final int countOfPhotos = db.getCountOfPhotos(null, localId);
      final int countOfContactGroups = db.getCountOfContactGroups(null, localId);
      final int countOfContactGroupMembers = db.getCountOfContactGroupMembers(null, localId);
      String result = "";
             result += "\n";
             result += String.format("Contact records          : %,7d", countOfContacts) + "\n";
             result += String.format("Contact names            : %,7d", countOfDisplayNames) + "\n";
             result += String.format("Contact organizations    : %,7d", countOfOrganizations) + "\n";
             result += String.format("Contact email addresses  : %,7d", countOfEmailAddresses) + "\n";
             result += String.format("Contact phone numbers    : %,7d", countOfPhoneNumbers) + "\n";
             result += String.format("Contact photos           : %,7d", countOfPhotos) + "\n";
             result += "\n";
             result += String.format("Contact groups           : %,7d", countOfContactGroups) + "\n";
             result += String.format("Contact group memberships: %,7d", countOfContactGroupMembers) + "\n";
      Action.logger.exiting(Action.CN, "report", result);
      return result;
    }
  }

  /**
   * Calls Google to freshen our local cache of  data. This is an incremental refresh.
   */
  static class Refresh extends Action {
    Refresh(String localId) {
      super(localId, null);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "refresh", localId);
      final String result = Action.refreshOrRestore(localId, true);
      Action.logger.exiting(Action.CN, "refresh", result);
      return result;
    }
  }
  /**
   * Calls Google to freshen our local cache of  data. This is an full refresh.
   */
  static class Restore extends Action {
    Restore(String localId) {
      super(localId, null);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "restore", localId);
      final String result = Action.refreshOrRestore(localId, false);
      Action.logger.exiting(Action.CN, "restore", result);
      return result;
    }
  }
  static String refreshOrRestore(String localId, boolean incrementalRefresh) {
    Action.logger.entering(Action.CN, "refreshOrRestore", new Object[] {localId, incrementalRefresh});
    final Database db = Database.getInstance();
    final GoogleApiNecessities gan = db.getTokenBlob(null, localId);
    final GoogleApiCaller gac = GoogleApiCaller.getInstance(gan);
    try {
      final FetchSummary fetchSummary = gac.fetchContactsAndGroups(incrementalRefresh);
      String result = "Google contacts refreshed for " + localId;
      if (fetchSummary.isIncrementalRefresh()) {
        result += " (incremental refresh)\n";
      } else {
        result += " (full refresh)\n";
      }
      final int contactsDeleted      = fetchSummary.getContactsDeleted();
      final int contactsAdded        = fetchSummary.getContactsAdded();
      final int contactGroupsDeleted = fetchSummary.getContactGroupsDeleted();
      final int contactGroupsAdded   = fetchSummary.getContactGroupsAdded();
      result += String.format("%,d contact%s deleted, %,d contact%s added or modified%n%,d group%s deleted, %,d group%s added or modified%n",
          contactsDeleted,      (contactsDeleted      == 1 ? "" : "s"),
          contactsAdded,        (contactsAdded        == 1 ? "" : "s"),
          contactGroupsDeleted, (contactGroupsDeleted == 1 ? "" : "s"),
          contactGroupsAdded,   (contactGroupsAdded   == 1 ? "" : "s"));
      Action.logger.exiting(Action.CN, "refreshOrRestore");
      return result;
    } catch (GeneralSecurityException | IOException e) {
      Action.logger.throwing(Action.CN, "refreshOrRestore", e);
      Utils.logProblemAndPunt(e);
    }
    Action.logger.exiting(Action.CN, "refreshOrRestore-NOT-REACHED");
    return null; // not reached
  }

  /**
   * Removes all local copies of contact records. We only have to delete
   * the row from the identities table. All others are deleted by cascade deletes.
   */
  static class Purge extends Action {
    Purge(String localId) {
      super(localId, null);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "purge", localId);
      final Database db = Database.getInstance();
      db.deleteIdentity(null, localId);
      final String result = "Local copy of Google contacts have been deleted for " + localId + "\n";
      Action.logger.exiting(Action.CN, "purge", result);
      return result;
    }
  }
  /**
   * Inserts a new record into the identities table because the user has just given OAUth2
   * consent interactively via the PHP script. We have to exchange the short-lived
   * authorization code for a longer-lived access token / refresh token pair.
   */
  static class CreateIdentity extends Action {
    CreateIdentity(String localId, String encodedTokenBlob) {
      super(localId, encodedTokenBlob);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "createIdentity", new Object[] {localId, arg2});
      final byte[] codeBytes = Base64.decodeBase64(arg2);
      String authCode = null;
      authCode = new String(codeBytes, StandardCharsets.UTF_8);
      final GoogleApiNecessities gan = new GoogleApiNecessities(localId);
      final GoogleApiCaller gac = GoogleApiCaller.getInstance(gan);
      String tokenBlob = null;
      try {
        tokenBlob = gac.exchangeAuthorizationCode(authCode);
      } catch (final IOException e) {
        Action.logger.throwing(Action.CN, "createIdentity", e);
        Utils.logProblemAndPunt(e);
      }
      final Database db = Database.getInstance();
      db.insertIdentity(null, localId, tokenBlob);
      final String result = "Created a new entry for local identity " + localId;
      Action.logger.exiting(Action.CN, "createIdentity", result);
      return result;
    }
  }
  /**
   * The PHP script uses this to find out if the user has already gone through the OAuth2
   * authorization consent. It uses the token blob to make a Google API call to get
   * a few user profile details.
   */
  static class LoadTokenBlob extends Action {
    LoadTokenBlob(String localId) {
      super(localId, null);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "loadTokenBlob", localId);
      final Database db = Database.getInstance();
      final GoogleApiNecessities gan = db.getTokenBlob(null, localId);
      String tokenBlob = (gan == null ? null : gan.getTokenBlob());
      if (tokenBlob == null) tokenBlob = "??"; // special signal to the PHP script: user not found
      // the leading "=" is also a signal for the PHP script
      final String encodedTokenBlob = "=" + Base64.encodeBase64String(tokenBlob.getBytes(StandardCharsets.UTF_8));
      String stubbyForLogging = encodedTokenBlob;
      if (encodedTokenBlob.length() > 20) {
        // this is the usual case
        stubbyForLogging = encodedTokenBlob.substring(0, 15) + "...";
      }
      Action.logger.exiting(Action.CN, "loadTokenBlob", stubbyForLogging);
      return encodedTokenBlob;
    }
  }
  /**
   * Report the calling user's existing contact group shares.
   */
  static class ReportShares extends Action {
    ReportShares(String localId) {
      super(localId, null);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "reportShares", localId);
      final Database db = Database.getInstance();
      final List<SharingObject> sharings = db.getShares(null, localId);
      final StringBuilder result = new StringBuilder();
      for (final SharingObject sharingObject : sharings) {
        result.append(sharingObject.isSharedTo() ? ">" : "<").append('\t');
        result.append(sharingObject.isSharedToAccepted() ? "t" : "f").append('\t');
        result.append(sharingObject.getContactGroupId()).append('\t');
        result.append(sharingObject.getContactGroupMemberCount()).append('\t');
        result.append(sharingObject.getContactGroupFormattedName());
        final Set<String> sharesTo = sharingObject.getSharePartners();
        for (final String shareTo : sharesTo) {
          result.append('\t').append(shareTo);
        }
        result.append('\n');
      }

      final String resultString = result.toString();
      Action.logger.exiting(Action.CN, "reportShares", resultString);
      return resultString;
    }
  }
  /**
   * This represents a complete replacement for the user's share records.
   * Delete all the old ones and then add these new ones. Do it in
   * a transaction in case something goes wrong.
   */
  static class UpdateShares extends Action {
    UpdateShares(String localId, String encodedSharesBlob) {
      super(localId, encodedSharesBlob);
    }

    @Override
    String act() {
      Action.logger.entering(Action.CN, "updateShares", new Object[] {localId, arg2});
      String sharesBlob;
      sharesBlob = new String(Base64.decodeBase64(arg2), StandardCharsets.UTF_8);
      final String[] singleShares = sharesBlob.split("\n");

      final Map<SharingObject,SharingObject> shareToSharingObjects = new HashMap<>();
      final Map<SharingObject,SharingObject> shareFromSharingbjects = new HashMap<>();
      for (final String singleShare : singleShares) {
        final String[] shareParts = singleShare.trim().split("\t");
        final String shareType = shareParts[0];
        if ("T".equals(shareType) && shareParts.length < 4) {
          // a contact group with no shared-to users; skip it
          continue;
        }
        if (shareParts.length > 4) {
          throw new GContactException("Unexpected sharing information: " + singleShare, null, ExitCode.ARGS);
        }
        final String contactGroupIdEncoded = shareParts[1];
        String contactGroupId;
        contactGroupId = new String(Base64.decodeBase64(contactGroupIdEncoded), StandardCharsets.UTF_8);
        final String sharingPartnerEncoded = shareParts[2];
        String sharingPartner = "x"; // dummy value
        if (!sharingPartnerEncoded.equals("x")) {
          sharingPartner = new String(Base64.decodeBase64(sharingPartnerEncoded), StandardCharsets.UTF_8);
        }
        final String formValue = shareParts[3];
        if ("T".equals(shareType)) {
          final String sharingUser = localId;
          final String[] receivingUsers = formValue.split("\\s");
          for (String receivingUser : receivingUsers) {
            receivingUser = receivingUser.trim();
            if (receivingUser.length() > 0) {
              final SharingObject sharingObject = new SharingObject();
              sharingObject.setContactGroupId(contactGroupId);
              sharingObject.setSharedTo(true);
              sharingObject.addSharePartner(receivingUser);
              shareToSharingObjects.put(sharingObject, sharingObject);
              Action.logger.finest("Sharing " + sharingUser + " => " + contactGroupId + " =>" + receivingUser);
            }
          }
        } else if ("F".equals(shareType)  &&  "t".equalsIgnoreCase(formValue)) {
          final String receivingUser = localId;
          final SharingObject sharingObject = new SharingObject();
          sharingObject.setContactGroupId(contactGroupId);
          sharingObject.setSharedTo(false);
          sharingObject.addSharePartner(sharingPartner);
          sharingObject.setSharedToAccepted(true);
          shareFromSharingbjects.put(sharingObject, sharingObject);
          Action.logger.finest("Opt-in for " + sharingPartner + " => " + contactGroupId + " => " + receivingUser);
        } else {
          throw new GContactException("Unexpected sharing information: " + singleShare, null, ExitCode.ARGS);
        }
      }

      final Database db = Database.getInstance();
      db.updateShares(localId, shareToSharingObjects, shareFromSharingbjects);

      final String resultString = null;
      Action.logger.exiting(Action.CN, "updateShares", resultString);
      return resultString;
    }
  }
}
