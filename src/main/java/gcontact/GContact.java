package gcontact;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.common.io.LineReader;
import gcontact.Utils.Config;

public class GContact {
  private static final String CN = GContact.class.getName();
  private static Logger logger = Logger.getLogger(GContact.CN);
  static {
    Utils.createIncidentId();
  }
  public static void main(String[] args) {
    GContact.logger.entering(GContact.CN, "main", args);
    try {
      // what happens in gcontact stays in gcontact
      final GContact gc = new GContact(System.in, System.out);
      gc.gatherAndPerformActions(args, null);
    } catch (final GContactException t) {
      System.exit(t.getExitCodeValue());
    } catch (final Throwable t) {
      Utils.logException(null, t);
      System.exit(ExitCode.OTHER.value());
    } finally {
      GContact.logger.exiting(GContact.CN, "main");
    }
  }

  private String localId = null;
  private boolean alreadyAuthenticated = false;
  private String proof = null;
  private final InputStream in;
  private final OutputStream out;
  final List<Action> actions = new ArrayList<>();

  GContact(InputStream in, OutputStream out) {
    this.in = in;
    this.out = out;
  }

  void gatherAndPerformActions(String[] args, String authenticatedLocalId) throws IOException, GContactException {
    if (authenticatedLocalId != null) {
      this.localId = authenticatedLocalId;
      alreadyAuthenticated = true;
    }
    GContact.logger.entering(GContact.CN, "gatherAndPerformActions", args);
    gatherActions(args);
    // we do this because logging up to this point may or may not have
    // actually happened if the config file was named among the
    // command line arguments
    if (GContact.logger.isLoggable(Level.FINER)) {
      final StringBuffer argybargy = new StringBuffer("GCcontact args:");
      for (final String arg : args) {
        argybargy.append(" ").append(arg);
      }
      GContact.logger.finer(argybargy.toString());
    }
    if (!alreadyAuthenticated) {
      Utils.proveIt(localId, proof);
    }

    // Let's do it/them!
    for (final Action action : actions) {
      final String output = action.act();
      if (output != null) {
        out.write(output.getBytes(StandardCharsets.ISO_8859_1));
      }
    }
    GContact.logger.exiting(GContact.CN, "gatherAndPerformActions");
  }

  private void gatherActions(String[] args) throws IOException {
    GContact.logger.entering(GContact.CN, "gatherActions", args);
    final boolean wantInputStream = innerGatherActions(args);
    if (wantInputStream) {
      processInputStream();
    }
    if (localId == null) {
      final String message = "The user's local identity was not provided (--localid).";
      final GContactException t = new GContactException(message, null, ExitCode.ARGS);
      final String incidentId = Utils.logException(null, t);
      out.write(("Invocation problem. Notify administrator, Incident ID " + incidentId + ".").getBytes(StandardCharsets.ISO_8859_1));
      throw t;
    }
    // we don't need proof if there was already authentication
    if (proof == null  &&  !alreadyAuthenticated) {
      final String message = "Security proof was not provided (--proof).";
      final GContactException t = new GContactException(message, null, ExitCode.ARGS);
      final String incidentId = Utils.logException(null, t);
      out.write(("Invocation problem. Notify administrator, Incident ID " + incidentId + ".").getBytes(StandardCharsets.ISO_8859_1));
      throw t;
    }
    if (actions.size() == 0) {
      final String message = "No actions were requested.";
      final GContactException t = new GContactException(message, null, ExitCode.ARGS);
      final String incidentId = Utils.logException(null, t);
      out.write(("Invocation problem. Notify administrator, Incident ID " + incidentId + ".").getBytes(StandardCharsets.ISO_8859_1));
      throw t;
    }
    GContact.logger.exiting(GContact.CN, "gatherActions");
  }

  private boolean innerGatherActions(String[] args) {
    boolean wantInputStream = false;
    for (int ii=0; ii<args.length; ++ii) {
      final String arg = args[ii];
      if (GContact.logger.isLoggable(Level.FINEST)) {
        GContact.logger.finest("arg: " + arg);
      }
      if (arg.equals("-")) {
        wantInputStream = true;
        continue;
      }
      if (arg.equals("--proof") || arg.equals("-p")) {
        proof = args[++ii];
        continue;
      }
      if (arg.equals("--localid") || arg.equals("-l")) {
        localId = args[++ii];
        continue;
      }
      if (arg.equals("--config") || arg.equals("-c")) {
        final String configFileName = args[++ii];
        Config.getInstance(configFileName);
        GContact.logger = Logger.getLogger(GContact.CN);  // probably not needed, but for safety
        continue;
      }
      if (arg.equalsIgnoreCase("REFRESH")) {
        final Action action = new Action.Refresh(localId);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("RESTORE")) {
        final Action action = new Action.Restore(localId);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("REPORT")) {
        final Action action = new Action.Report(localId);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("REPORT_SHARES")) {
        final Action action = new Action.ReportShares(localId);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("UPDATE_SHARES")) {
        final String encodedSharesBlob = args[++ii];
        final Action action = new Action.UpdateShares(localId, encodedSharesBlob);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("PURGE")) {
        final Action action = new Action.Purge(localId);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("CREATE_IDENTITY")) {
        final String encodedTokenBlob = args[++ii];
        final Action action = new Action.CreateIdentity(localId, encodedTokenBlob);
        actions.add(action);
        continue;
      }
      if (arg.equalsIgnoreCase("LOAD_TOKEN_BLOB")) {
        final Action action = new Action.LoadTokenBlob(localId);
        actions.add(action);
        continue;
      }
      final String message = "Invalid argument '" + arg + "'.";
      final GContactException t = new GContactException(message, null, ExitCode.ARGS);
      final String incidentId = Utils.logException(null, t);
      try {
        out.write(("Invocation problem. Notify administrator, Incident ID " + incidentId + ".").getBytes(StandardCharsets.ISO_8859_1));
      } catch (final IOException e) {
        // too deep now; punt
      }
      throw t;
    }
    return wantInputStream;
  }

  private void processInputStream() throws IOException {
    GContact.logger.entering(GContact.CN, "processInputStream");
    final LineReader lr = new LineReader(new InputStreamReader(in, StandardCharsets.ISO_8859_1));
    String line;
    while ((line = lr.readLine()) != null) {
      line = line.trim();
      if (line.length() == 0) {
        continue; // shouldn't happen, but you never know
      }
      final String[] args = line.split("\\s");
      innerGatherActions(args);  // ignore return value
    }
    GContact.logger.exiting(GContact.CN, "processInputStream");
    return;
  }
}
