package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.time.Duration;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import gcontact.Utils.Config;

/**
 * This class exists merely to be the target of the MyVD ExternalCalloutInsert
 * for the search() method defined below. It's used to trigger an incremental
 * refresh if enough time has elapsed for cached data.
 *
 * It's looking for a single item in the calloutArgs, which is a path to the
 * gcontact config file. It always returns true unless it throws out an exception.
 */
public class MyVDCalloutRefresh {  // must be public
  private static final String CN = MyVDCalloutRefresh.class.getName();
  private Logger logger = Logger.getLogger(MyVDCalloutRefresh.CN);
  private Config config;
  public void init(String name, String[] args) { // must be public
    logger = Logger.getLogger(logger.getName() + ":" + name);
    logger.entering(MyVDCalloutRefresh.CN, "init",
        new Object[] {name, ((args == null || args.length == 0)? null : args[0])});
    // This is here so that the start-up of MyVD also kicks off the Refresher service.
    // MyVD configures all of the MyVD inserts immediately and doesn't wait for
    // an incoming LDAP request, so it's just what we want.
    if (args == null  ||  args.length == 0) {
      return;
    }
    final String configPath = args[0];
    config = Config.getInstance(configPath);

    // This starts indepenently running services
    Responder.staticInit(configPath);
    Refresher.staticInit(configPath);
  }

  // why did I call this "inner_search" instead of "innerSearch"?
  private boolean inner_search(String localId, String[] calloutArgs, Object[] originalArgs) {
    logger.entering(MyVDCalloutRefresh.CN, "inner_search",
        new Object[] {localId, ((calloutArgs == null  ||  calloutArgs.length == 0)? null : calloutArgs[0])});
    final Duration ttl = config.getContactsCacheTtl();
    final Database db = Database.getInstance();
    Date lastRefresh = null;
    try {
      lastRefresh = db.getLastRefreshDate(null, localId);
    } catch (final GContactException e) {
      if (e.getExitCode() == ExitCode.NO_USER) {
        // this means the user was not in the DB
        logger.exiting(MyVDCalloutRefresh.CN, "inner_search", false);
        return false;
      }
      throw e;
    } catch (final RuntimeException e) {
      Utils.logProblemAndPunt(e);
    }
    long lastRefreshMs = 0L;
    if (lastRefresh != null) {
      lastRefreshMs = lastRefresh.getTime();
    } else {
      lastRefreshMs = 0L;
    }
    final long nowMs = System.currentTimeMillis();
    final long elapsedMs = nowMs - lastRefreshMs;
    if (logger.isLoggable(Level.FINE)) {
      logger.fine(String.format("Refresh check for user '%s'. Elapsed %s ms compared to TTL %s ms",
          localId, Duration.ofMillis(elapsedMs), ttl));
    }
    if (elapsedMs > ttl.toMillis()) {
      logger.info("Incremental refresh for user " + localId);
      final long refreshNowMs = System.currentTimeMillis();
      logger.entering(MyVDCalloutRefresh.CN, "refresh", localId);
      final String result = Action.refreshOrRestore(localId, true);
      logger.exiting(MyVDCalloutRefresh.CN, "refresh", result);
      final long refreshMs = System.currentTimeMillis() - refreshNowMs;
      if (logger.isLoggable(Level.FINE)) {
        logger.fine(String.format("Incremental refresh ACTION for user '%s' consumed %,d ms elapsed time.",
            localId, refreshMs));
      }
    }
    logger.exiting(MyVDCalloutRefresh.CN, "inner_search", true);
    return true;
  }

  public boolean search(String localId, String[] calloutArgs, Object[] originalArgs) {
    logger.entering(MyVDCalloutRefresh.CN, "search",
        new Object[] {localId, ((calloutArgs == null || calloutArgs.length == 0)? null : calloutArgs[0])});
    try {
      return inner_search(localId, calloutArgs, originalArgs);
    } catch (final GContactException e) {
      return false;
    } catch (final Throwable t) {
      Utils.logException(null, t);
      return false;
    } finally {
      logger.exiting(MyVDCalloutRefresh.CN, "search");
    }
  }
}
