package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import gcontact.Database.GoogleApiNecessities;
import gcontact.GoogleApiCaller.FetchSummary;
import gcontact.Utils.Config;

class Refresher implements Runnable {
  private static final String CN = Refresher.class.getName();
  private static final Logger logger = Logger.getLogger(Refresher.CN);
  private static Config config;
  private static int refresherWorkerCount;
  private static Duration refresherPeriodicity;
  private static Duration refresherStartupDelay;
  private static int refresherQueryLimit;
  private static float refresherFullProbability;
  private static boolean isStarted = false;
  private static ScheduledExecutorService producerPoolOfOne;
  private static ExecutorService consumerPoolOfMany;

  private static enum RunnableType {
    PRODUCER,
    CONSUMER,
  }

  private final RunnableType runnableType;
  private final GoogleApiNecessities workItem;
  private Refresher(RunnableType runnableType, GoogleApiNecessities workItem) {
    this.runnableType = runnableType;
    this.workItem = workItem;
  }
  private Refresher(RunnableType runnableType) {
    this(runnableType, null);
  }

  static synchronized void staticInit(String configPath) {
    if (Refresher.isStarted) {
      Refresher.logger.fine("Refresher service already started. Ignoring call. Harmless.");
      return;
    }
    Refresher.logger.entering(Refresher.CN, "staticInit", configPath);
    Refresher.config = Config.getInstance(configPath);
    Refresher.refresherWorkerCount = Refresher.config.getRefresherWorkerCount();
    Refresher.logger.info("refresherWorkerCount: " + Refresher.refresherWorkerCount);
    if (Refresher.refresherWorkerCount <= 0) {
      Refresher.logger.info("Will not run contacts Refresher service due to worker count " + Refresher.refresherWorkerCount);
      Refresher.logger.exiting(Refresher.CN, "staticInit 1");
      return;
    }
    Refresher.refresherPeriodicity = Refresher.config.getRefresherPeriodicity();
    Refresher.logger.info("refresherPeriodicity: " + Utils.durTs(Refresher.refresherPeriodicity));
    if (Refresher.refresherPeriodicity.getSeconds() < 60) {
      Refresher.logger.exiting(Refresher.CN, "staticInit 2");
      throw new IllegalArgumentException("RefresherPeriodicity is less and 60 seconds: " + Utils.durTs(Refresher.refresherPeriodicity));
    }
    Refresher.refresherStartupDelay = Refresher.config.getRefresherStartupDelay();
    Refresher.logger.info("refresherStartupDelay: " + Utils.durTs(Refresher.refresherStartupDelay));
    Refresher.refresherQueryLimit = Refresher.config.getRefresherQueryLimit();
    Refresher.logger.info("refresherQueryLimit: " + Refresher.refresherQueryLimit);
    if (Refresher.refresherQueryLimit < 3) {
      Refresher.logger.exiting(Refresher.CN, "staticInit 3");
      throw new IllegalArgumentException("RefresherQueryLimit is less than 3: " + Refresher.refresherQueryLimit);
    }

    Refresher.refresherFullProbability = Refresher.config.getRefresherFullProbability();
    Refresher.logger.info("refresherFullProbability: " + Refresher.refresherFullProbability);

    Refresher.createAndStartService();
    Refresher.logger.exiting(Refresher.CN, "staticInit");
  }

  private static void createAndStartService() {
    Refresher.logger.entering(Refresher.CN, "createAndStartService");
    Refresher.isStarted = true;

    Refresher.producerPoolOfOne = Executors.newSingleThreadScheduledExecutor();
    final Refresher producerRunnable = new Refresher(RunnableType.PRODUCER);
    Refresher.producerPoolOfOne.scheduleAtFixedRate(producerRunnable,
        Refresher.refresherStartupDelay.toMillis(),
        Refresher.refresherPeriodicity.toMillis(),
        TimeUnit.MILLISECONDS);

    Refresher.consumerPoolOfMany = Executors.newFixedThreadPool(Refresher.refresherWorkerCount);
    Refresher.logger.exiting(Refresher.CN, "createAndStartService");
  }

  @Override
  public void run() {
    Refresher.logger.fine(runnableType.name() + " Starting Refresher item " + Thread.currentThread().getName());
    switch (runnableType) {
      case PRODUCER:
        producerOneRunWrapper();
        break;
      case CONSUMER:
        consumerOneItem();
        break;
    }
  }

  private void producerOneRunWrapper() {
    final Database db = Database.getInstance();
    final long runStartedAt = System.currentTimeMillis();
    producerOneRun(db);
    final long runEndedAt = System.currentTimeMillis();
    final Duration justCompletedRunDuration = Duration.ofMillis(runEndedAt-runStartedAt);
    if (justCompletedRunDuration.compareTo(Refresher.refresherPeriodicity) >= 0) {
      Refresher.logger.info("The producer run outlasted the periodicity. " + justCompletedRunDuration + " > " + Refresher.refresherPeriodicity);
    }
  }

  private void producerOneRun(final Database db) {
    Refresher.logger.entering(Refresher.CN, "producerOneRun");
    GoogleApiNecessities lastItem = GoogleApiNecessities.STARTER;
    Refresher.logger.info("Producer loop starting");
    final long before = System.currentTimeMillis();
    while (true) {
      Refresher.logger.fine("Querying with capacity " + Refresher.refresherQueryLimit);
      if (Refresher.logger.isLoggable(Level.FINE)) {
        if (lastItem == GoogleApiNecessities.STARTER) {
          Refresher.logger.fine("Previous query last item was the STARTER");
        } else {
          Refresher.logger.fine("Previous query last item localId=" + lastItem.getLocalId() + ", random=" + lastItem.getRandom());
        }
      }
      final List<GoogleApiNecessities> blobs = db.getTokenBlobs(null, lastItem, Refresher.refresherQueryLimit);
      if (blobs.size() == 0) {
        final long after = System.currentTimeMillis();
        Refresher.logger.info("Producer loop completed. Duration: " + Utils.durTs(after-before));
        break;
      }
      lastItem = blobs.get(blobs.size() - 1);
      for (final GoogleApiNecessities blob : blobs) {
        if (Refresher.logger.isLoggable(Level.FINER)) {
          Refresher.logger.finer(Thread.currentThread().getName() + " workItem: localId=" + blob.getLocalId() + ", random=" + blob.getRandom());
        }
        final Refresher workItemRunnable = new Refresher(RunnableType.CONSUMER, blob);
        Refresher.consumerPoolOfMany.submit(workItemRunnable);
      }
    }
    Refresher.logger.exiting(Refresher.CN, "producerOneRun");
  }

  private void consumerOneItem() {
    Refresher.logger.entering(Refresher.CN, "consumerOneItem");
    final GoogleApiCaller gac = GoogleApiCaller.getInstance(workItem);

    boolean doIncrementalRefresh = true;
    if (Refresher.refresherFullProbability >= 1.0) {
      doIncrementalRefresh = false;
    } else if (Refresher.refresherFullProbability <= 0.0) {
      doIncrementalRefresh = true;
    } else {
      final float thisChooser = Utils.getRandom().nextFloat();
      if (Refresher.logger.isLoggable(Level.FINER)) {
        Refresher.logger.finer("Chooser for " + workItem.getLocalId() + ": " + thisChooser + " compared to " + Refresher.refresherFullProbability);
      }
      if (thisChooser <= Refresher.refresherFullProbability) {
        doIncrementalRefresh = false;
      }
    }

    try {
      final long before = System.currentTimeMillis();
      final FetchSummary result = gac.fetchContactsAndGroups(doIncrementalRefresh);
      final long after = System.currentTimeMillis();
      if (Refresher.logger.isLoggable(Level.FINER)) {
        Refresher.logger.finer(
            String.format("%s refreshed localId %s; requested: %s; actual %s; duration: %s",
                Thread.currentThread().getName(),
                workItem.getLocalId(),
                (doIncrementalRefresh          ? "INCR" : "FULL"),
                (result.isIncrementalRefresh() ? "INCR" : "FULL"),
                Utils.durTs(after-before)
                )
            );
      }
    } catch (final GeneralSecurityException e) {
      Refresher.logger.severe("A Java security exception indicates some kind of configuration or environmental problem: " + e);
      Refresher.logger.exiting(Refresher.CN, "consumerOneItem 1");
      throw new IllegalStateException(e);
    } catch (final IOException e) {
      Refresher.logger.exiting(Refresher.CN, "consumerOneItem 2");
      Refresher.logger.warning("An exception occurred while trying to refresh for user " + workItem.getLocalId() + ". Ignoring: " + e);
    }
    Refresher.logger.exiting(Refresher.CN, "consumerOneItem");
  }
}
