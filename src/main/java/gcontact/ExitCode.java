package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

//import java.util.logging.Logger;

enum ExitCode {
  CONFIG  (1), // some kind of configuration problem
  MDALG   (2), // message digest algorithm not supported; shouldn't happen
  PROOF   (3), // proof token validation failed; possible security attack
  DB      (4), // database problem
  ARGS    (5), // problem with command line arguments
  NO_USER (6), // special sentinal value meaning the user wasn't found in the database
  OTHER  (44);// some other kind of problem; doesn't fit any of the above

  //private static final String CN = ExitCode.class.getName();
  //private static Logger logger = Logger.getLogger(CN);
  private final int v;
  private ExitCode(int v) { this.v = v; }
  int value() { return v; }
}