package gcontact;

import java.io.ByteArrayOutputStream;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.apache.http.ConnectionClosedException;
import org.apache.http.ExceptionLogger;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.bootstrap.HttpServer;
import org.apache.http.impl.bootstrap.ServerBootstrap;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.ssl.SSLContexts;
import gcontact.Utils.Config;

/**
 * This class exists to listen for requests from the PHP script and respond to them.
 * Unlike the GContact main() class, it's a daemon server and doesn't exit. It gets
 * started as a side-effect of the MyVD server.
 */
public class Responder {

  // I wouldn't ordinarily use Apache HTTP components for such a simple job. They're kind of
  // complex for what I need to do. But I already have HttpCore as a transitive dependency
  // from the Google API stuff. So, I merely promoted it from a transitive dependency to a
  // direct dependency, and then I adapted the simple blocking server example from the HttpCore
  // web site. The HttpCore thread pool for handling requests is not exposed. I can see from the sources
  // that it's unbounded but idles down to 0 workers. That should be OK for something running
  // on localhost or behind a protective firewall. If you need to expose it to the internet
  // at large, it's subject to DoS.

  private static final String CN = Responder.class.getName();
  private static final Logger logger = Logger.getLogger(Responder.CN);
  private static Config config;
  private static boolean isStarted = false;
  private static InetAddress responderAddress;
  private static int responderPort;
  private static File responderKeystoreFile;
  private static boolean responderUseTls;
  private static String responderKeystoreSecret;
  private static String responderKeySecret;
  private static ResponderExceptionLogger exceptionLogger = new ResponderExceptionLogger();
  private static String responderPathPattern;
  private static String responderServerInfo;
  private static boolean responderUseDigestAuthentication;
  private static DigestAuthHelper digestAuthHelper;

  static synchronized void staticInit(String configPath) {
    if (Responder.isStarted) {
      Responder.logger.fine("Responder service already started. Ignoring call. Harmless.");
      return;
    }
    Responder.logger.entering(Responder.CN, "staticInit", configPath);

    Responder.config = Config.getInstance(configPath);
    Responder.responderPort = Responder.config.getResponderPort();
    Responder.logger.info("responderPort: " + Responder.responderPort);
    if (Responder.responderPort <= 0) {
      Responder.logger.info("Will not run contacts Responder service due to port " + Responder.responderPort);
      Responder.logger.exiting(Responder.CN, "staticInit 1");
      return;
    }
    Responder.responderAddress = Responder.config.getResponderAddress();
    Responder.logger.info("responderAddress: " + Responder.responderAddress);
    Responder.responderPathPattern = Responder.config.getResponderPathPattern();
    Responder.logger.info("responderPathPattern: " + Responder.responderPathPattern);
    Responder.responderServerInfo = Responder.config.getResponderServerInfo();
    Responder.logger.info("responderServerInfo: " + Responder.responderServerInfo);

    Responder.responderUseTls = Responder.config.getResponderUseTls();
    Responder.logger.info("responderUseTls: " + Responder.responderUseTls);
    Responder.responderKeystoreFile = Responder.config.getResponderKeystoreFile();
    Responder.logger.info("responderKeystoreFile: " + Responder.responderKeystoreFile);
    Responder.responderKeystoreSecret = Responder.config.getResponderKeystoreSecret();
    Responder.logger.info("responderKeystoreSecret " + (Responder.responderKeystoreSecret==null?"not":"") + " configured (not logged)");
    Responder.responderKeySecret = Responder.config.getResponderKeySecret();
    Responder.logger.info("responderKeySecret " + (Responder.responderKeySecret==null?"not":"") + " configured (not logged)");
    Responder.responderUseDigestAuthentication = Responder.config.getResponderUseDigestAuthentication();
    Responder.logger.info("responderUseDigestAuthentication: " + Responder.responderUseDigestAuthentication);
    if (Responder.responderUseDigestAuthentication) {
      final String authenticationRealm = Responder.config.getResponderAuthenticationRealm();
      Responder.logger.info("responderAuthenticationRealm: " + authenticationRealm);
      final String proofSecret = Responder.config.getProofSecret();
      Responder.logger.info("proofSecret " + (proofSecret==null?"not":"") + " configured (not logged)");
      final String responderServerDigestSecret = Responder.config.getResponderServerDigestSecret();
      Responder.logger.info("responderServerDigestSecret " + (responderServerDigestSecret==null?"not":"") + " configured (not logged)");
      final Duration responderNonceTimeTolerance = Responder.config.getResponderNonceTimeTolerance();
      Responder.logger.info("responderNonceTimeTolerance " + responderNonceTimeTolerance.toString().toLowerCase());
      Responder.digestAuthHelper = new DigestAuthHelper(authenticationRealm, proofSecret, responderServerDigestSecret, responderNonceTimeTolerance);
    }

    Responder.createAndStartService();
    Responder.logger.exiting(Responder.CN, "staticInit");
  }

  private static void createAndStartService() {
    Responder.logger.entering(Responder.CN, "createAndStartService");
    SSLContext sslContext = null;
    if (Responder.responderUseTls) {
      if (Responder.responderKeystoreFile == null) {
        throw new IllegalArgumentException("TLS requested, but keystore not configured");
      }
      try {
        sslContext = SSLContexts.custom()
            .loadKeyMaterial(Responder.responderKeystoreFile,
                Responder.responderKeystoreSecret.toCharArray(),
                Responder.responderKeySecret.toCharArray())
            .build();
      } catch (final Exception e) {
        Responder.logger.exiting(Responder.CN, "createAndStartService 1");
        throw new IllegalArgumentException("Exception when accessing the configured keystore for TLS: " + e, e);
      }
    }

    final SocketConfig socketConfig = SocketConfig.custom()
        .setSoTimeout(15000)
        .setTcpNoDelay(true)
        .build();

    String simplePattern = Responder.responderPathPattern;
    String wildPattern = Responder.responderPathPattern;
    if (Responder.responderPathPattern.endsWith("/")) {
      simplePattern = Responder.responderPathPattern.substring(0, Responder.responderPathPattern.length()-1);
      wildPattern = Responder.responderPathPattern + "*";
      Responder.logger.info("Treating '" + Responder.responderPathPattern + "' as both '" + simplePattern + "' and '" + wildPattern + "'");
    }
    final HttpServer server = ServerBootstrap.bootstrap()
        .setLocalAddress(Responder.responderAddress)
        .setListenerPort(Responder.responderPort)
        .setServerInfo(Responder.responderServerInfo)
        .setSocketConfig(socketConfig)
        .setSslContext(sslContext)
        .setExceptionLogger(Responder.exceptionLogger)
        .registerHandler(simplePattern, new ActionsHandler())
        .registerHandler(wildPattern, new ActionsHandler())
        .create();

    try {
      server.start();
    } catch (final IOException e) {
      Responder.logger.exiting(Responder.CN, "createAndStartService 2");
      throw new IllegalStateException("Could not start the HTTP server for the Responder service: " + e, e);
    }
    Responder.logger.exiting(Responder.CN, "createAndStartService");
  }

  private static class ResponderExceptionLogger implements ExceptionLogger {
    @Override
    public void log(final Exception e) {
      if (!(e instanceof ConnectionClosedException)) {
        Responder.logger.log(Level.WARNING, "Exception in Responder HTTP server: " + e, e);
      }
    }
    public void log(final String s) {
      Responder.logger.log(Level.WARNING, s);
    }
  }

  private static class ActionsHandler implements HttpRequestHandler  {
    private static final String CN = ActionsHandler.class.getName();
    private ActionsHandler() {
      super();
    }
    @Override
    public void handle(final HttpRequest request, final HttpResponse response, final HttpContext context) {
      Responder.logger.entering(ActionsHandler.CN, "handle", request.getRequestLine());
      final String method = request.getRequestLine().getMethod().toUpperCase(Locale.ROOT);
      if (!method.equals("POST")) {
        Responder.exceptionLogger.log(method + " method not supported");
        ActionsHandler.replyBadRequest(response);
        Responder.logger.exiting(ActionsHandler.CN, "handle 1");
        return;
      }
      String localId = null;
      if (Responder.responderUseDigestAuthentication) {
        final Header authorizationHeader = request.getFirstHeader("Authorization");
        if (authorizationHeader == null) {
          ActionsHandler.challengeForAuthentication(response);
          return;
        }
        final String authorizationValue = authorizationHeader.getValue();
        if (authorizationValue == null  ||  authorizationValue.length() == 0) {
          ActionsHandler.replyBadRequest(response);
          Responder.logger.exiting(ActionsHandler.CN, "handle 2");
          return;
        }
        try {
          localId = Responder.digestAuthHelper.authenticate(authorizationValue, method);
        } catch (final Exception e) {
          Responder.logger.log(Level.WARNING, "Authentication exception in Responder HTTP server: " + e, e);
          ActionsHandler.replyBadRequest(response);
          Responder.logger.exiting(ActionsHandler.CN, "handle 3");
          return;
        }
        if (localId == null  ||  localId.length() == 0) {
          ActionsHandler.replyBadRequest(response);
          Responder.logger.exiting(ActionsHandler.CN, "handle 6");
          return;
        }
      }

      if (!(request instanceof HttpEntityEnclosingRequest)) {
        Responder.exceptionLogger.log("HTTP request has no body");
        ActionsHandler.replyBadRequest(response);
        Responder.logger.exiting(ActionsHandler.CN, "handle 5");
        return;
      }

      final HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
      try (InputStream contentStream = entity.getContent();
          ByteArrayOutputStream out = new ByteArrayOutputStream()) {

        final GContact gc = new GContact(contentStream, out);
        gc.gatherAndPerformActions(new String[] {"-"}, localId);

        out.flush();
        response.setStatusCode(HttpStatus.SC_OK);
        final ByteArrayEntity actionOutput = new ByteArrayEntity(out.toByteArray(), ContentType.create("text/plain", StandardCharsets.ISO_8859_1.name()));
        response.setEntity(actionOutput);
      } catch (final RuntimeException | IOException e) {
        Responder.exceptionLogger.log(e);
        ActionsHandler.replyBadRequest(response);
        Responder.logger.exiting(ActionsHandler.CN, "handle 4");
        return;
      }
      Responder.logger.exiting(ActionsHandler.CN, "handle");
    }

    private static void replyBadRequest(final HttpResponse response) {
      response.setStatusCode(HttpStatus.SC_BAD_REQUEST);
      final StringEntity entity = new StringEntity(
          "<html>\n<body>\n<h1>Bad Request. <i>Bad, bad request!</i> Git offa my lawn, you kids.</h1>\n</body>\n</html>\n",
          ContentType.create("text/html", "UTF-8"));
      response.setEntity(entity);
    }

    private static void challengeForAuthentication(final HttpResponse response) {
      response.setStatusCode(HttpStatus.SC_UNAUTHORIZED);
      response.setHeader("WWW-Authenticate", Responder.digestAuthHelper.createServerHeaderContents());
    }
  }
}
