package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDriver;
import org.apache.commons.pool2.impl.GenericObjectPool;
import gcontact.Utils.Config;

/**
 * All actual DB stuff goes in here. Connection objects, transactions, and friends don't get out.
 */
class Database {
  private static final String CN = Database.class.getName();
  private static final Logger logger = Logger.getLogger(Database.CN);
  /**
   * The Tx class is used as a lightweight Transaction stand-in. It
   * holds onto a Connection object and manages the commit/rollback
   * state. It exists so that other classes don't have to know about
   * Connection details in our Database class.
   */
  static class Tx {
    private static final String CN = Tx.class.getName();
    private static Logger logger = Logger.getLogger(Tx.CN);
    private final Connection cnx;
    private Tx(Connection cnx) {
      this.cnx = cnx;
    }
    static Tx getInstance() {
      Tx.logger.entering(Tx.CN, "getInstance");
      final Connection cnx = Database.instance.getConnection(null);
      try {
        cnx.setAutoCommit(false);
      } catch (final SQLException e) {
        Utils.logDbProblemAndPunt(e);
      }
      final Tx tx = new Tx(cnx);
      Tx.logger.exiting(Tx.CN, "getInstance", tx);
      return tx;
    }
    private Connection getConnection() {
      return cnx;
    }

    void commit() throws SQLException {
      Tx.logger.entering(Tx.CN, "commit");
      cnx.commit();
      cnx.setAutoCommit(true);
      cnx.close();
      Tx.logger.exiting(Tx.CN, "commit");
    }
    void rollback() throws SQLException {
      Tx.logger.entering(Tx.CN, "rollback");
      cnx.rollback();
      cnx.setAutoCommit(true);
      cnx.close();
      Tx.logger.exiting(Tx.CN, "rollback");
    }
    boolean isClosed() throws SQLException {
      Tx.logger.entering(Tx.CN, "isClosed");
      final boolean isClosed = cnx.isClosed();
      Tx.logger.exiting(Tx.CN, "isClosed", isClosed);
      return isClosed;
    }
  }

  /**
   * This is just a convenience class for carrying a handful of things needed for the Google API calls.
   */
  static class GoogleApiNecessities {
    // assumes empty string sorts earlier than any real localId
    static GoogleApiNecessities STARTER = new GoogleApiNecessities("", null, null, null, Integer.MIN_VALUE);
    String getLocalId() {
      return localId;
    }
    String getTokenBlob() {
      return tokenBlob;
    }
    String getContactsSyncToken() {
      return contactsSyncToken;
    }
    String getContactGroupsSyncToken() {
      return contactGroupsSyncToken;
    }
    public int getRandom() {
      return random;
    }
    private final String localId;
    private final String tokenBlob;
    private final String contactsSyncToken;
    private final String contactGroupsSyncToken;
    private final int random; // for convenient bookkeeping
    private GoogleApiNecessities(String localId, String tokenBlob, String contactsSyncToken, String contactGroupsSyncToken, int random) {
      this.localId = localId;
      this.tokenBlob = tokenBlob;
      this.contactsSyncToken = contactsSyncToken;
      this.contactGroupsSyncToken = contactGroupsSyncToken;
      this.random = random;
    }
    GoogleApiNecessities(String localId) {
      this(localId, null, null, null, 0);
    }

  }

  /**
   * This carries around the values of the "isPrimary" things from MV fields. They get
   * stored redundantly in the contacts record.
   */
  static class PrimaryItems {
    private String displayName;
    private String givenName;
    private String familyName;
    private String emailAddress;
    private String phoneNumber;
    private String canonicalForm;
    private String department;
    private byte[] photoBytes;
    String getDisplayName() {
      return displayName;
    }
    void setDisplayName(String displayName) {
      this.displayName = displayName;
    }
    String getGivenName() {
      return givenName;
    }
    void setGivenName(String givenName) {
      this.givenName = givenName;
    }
    String getFamilyName() {
      return familyName;
    }
    void setFamilyName(String familyName) {
      this.familyName = familyName;
    }
    String getEmailAddress() {
      return emailAddress;
    }
    void setEmailAddress(String emailAddress) {
      this.emailAddress = emailAddress;
    }
    String getPhoneNumber() {
      return phoneNumber;
    }
    void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    String getCanonicalForm() {
      return canonicalForm;
    }
    void setCanonicalForm(String canonicalForm) {
      this.canonicalForm = canonicalForm;
    }
    String getDepartment() {
      return department;
    }
    void setDepartment(String department) {
      this.department = department;
    }
    byte[] getPhotoBytes() {
      return photoBytes;
    }
    void setPhotoBytes(byte[] photoBytes) {
      this.photoBytes = photoBytes;
    }
  }
  private static Database instance = null;
  private Connection getConnection(Tx tx) {
    if (tx != null) {
      return tx.getConnection();
    }
    try {
      final Connection cnx = DriverManager.getConnection("jdbc:apache:commons:dbcp:gcontact");
      return cnx;
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    }
    return null;
  }
  private void closeConnection(Connection cnx) {
    try {
      if (!cnx.isClosed()) {
        cnx.setAutoCommit(true);
        cnx.close();
      }
    } catch (final SQLException e) {
      // ignoring close() problems; life is too short
    }
  }

  // DBCP2 Connection pooling code is based on the example from here (sorry for the long URL; not my fault):
  // https://git-wip-us.apache.org/repos/asf?p=commons-dbcp.git;a=blob;f=doc/PoolingDriverExample.java;h=8fd8ec50ac14f66dec84342e7a441388932190d1;hb=refs/heads/master
  private static void setupPooledDriver(String dbConnectionString, String dbUser, String dbPassword) throws ClassNotFoundException, SQLException {
    final ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(dbConnectionString, dbUser, dbPassword);
    final PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
    poolableConnectionFactory.setValidationQuery("SELECT 1");
    final GenericObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
    // the default for this is false, which surprised me a little
    connectionPool.setTestOnBorrow(true);
    poolableConnectionFactory.setPool(connectionPool);
    Class.forName("org.apache.commons.dbcp2.PoolingDriver");
    final PoolingDriver driver = (PoolingDriver)DriverManager.getDriver("jdbc:apache:commons:dbcp:");
    driver.registerPool("gcontact", connectionPool);
  }

  private Database() {}
  static Database getInstance() {
    Database.logger.entering(Database.CN, "getInstance");
    if (Database.instance == null) {
      Database.instance = new Database();
      try {
        final Config config = Config.getInstance(null);
        final String dbUser = config.getDbUsername();
        final String dbPassword = config.getDbPassword();
        final String dbConnectionString = config.getDbConnectionString();
        Database.setupPooledDriver(dbConnectionString, dbUser, dbPassword);
      } catch (final SQLException e) {
        Utils.logDbProblemAndPunt(e);
      } catch (final ClassNotFoundException e) {
        Utils.logProblemAndPunt(e);
      }
    }
    Database.logger.exiting(Database.CN, "getInstance", Database.instance);
    return Database.instance;
  }
  Tx getTx() {
    Database.logger.entering(Database.CN, "getTx");
    final Tx tx = Tx.getInstance();
    Database.logger.exiting(Database.CN, "getTx", tx);
    return tx;
  }

  /**
   * After a fetch cycle using the Google APIs, we generally get new values for the sync tokens. This
   * method persists them into the DB for use with the next fetch cycle.
   */
  void updateFetchInformation(Tx tx, String localId, String contactsSyncToken, String contactGroupsSyncToken) {
    Database.logger.entering(Database.CN, "updateFetchInformation",
        new Object[] {tx, localId, contactsSyncToken, contactGroupsSyncToken});
    int rowsUpdated = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("UPDATE identities SET " +
            "fetch_timestamp = CURRENT_TIMESTAMP, " +
            "contacts_sync_token = ?, " +
            "contact_groups_sync_token = ? " +
            "WHERE local_id = ?")) {
      pstmt.setString(1, contactsSyncToken);
      pstmt.setString(2, contactGroupsSyncToken);
      pstmt.setString(3, localId);
      rowsUpdated = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    if (rowsUpdated != 1) {
      final SQLException e = new SQLException("Database inconsistency. Expected 1 record for " + localId + " but saw " + rowsUpdated + ".");
      Utils.logDbProblemAndPunt(e);
    }
    Database.logger.exiting(Database.CN, "updateFetchInformation");
  }

  /**
   * Redundantly stores several fields from "isPrimary" multi-valued records in case
   * a denormalized query is used by MyVD.
   */
  void updatePrimaryItems(Tx tx, String localId, String contactId, PrimaryItems pItems) {
    Database.logger.entering(Database.CN, "updatePrimaryItems",
        new Object[] {tx, localId, pItems});
    int rowsUpdated = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("UPDATE contacts SET " +
            "display_name = ?, " +
            "given_name = ?, " +
            "family_name = ?, " +
            "email_address = ?, " +
            "phone_number = ?, " +
            "canonical_form = ?, " +
            "department = ?, " +
            "photo_bytes_b64 = ? " +
            "WHERE local_id = ? AND contact_id = ?")) {
      pstmt.setString(1, pItems.getDisplayName());
      pstmt.setString(2, pItems.getGivenName());
      pstmt.setString(3, pItems.getFamilyName());
      pstmt.setString(4, pItems.getEmailAddress());
      pstmt.setString(5, pItems.getPhoneNumber());
      pstmt.setString(6, pItems.getCanonicalForm());
      pstmt.setString(7, pItems.getDepartment());
      final byte[] photoBytes = pItems.getPhotoBytes();
      if (photoBytes != null  &&  photoBytes.length > 0) {
        pstmt.setString(8, Base64.getEncoder().encodeToString(photoBytes));
      } else {
        pstmt.setString(8, null);
      }
      pstmt.setString(9, localId);
      pstmt.setString(10, contactId);
      rowsUpdated = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    if (rowsUpdated != 1) {
      final SQLException e = new SQLException("Database inconsistency. Expected 1 record for " + localId + " but saw " + rowsUpdated + ".");
      Utils.logDbProblemAndPunt(e);
    }
    Database.logger.exiting(Database.CN, "updatePrimaryItems");
  }

  /**
   * The access token times out from time to time. Actually, my observation is that it's only good
   * for 1 hour. After that, we use the refresh token to get a new one. The new stuff, which is the
   * new access token and whatever else Google sends us, is updated in the DB so it's ready for the
   * next Google API call.
   */
  void updateTokenBlob(Tx tx, String localId, String tokenBlob) {
    Database.logger.entering(Database.CN, "updateTokenBlob",
        new Object[] {tx, localId, tokenBlob});
    int rowsUpdated = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("UPDATE identities SET " +
            "token_timestamp = CURRENT_TIMESTAMP, " +
            "google_token_blob = ? " +
            "WHERE local_id = ?")) {
      pstmt.setString(1, tokenBlob);
      pstmt.setString(2, localId);
      rowsUpdated = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    if (rowsUpdated != 1) {
      final SQLException e = new SQLException("Database inconsistency. Expected 1 record for " + localId + " but saw " + rowsUpdated + ".");
      Utils.logDbProblemAndPunt(e);
    }
    Database.logger.exiting(Database.CN, "updateTokenBlob");
  }

  /**
   * Returns null if the localId is not in the database.
   */
  GoogleApiNecessities getTokenBlob(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getTokenBlob", new Object[] {tx, localId});
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT google_token_blob, contacts_sync_token, contact_groups_sync_token, random " +
            "FROM identities " +
            "WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        final boolean hadNext = rs.next();
        if (hadNext) {
          final String tokenBlob = rs.getString("google_token_blob");
          final String contactsSyncToken = rs.getString("contacts_sync_token");
          final String contactGroupsSyncToken = rs.getString("contact_groups_sync_token");
          final int random = rs.getInt("random");
          final GoogleApiNecessities gan = new GoogleApiNecessities(localId, tokenBlob, contactsSyncToken, contactGroupsSyncToken, random);
          Database.logger.exiting(Database.CN, "getTokenBlob", gan);
          return gan;
        }
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getTokenBlob-NOT_REACHED");
    return null; // not reached
  }

  /**
   * Returns null if the localId is not in the database.
   */
  List<GoogleApiNecessities> getTokenBlobs(Tx tx, GoogleApiNecessities lastItem, int maxResults) {
    Database.logger.entering(Database.CN, "getTokenBlobs", new Object[] {tx, lastItem.getLocalId(), lastItem.getRandom(), maxResults});
    final Connection cnx = getConnection(tx);
    final List<GoogleApiNecessities> blobs = new ArrayList<>();
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT local_id, google_token_blob, contacts_sync_token, contact_groups_sync_token, random " +
            "FROM identities " +
            "WHERE random >= ? AND local_id > ? " +  // assumes localIds are unique but random values are not
            "ORDER BY random ASC, local_id ASC " +
            "LIMIT ? ")) {
      pstmt.setInt(   1, lastItem.getRandom());
      pstmt.setString(2, lastItem.getLocalId());
      pstmt.setInt(   3, maxResults);
      try (ResultSet rs = pstmt.executeQuery()) {
        while (rs.next()) {
          final String localId = rs.getString("local_id");
          final String tokenBlob = rs.getString("google_token_blob");
          final String contactsSyncToken = rs.getString("contacts_sync_token");
          final String contactGroupsSyncToken = rs.getString("contact_groups_sync_token");
          final int random = rs.getInt("random");
          final GoogleApiNecessities gan = new GoogleApiNecessities(localId, tokenBlob, contactsSyncToken, contactGroupsSyncToken, random);
          blobs.add(gan);
        }
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getTokenBlobs", blobs);
    return blobs;
  }

  /**
   * Throws if the localId is not in the database. Returns null if no fetch
   * has ever been done.
   */
  Date getLastRefreshDate(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getLastRefreshDate", new Object[] {tx, localId});
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT fetch_timestamp " +
            "FROM identities " +
            "WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        final boolean hadNext = rs.next();
        if (hadNext) {
          final Date lastRefresh = rs.getTimestamp("fetch_timestamp");
          Database.logger.exiting(Database.CN, "getLastRefreshDate", lastRefresh);
          return lastRefresh;
        } else {
          final String message = "User '" + localId + "' is not in the database. Perhaps authorization consent is needed.";
          throw new GContactException(message, null, ExitCode.NO_USER);
        }
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getLastRefreshDate-");
    return null;
  }

  int getCountOfContacts(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfContacts", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT COUNT(*) as K FROM contacts WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfContacts", count);
    return count;
  }

  int getCountOfContactGroups(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfContactGroups", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT COUNT(*) as K FROM contact_groups WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfContactGroups", count);
    return count;
  }

  int getCountOfContactGroupMembers(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfContactGroupMembers", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT COUNT(*) as K FROM contact_group_members WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfContactGroupMembers", count);
    return count;
  }

  /**
   * I could have written this without the 3-way JOIN because I put a local_id column directly into that
   * table. But I wrote this before I added that column, and I'm not sure I will leave that column
   * there.
   */
  int getCountOfEmailAddresses(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfEmailAddresses", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement(
          "SELECT COUNT(*) as K " +
          "FROM email_addresses AS e " +
              "JOIN contacts AS c ON c.contact_id = e.contact_id " +
              "JOIN identities AS i on i.local_id = c.local_id " +
          "WHERE i.local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfEmailAddresses", count);
    return count;
  }

  int getCountOfPhoneNumbers(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfPhoneNumbers", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement(
          "SELECT COUNT(*) as K " +
          "FROM phone_numbers AS p " +
              "JOIN contacts AS c ON c.contact_id = p.contact_id " +
              "JOIN identities AS i on i.local_id = c.local_id " +
          "WHERE i.local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfPhoneNumbers", count);
    return count;
  }

  int getCountOfPhotos(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfPhotos", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement(
          "SELECT COUNT(*) as K " +
          "FROM photos AS p " +
              "JOIN contacts AS c ON c.contact_id = p.contact_id " +
              "JOIN identities AS i on i.local_id = c.local_id " +
          "WHERE i.local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfPhotos", count);
    return count;
  }

  int getCountOfOrganizations(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfOrganizations", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement(
          "SELECT COUNT(*) as K " +
          "FROM organizations AS o " +
              "JOIN contacts AS c ON c.contact_id = o.contact_id " +
              "JOIN identities AS i on i.local_id = c.local_id " +
          "WHERE i.local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfOrganizations", count);
    return count;
  }

  int getCountOfDisplayNames(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getCountOfDisplayNames", new Object[] {tx, localId});
    int count = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement(
          "SELECT COUNT(*) as K " +
          "FROM display_names AS d " +
              "JOIN contacts AS c ON c.contact_id = d.contact_id " +
              "JOIN identities AS i on i.local_id = c.local_id " +
          "WHERE i.local_id = ?")) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        rs.next();
        count = rs.getInt("K");
      }
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "getCountOfDisplayNames", count);
    return count;
  }

  int deleteIdentity(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "deleteIdentity", new Object[] {tx, localId});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("DELETE FROM identities WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "deleteIdentity", howMany);
    return howMany;
  }

  int deleteContacts(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "deleteContacts", new Object[] {tx, localId});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("DELETE FROM contacts WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "deleteContacts", howMany);
    return howMany;
  }

  /**
   * The contact groups have members in another table, but there are deleted via cascade delete.
   */
  int deleteContactGroups(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "deleteContactGroups", new Object[] {tx, localId});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("DELETE FROM contact_groups WHERE local_id = ?")) {
      pstmt.setString(1, localId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    Database.logger.exiting(Database.CN, "deleteContactGroups", howMany);
    return howMany;
  }

  /**
   * This deletes all of the user's data without deleting the user's identities table entry.
   */
  int deleteSubordinateData(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "deleteSubordinateData", new Object[] {tx, localId});
    final int howManyContacts = deleteContacts(tx, localId);
    final int howManyContactGroups = deleteContactGroups(tx, localId);
    final int howMany = howManyContacts + howManyContactGroups;
    Database.logger.exiting(Database.CN, "deleteSubordinateData", howMany);
    return howMany;
  }

  boolean deleteContact(Tx tx, String localId, String contactId) {
    Database.logger.entering(Database.CN, "deleteContact");
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("DELETE FROM contacts WHERE contact_id = ? AND local_id = ?")) {
      pstmt.setString(1, contactId);
      pstmt.setString(2, localId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "deleteContact", wasOne);
    return wasOne;
  }

  boolean deleteContactGroup(Tx tx, String localId, String contactGroupId) {
    Database.logger.entering(Database.CN, "deleteContactGroup",
        new Object[] {tx, localId, contactGroupId});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("DELETE FROM contact_groups WHERE contact_group_id = ? AND local_id = ?")) {
      pstmt.setString(1, contactGroupId);
      pstmt.setString(2, localId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "deleteContactGroup", wasOne);
    return wasOne;
  }

  boolean insertContact(Tx tx, String localId, String contactId) {
    Database.logger.entering(Database.CN, "insertContact",
        new Object[] {tx, localId, contactId});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO contacts (" +
            "local_id, " +
            "contact_id) " +
            "VALUES (?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "insertContact", wasOne);
    return wasOne;
  }

  boolean insertContactGroup(Tx tx, String localId, String contactGroupId, String contactGroupName, String contactGroupFormattedName, String contactGroupType) {
    Database.logger.entering(Database.CN, "insertContactGroup",
        new Object[] {tx, localId, contactGroupId, contactGroupName});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO contact_groups (" +
            "local_id, " +
            "contact_group_id, " +
            "contact_group_name, " +
            "contact_group_formatted_name, " +
            "contact_group_type) " +
            "VALUES (?, ?, ?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactGroupId);
      pstmt.setString(3, contactGroupName);
      pstmt.setString(4, contactGroupFormattedName);
      pstmt.setString(5, contactGroupType);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "insertContactGroup", wasOne);
    return wasOne;
  }

  boolean insertContactGroupMember(Tx tx, String localId, String contactGroupId, String contactId) {
    Database.logger.entering(Database.CN, "insertContactGroupMember",
        new Object[] {tx, localId, contactGroupId, contactId});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    // the column is "contact_id" because you can't be in a group without being a contact
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO contact_group_members (" +
            "local_id, " +
            "contact_group_id, " +
            "contact_id) " +
            "VALUES (?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactGroupId);
      pstmt.setString(3, contactId);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "insertContactGroupMember", wasOne);
    return wasOne;
  }

  boolean insertDisplayName(Tx tx, String localId, String contactId, String displayName, Boolean isPrimary, String prefix, String givenName, String middleName, String familyName, String suffix) {
    Database.logger.entering(Database.CN, "insertDisplayName",
        new Object[] {tx, localId, contactId, displayName, isPrimary});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO display_names (" +
            "local_id, " +
            "contact_id, " +
            "is_primary, " +
            "display_name, " +
            "prefix, " +
            "given_name, " +
            "middle_name, " +
            "family_name, " +
            "suffix) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactId);
      pstmt.setBoolean(3, isPrimary == null ? false : isPrimary);
      pstmt.setString(4, displayName);
      pstmt.setString(5, prefix);
      pstmt.setString(6, givenName);
      pstmt.setString(7, middleName);
      pstmt.setString(8, familyName);
      pstmt.setString(9, suffix);

      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "insertDisplayName", wasOne);
    return wasOne;
  }

  boolean insertPhoneNumber(Tx tx, String localId, String contactId, String phoneNumber, Boolean isPrimary, String canonicalForm, String type) {
    Database.logger.entering(Database.CN, "insertPhoneNumber",
        new Object[] {tx, localId, contactId, phoneNumber, isPrimary});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO phone_numbers (" +
            "local_id, " +
            "contact_id, " +
            "is_primary, " +
            "phone_number, " +
            "canonical_form, " +
            "type) " +
            "VALUES (?, ?, ?, ?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactId);
      pstmt.setBoolean(3, isPrimary == null ? false : isPrimary);
      pstmt.setString(4, phoneNumber);
      pstmt.setString(5, canonicalForm);
      pstmt.setString(6, type);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "insertPhoneNumber", wasOne);
    return wasOne;
  }

  boolean insertPhoto(Tx tx, String localId, String contactId, String photoUrl, String contentType, String originalContentType, Boolean isDefault, byte[] photoBytes, Boolean isPrimary) {
    Database.logger.entering(Database.CN, "insertPhoto",
        new Object[] {tx, localId, contactId, photoUrl, contentType, isPrimary});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO photos (" +
            "local_id, " +
            "contact_id, " +
            "is_primary, " +
            "is_default, " +
            "photo_url, " +
            "content_type, " +
            "original_content_type, " +
            "photo_bytes_b64) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactId);
      pstmt.setBoolean(3, isPrimary == null ? false : isPrimary);
      pstmt.setBoolean(4, isDefault == null ? false : isDefault);
      pstmt.setString(5, photoUrl);
      pstmt.setString(6, contentType);
      pstmt.setString(7, originalContentType);
      if (photoBytes != null  &&  photoBytes.length > 0) {
        pstmt.setString(8, Base64.getEncoder().encodeToString(photoBytes));
      } else {
        pstmt.setString(8, null);
      }
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = (howMany == 1);
    Database.logger.exiting(Database.CN, "insertPhoto", wasOne);
    return wasOne;
  }

  boolean insertEmailAddress(Tx tx, String localId, String contactId, String emailAddress, String displayName, Boolean isPrimary, String type) {
    Database.logger.entering(Database.CN, "insertEmailAddress",
        new Object[] {tx, localId, contactId, emailAddress, isPrimary});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO email_addresses (" +
            "local_id, " +
            "contact_id, " +
            "is_primary, " +
            "email_address, " +
            "display_name, " +
            "type) " +
            "VALUES (?, ?, ?, ?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactId);
      pstmt.setBoolean(3, isPrimary == null ? false : isPrimary);
      pstmt.setString(4, emailAddress);
      pstmt.setString(5, displayName);
      pstmt.setString(6, type);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = howMany == 1;
    Database.logger.exiting(Database.CN, "insertEmailAddress", wasOne);
    return wasOne;
  }

  boolean insertOrganization(Tx tx, String localId, String contactId, String name, String department, String title, Boolean isCurrent, Boolean isPrimary, String type) {
    Database.logger.entering(Database.CN, "insertOrganization",
        new Object[] {tx, localId, contactId, name, isCurrent, isPrimary});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO organizations (" +
            "local_id, " +
            "contact_id, " +
            "is_current, " +
            "is_primary, " +
            "name, " +
            "department, " +
            "title, " +
            "type) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, contactId);
      pstmt.setBoolean(3, isCurrent == null ? false : isCurrent);
      pstmt.setBoolean(4, isPrimary == null ? false : isPrimary);
      pstmt.setString(5, name);
      pstmt.setString(6, department);
      pstmt.setString(7, title);
      pstmt.setString(8, type);
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = howMany == 1;
    Database.logger.exiting(Database.CN, "insertOrganization", wasOne);
    return wasOne;
  }

  boolean insertIdentity(Tx tx, String localId, String tokenBlob) {
    Database.logger.entering(Database.CN, "insertIdentity",
        new Object[] {tx, localId, tokenBlob});
    int howMany = 0;
    final Connection cnx = getConnection(tx);
    try (PreparedStatement pstmt =
        cnx.prepareStatement("INSERT INTO identities (" +
            "local_id, " +
            "google_token_blob, " +
            "token_timestamp, " +
            "random) " +
            "VALUES (?, ?, CURRENT_TIMESTAMP, ?)")) {
      pstmt.setString(1, localId);
      pstmt.setString(2, tokenBlob);
      pstmt.setInt(3, Utils.getRandom().nextInt());
      howMany = pstmt.executeUpdate();
    } catch (final SQLException e) {
      final Throwable t = new IllegalStateException("Unable to create an entry for local identity " + localId, e);
      Utils.logProblemAndPunt(t);
    } finally {
      if (tx == null) closeConnection(cnx);
    }
    final boolean wasOne = howMany == 1;
    Database.logger.exiting(Database.CN, "insertIdentity", wasOne);
    return wasOne;
  }

  private static class OrderByFormattedNameWithinToFrom implements Comparator<SharingObject> {
    private static final OrderByFormattedNameWithinToFrom INSTANCE = new OrderByFormattedNameWithinToFrom();
    private OrderByFormattedNameWithinToFrom() {}
    @Override
    public int compare(SharingObject arg0, SharingObject arg1) {
      final boolean shareTo0 = arg0.isSharedTo();
      final boolean shareTo1 = arg1.isSharedTo();
      if (shareTo0 == shareTo1) {
        return arg0.getContactGroupFormattedName().compareToIgnoreCase(arg1.getContactGroupFormattedName());
      }
      if (shareTo0) {
        return -1;
      }
      return 1;
    }
  }

  /**
   * Gets the user's contact group shares. Returned as a List, with
   * each item in the List being a String[]. The first elements of the
   * array is the contact group I
   */
  List<SharingObject> getShares(Tx tx, String localId) {
    Database.logger.entering(Database.CN, "getShares", new Object[] {tx, localId});
    final Connection cnx = getConnection(tx);
    final List<SharingObject> sharings = new ArrayList<SharingObject>();

    try {
      getSharesWrapped(localId, cnx, sharings);
    } finally {
      if (tx == null) closeConnection(cnx);
    }

    // Sorted for UI purposes
    sharings.sort(OrderByFormattedNameWithinToFrom.INSTANCE);
    Database.logger.exiting(Database.CN, "getShares", sharings.size());
    return sharings;
  }

  private void getSharesWrapped(String localId, Connection cnx, List<SharingObject> sharings) {
    // We use an outer join so that we pick up groups that are not shared at all.
    // contact_group_ui sort for processing, share_to sort for consistent UI
    try (PreparedStatement pstmtShareTo =
        cnx.prepareStatement("SELECT " +
              "g.contact_group_id AS cgid, " +
              "g.contact_group_name AS name, " +
              "g.contact_group_formatted_name AS fname, " +
              "s.share_to AS share_partner, " +
              "s.share_to_opt_in AS opt_in " +
            "FROM contact_groups AS g " +
            "LEFT OUTER JOIN shares AS s " +
              "ON (g.local_id = s.local_id  AND  g.contact_group_id = s.contact_group_id) " +
            "WHERE g.local_id = ? ORDER BY g.contact_group_id, s.share_to");
        // I wanted to get the member count by adding the contact_group_members table as
        // an additional JOIN, but I couldn't work out the syntax to get what I wanted.
        PreparedStatement pstmtMemberCount =
            cnx.prepareStatement("SELECT COUNT(*) AS member_count FROM contact_group_members " +
                                 "WHERE local_id = ? AND contact_group_id = ?")) {
      getSharesCommon(localId, sharings, pstmtShareTo, pstmtMemberCount, true);
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    }

    try (PreparedStatement pstmtShareFrom =
        cnx.prepareStatement("SELECT " +
              "g.contact_group_id AS cgid, " +
              "g.contact_group_name AS name, " +
              "g.contact_group_formatted_name AS fname, " +
              "s.local_id AS share_partner, " +
              "s.share_to_opt_in AS opt_in " +
            "FROM contact_groups AS g " +
            "LEFT OUTER JOIN shares AS s " +
              "ON (g.local_id = s.local_id  AND  g.contact_group_id = s.contact_group_id) " +
            "WHERE s.share_to = ? ORDER BY g.contact_group_id, s.local_id")) {
      getSharesCommon(localId, sharings, pstmtShareFrom, null, false);
    } catch (final SQLException e) {
      Utils.logDbProblemAndPunt(e);
    }
  }

  private void getSharesCommon(String localId, List<SharingObject> sharings,
      PreparedStatement pstmtSharing, PreparedStatement pstmtMemberCount, boolean isSharedTo) throws SQLException {
    pstmtSharing.setString(1, localId);
    Database.logger.entering(Database.CN, "getShares", new Object[] {localId, (pstmtMemberCount != null)});
    try (ResultSet rsShares = pstmtSharing.executeQuery()) {
      String previousCgid = null;
      SharingObject sharingObject = null;
      while(rsShares.next()) {
        final String cgid = rsShares.getString("cgid");
        final String name = rsShares.getString("name");
        final String fname = rsShares.getString("fname");
        final String sharePartner = rsShares.getString("share_partner");
        final boolean optIn = rsShares.getBoolean("opt_in");
        int memberCount = 0;
        if (pstmtMemberCount != null) {
          pstmtMemberCount.setString(1, localId);
          pstmtMemberCount.setString(2, cgid);
          try (ResultSet rsMemberCount = pstmtMemberCount.executeQuery()) {
            rsMemberCount.next();
            memberCount = rsMemberCount.getInt("member_count");
          }
        }
        if (isSharedTo  &&  cgid.equals(previousCgid)) {
          if (sharePartner != null) sharingObject.addSharePartner(sharePartner);
        } else {
          previousCgid = cgid;
          if (sharingObject != null) {
            sharings.add(sharingObject);
          }
          sharingObject = new SharingObject();
          sharingObject.setContactGroupId(cgid);
          sharingObject.setContactGroupName(name);
          sharingObject.setContactGroupFormattedName(fname);
          sharingObject.setContactGroupMemberCount(memberCount);
          sharingObject.setSharedToAccepted(optIn);
          if (sharePartner != null) sharingObject.addSharePartner(sharePartner);
          sharingObject.setSharedTo(isSharedTo);
        }
      }
      if (sharingObject != null) {
        sharings.add(sharingObject);
      }
    }
    Database.logger.exiting(Database.CN, "getSharesCommon", sharings.size());
  }

  /**
   * The idea here is that we are given a complete set of revised sharing information,
   * both share-to and share-from. For the share-to items, the opt-in field is "owned"
   * by the sharing partner. For the share-from items, it's owned by this user, but
   * the rest of the record in the DB is owned by the sharing partner.
   */
  void updateShares(String localId, Map<SharingObject,SharingObject> shareToSharingObjects, Map<SharingObject,SharingObject> shareFromSharingObjects) {
    final Tx tx = getTx();
    final Connection cnx = getConnection(tx);

    try {
      updateSharesShareTos(localId, shareToSharingObjects, cnx);
      updateSharesShareFroms(localId, shareFromSharingObjects, cnx);
      tx.commit();
    } catch (final SQLException e) {
      try {
        tx.rollback();
        cnx.close();
      } catch (final SQLException e1) {
        // ignore since already throwing
      }
      final Throwable t = new IllegalStateException("Unable to update contact group sharing information for " + localId, e);
      Utils.logProblemAndPunt(t);
    }
  }

  /**
   * We get a list of all share-to DB records. For each one, if it's not among the
   * revised share-to items, remove it from the database. Otherwise, leave it alone.
   * Then, for any new items in the share-tos, create DB records.
   */
  private void updateSharesShareTos(String localId, Map<SharingObject,SharingObject> shareToSharingObjects, Connection cnx) throws SQLException {
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT ser, local_id, contact_group_id, share_to, share_to_opt_in " +
                             "FROM shares WHERE local_id = ? FOR UPDATE",
                             ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)) {
      pstmt.setString(1, localId);
      try (ResultSet rs = pstmt.executeQuery()) {
        updateSharesShareToExistingRows(localId, shareToSharingObjects, rs);
        rs.moveToInsertRow();
        updateSharesShareTosNewRows(localId, shareToSharingObjects, rs);
      }
    }
  }

  private void updateSharesShareToExistingRows(String localId, Map<SharingObject,SharingObject> shareToSharingObjects, ResultSet rs) throws SQLException {
    final String sharingUser = localId;
    // we're only using this object as a key into the Map
    final SharingObject reusableSO = new SharingObject();
    while (rs.next()) {
      final String contactGroupId = rs.getString("contact_group_id");
      final String receivingUser = rs.getString("share_to");
      reusableSO.setContactGroupId(contactGroupId);
      reusableSO.clearSharePartners();
      reusableSO.addSharePartner(receivingUser);
      reusableSO.setSharedTo(true);
      final SharingObject wasPresentInRevision = shareToSharingObjects.remove(reusableSO);
      if (wasPresentInRevision == null) {
        Database.logger.finest("Remove share: " + sharingUser +  " => " + contactGroupId + " => " + receivingUser);
        rs.deleteRow();
      }
    }
  }

  private void updateSharesShareTosNewRows(String localId, Map<SharingObject,SharingObject> shareToSharingObjects, ResultSet rs) throws SQLException {
    final String sharingUser = localId;
    for (final SharingObject sharingOjbect : shareToSharingObjects.values()) {
      final String contactGroupId = sharingOjbect.getContactGroupId();
      final Set<String> receivingUsers = sharingOjbect.getSharePartners();
      for (final String receivingUser : receivingUsers) {
        // there should be exactly one of these
        Database.logger.finest("Adding share: " + sharingUser + " => " + contactGroupId + " => " + receivingUser);
        rs.updateString("local_id", localId);
        rs.updateString("contact_group_id", contactGroupId);
        rs.updateString("share_to", receivingUser);
        rs.updateBoolean("share_to_opt_in", false);
        rs.insertRow();
      }
    }
  }

  /**
   * We get a list of all the share-from DB records. For each one, if it's not among
   * the revised share-from items, we set the opt-in flag to false. Ignore any "extra"
   * share-from items that don't have DB records.
   */
  private void updateSharesShareFroms(String localId, Map<SharingObject,SharingObject> shareFromSharingObjects, Connection cnx) throws SQLException {
    final String receivingUser = localId;
    try (PreparedStatement pstmt =
        cnx.prepareStatement("SELECT ser, local_id, contact_group_id, share_to, share_to_opt_in " +
                             "FROM shares WHERE share_to = ? FOR UPDATE",
                             ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)) {
      pstmt.setString(1, receivingUser);
      try (ResultSet rs = pstmt.executeQuery()) {
        final SharingObject reusableSO = new SharingObject();
        while (rs.next()) {
          final String contactGroupId = rs.getString("contact_group_id");
          final String sharingUser = rs.getString("local_id");
          reusableSO.setContactGroupId(contactGroupId);
          reusableSO.clearSharePartners();
          reusableSO.addSharePartner(sharingUser);
          reusableSO.setSharedTo(false);
          final SharingObject wasPresentInRevision = shareFromSharingObjects.remove(reusableSO);
          if (wasPresentInRevision != null) {
            final boolean optInDbValue = rs.getBoolean("share_to_opt_in");
            if (optInDbValue) {
              Database.logger.finest("Keep   opt-in: " + sharingUser + " => "+ contactGroupId + " => " + receivingUser);
            } else {
              Database.logger.finest("Apply  opt-in: " + sharingUser + " => "+ contactGroupId + " => " + receivingUser);
              rs.updateBoolean("share_to_opt_in", true);
              rs.updateRow();
            }
          } else {
            Database.logger.finest("Remove opt-in: " + sharingUser + " => "+ contactGroupId + " => " + receivingUser);
            rs.updateBoolean("share_to_opt_in", false);
            rs.updateRow();
          }
        }
      }
    }
  }
}
