package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

class Utils {
  private static final String CN = Utils.class.getName();
  private static final Logger logger = Logger.getLogger(Utils.CN);
  private static final SecureRandom random = new SecureRandom();

  // copied from
  // https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
  // In the old days, I would have used JDK DatatypeConverter, but that goes away in Java 11. I could
  // use something from Apache Commons Codec, but I only have that as a transitive dependency. I'd
  // rather not promote it to a primary dependency.
  private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
  /**
   * This is used in the validation of the proof token received from PHP, which
   * is already in this format.
   */
  static String bytesToHex(byte[] bytes) {
      final char[] hexChars = new char[bytes.length * 2];
      for (int j = 0; j < bytes.length; j++) {
          final int v = bytes[j] & 0xFF;
          hexChars[j * 2] = Utils.HEX_ARRAY[v >>> 4];
          hexChars[j * 2 + 1] = Utils.HEX_ARRAY[v & 0x0F];
      }
      return new String(hexChars);
  }

  static String hashToHex(String input) {
    if (input == null) {
      return null;
    }
    return Utils.hashToHex(input.getBytes(StandardCharsets.UTF_8));
  }

  static String hashToHex(byte[] input) {
    MessageDigest digest;
    try {
      digest = MessageDigest.getInstance("SHA-256");
    } catch (final NoSuchAlgorithmException e) {
      throw new IllegalStateException("MessageDigest doesn't support SHA-256, which should never happen. Tilt.");
    }
    final byte[] hashed = digest.digest(input);
    return Utils.bytesToHex(hashed);
  }


  static String durTs(Duration d) {
    if (d == null) return null;
    return d.toString().toLowerCase();
  }

  static String durTs(long millis) {
    return Utils.durTs(Duration.ofMillis(millis));
  }

  /**
   * For logging purposes. Similar to the function in the PHP script.
   * Just tries to be unique-ish. Not critical. User error messages will mention
   * the incident ID. If you are lucky, they will provide that when reporting
   * a problem, and you can just search for it in the logs.
   */
  static final String createIncidentId() {
    Utils.logger.entering(Utils.CN, "createIncidentId");
    final Random r = new Random();
    final int left = 1001 + r.nextInt(8999);
    final int right = 10001 + r.nextInt(89999);
    final String incidentId = left + "-" + Integer.toHexString(right).toUpperCase();
    Utils.logger.exiting(Utils.CN, "createIncidentId", incidentId);
    return incidentId;
  }

  /**
   * Logs and exception to stderr. Optionally also logs it to stdout if debugging
   * is enabled.
   * @param incidentId for identifying a logged item; generated if not provided
   * @param t
   * @return the incidentId, whether passed in or generated here
   */
  static String logException(String incidentId, Throwable t) {
    if (incidentId == null) {
      incidentId = Utils.createIncidentId();
    }
    final String incidentString = "Incident ID " + incidentId;
    Utils.logger.log(Level.WARNING, incidentString, t);
    return incidentId;
  }

  static void logDbProblemAndPunt(SQLException e) {
    final String incidentId = Utils.logException(null, e);
    final String message = "Database problem. Notify administrator, Incident ID " + incidentId + ".";
    System.out.println(message);
    throw new GContactException(message, e, ExitCode.DB);
  }

  static void logProblemAndPunt(Throwable t) {
    final String incidentId = Utils.logException(null, t);
    final String message = "Processing problem. Notify administrator, Incident ID " + incidentId + ".";
    System.out.println(message);
    throw new GContactException(message, t, ExitCode.OTHER);
  }

  /**
   * This is how we trust the PHP script to call us (and it's not something else). There is a
   * shared secret in the PHP and Java config files. The proof is the hash of that secret and
   * the local ID of the user. This is by no means foolproof, of course, but the thing it
   * protects is not very powerful.
   */
  static void proveIt(String localId, String proof) {
    Utils.logger.entering(Utils.CN, "proveIt", new Object[] {localId, proof});
    final String proofSecret = Config.getInstance(null).getProofSecret();
    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-256");
    } catch (final NoSuchAlgorithmException e) {
      final String incidentId = Utils.logException(null, e);
      final String message = "Validation problem. Notify administrator, Incident ID " + incidentId + ".";
      System.out.println(message);
      throw new GContactException(message, e, ExitCode.MDALG);
    }
    md.update((proofSecret + localId).getBytes(Charset.forName("utf8")));
    final byte[] digestBytes = md.digest();
    final String digest = Utils.bytesToHex(digestBytes);
    if (digest.equalsIgnoreCase(proof)) {
      if (Utils.logger.isLoggable(Level.FINEST)) {
        Utils.logger.finest("DIGEST\np=" + proof.toLowerCase() + ", \nd=" + digest.toLowerCase());
      }
    } else {
      final Throwable t = new IllegalAccessException("PROOF mismatch, p=" + proof + ", d=" + digest);
      final String incidentId = Utils.logException(null, t);
      final String message = "Validation problem. Notify administrator, Incident ID " + incidentId + ".";
      System.out.println(message);
      throw new GContactException(message, t, ExitCode.PROOF);
    }
    Utils.logger.exiting(Utils.CN, "proveIt");
  }

  static byte[] toJpeg(byte[] input, String contentType) {
    final ByteArrayInputStream bais = new ByteArrayInputStream(input);
    BufferedImage image;
    try {
      image = ImageIO.read(bais);
    } catch (final IOException e) {
      Utils.logger.info("Exception reading photo for conversion from " + contentType + " to JPEG :" + e);
      return null;
    }
    if (image == null) { //couldn't cope
      return null;
    }
    image = Utils.ensureOpaque(image);
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    boolean success;
    try {
      success = ImageIO.write(image, "jpg", baos);
    } catch (final IOException e) {
      Utils.logger.info("Exception writing photo for conversion from " + contentType + " to JPEG :" + e);
      return null;
    }
    if (!success) {
      return null;
    }
    return baos.toByteArray();
  }

  /**
   * Taken from here, along with the advice.
   * https://stackoverflow.com/a/57626892/6362522
   * https://stackoverflow.com/a/17845696/6362522
   */
  private static BufferedImage ensureOpaque(BufferedImage bi) {
    if (bi.getTransparency() == Transparency.OPAQUE) {
        return bi;
    }
    final int w = bi.getWidth();
    final int h = bi.getHeight();
    final int[] pixels = new int[w * h];
    bi.getRGB(0, 0, w, h, pixels, 0, w);
    final BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
    bi2.setRGB(0, 0, w, h, pixels, 0, w);
    return bi2;
  }

  public static SecureRandom getRandom() {
    return Utils.random;
  }

  /**
   * For a few hundred rows and not much parallel activity, there is no danger of doing wholesale
   * updates all wrapped in a single transaction. Maybe that is your scenario. But if you have a
   * lot of users doing updates or maybe an atypically large number of contacts or contact group
   * members, maybe you'll get into transaction timeout trouble or other typical DB woes. This
   * enum corresponds to the similar Java config item so that you have some control over how big
   * your transactions are. You can even decide to not use transactions at all (which would be a
   * handy choice if you are using a DB that doesn't support transactions (like some MySQL table
   * engines).
   */
  static enum TxGranularity {
    NONE,  // no transactions are used
    SMALL, // transactions are committed after every fetch page
    LARGE; // (DEFAULT) transactions are committed after all fetching
    static TxGranularity getInstance(String granularity) {
      if (granularity == null) return LARGE;
      final char g = granularity.toLowerCase().charAt(0);
      switch (g) {
        case 'n': return NONE;
        case 's': return SMALL;
        case 'l': return LARGE;
        default:
          throw new IllegalArgumentException("The value '" + granularity + "' is not valid for TransactionGranularity.");
      }
    }
  }

  static class Config {
    private static final ClassLoader CLASS_LOADER = GContact.class.getClassLoader();
    private static final String CN = Config.class.getName();
    private static Logger logger = Logger.getLogger(Config.CN);
    private static Config config;
    private Properties properties;
    static Config getInstance(String configFileName) {
      if (Config.config == null) {
        Config.config = new Config(configFileName);
      }
      return Config.config;
    }
    private Config(String configFileName) {
      Config.logger.entering(Config.CN, "Config", configFileName);
      properties = new Properties();
      try (InputStream configStream = (configFileName == null)
          ? Config.CLASS_LOADER.getResourceAsStream("gcontact.properties")
          : new FileInputStream(configFileName)) {
        properties.load(configStream);
        final Properties temp = new Properties();
        // convert to lowercase for CI lookups
        for (final String name : properties.stringPropertyNames()) {
          final String value = properties.getProperty(name);
          temp.setProperty(name.toLowerCase(), value);
        }
        properties = temp;
        initLogging();
      } catch (final Exception e) {
        final String incidentId = Utils.logException(null, e);
        final String message = "Configuration problem. Notify administrator, Incident ID " + incidentId + ".";
        System.out.println(message);
        throw new GContactException(message, e, ExitCode.CONFIG);
      }
      Config.logger.exiting(Config.CN, "Config");
    }

    private void initLogging() {
      final InputStream loggingConfig = getLoggingConfiguration();
      if (loggingConfig != null) {
        try {
          LogManager.getLogManager().readConfiguration(loggingConfig);
        } catch (SecurityException | IOException e) {
          // too deeply in trouble ... punt and ignore
        }
      }
    }
    // In all the getters, use lowercase property names
    String getDbUsername() {
      return properties.getProperty("dbusername");
    }
    String getDbPassword() {
      return properties.getProperty("dbpassword");
    }
    String getDbConnectionString() {
      return properties.getProperty("dbconnectionstring");
    }
    String getGoogleClientId() {
      return properties.getProperty("googleclientid");
    }
    String getGoogleClientSecret() {
      return properties.getProperty("googleclientsecret");
    }
    String getProofSecret() {
      return properties.getProperty("proofsecret");
    }
    String getApplicationName() {
      return properties.getProperty("applicationname", "Google contacts fetcher");
    }
    String getOauth2RedirectUrl() {
      return properties.getProperty("oauth2redirecturl");
    }
    int getFetchPageSize() {
      final String p = properties.getProperty("fetchpagesize", "100");
      return Integer.parseInt(p);
    }
    TxGranularity getTxGranularity() {
      final String p = properties.getProperty("transactiongranularity", null);
      return TxGranularity.getInstance(p);
    }
    Duration getContactsCacheTtl() {
      final String p = properties.getProperty("contactscachettl", "PT36H");
      return Duration.parse(p);
    }
    boolean getResolvePhotos() {
      final String resolvePhotos = properties.getProperty("resolvephotos", "true");
      return Boolean.parseBoolean(resolvePhotos);
    }
    boolean getConvertPhotosToJpeg() {
      final String p = properties.getProperty("convertphotostojpeg", "false");
      return Boolean.parseBoolean(p);
    }
    Duration getResolvePhotosConnectionTimeout() {
      final String p = properties.getProperty("resolvephotosconnectiontimeout", "PT5S");
      return Duration.parse(p);
    }
    Duration getResolvePhotosReadTimeout() {
      final String p = properties.getProperty("resolvephotosreadtimeout", "PT10S");
      return Duration.parse(p);
    }
    InputStream getLoggingConfiguration() {
      final String p = properties.getProperty("loggingconfigfile");
      if (p != null) {
        final File logginfConfigFile = new File(p);
        if (logginfConfigFile.canRead()) {
          try {
            return new FileInputStream(logginfConfigFile);
          } catch (final FileNotFoundException e) {
            // can't happen since I already checked unless the file
            // changed from readable in the brief window of these few lines
          }
        }
      }
      return null;
    }
    Duration getRefresherPeriodicity() {
      final String p = properties.getProperty("refresherperiodicity", "PT12H");
      return Duration.parse(p);
    }
    Duration getRefresherStartupDelay() {
      final String p = properties.getProperty("refresherstartupdelay", "PT5M");
      return Duration.parse(p);
    }
    int getRefresherQueryLimit() {
      final String p = properties.getProperty("refresherquerylimit", "100");
      return Integer.parseInt(p);
    }

    int getRefresherWorkerCount() {
      final String p = properties.getProperty("refresherworkercount", "2");
      return Integer.parseInt(p);
    }
    float getRefresherFullProbability() {
      final String p = properties.getProperty("refresherfullprobability", "0.1");
      return Float.parseFloat(p);
    }
    InetAddress getResponderAddress() {
      final String p = properties.getProperty("responderaddress");
      if (p == null) {
        return InetAddress.getLoopbackAddress();
      }
      try {
        return InetAddress.getByName(p);
      } catch (final UnknownHostException e) {
        throw new IllegalArgumentException("Bad value for config property responderAddress: " + p, e);
      }
    }
    int getResponderPort() {
      final String p = properties.getProperty("responderport", "7373");
      return Integer.parseInt(p);
    }
    boolean getResponderUseTls() {
      final String p = properties.getProperty("responderusetls", "false");
      return Boolean.parseBoolean(p);
    }
    File getResponderKeystoreFile() {
      final String p = properties.getProperty("responderkeystorefile");
      if (p == null) {
        return null;
      }
      return new File(p);
    }
    String getResponderKeystoreSecret() {
      return properties.getProperty("responderkeystoresecret");
    }
    String getResponderKeySecret() {
      return properties.getProperty("responderkeysecret");
    }
    String getResponderPathPattern() {
      return properties.getProperty("responderpathpattern", "/gcontact/actions/");
    }
    String getResponderServerInfo() {
      return properties.getProperty("responderserverinfo", "gcontact");
    }
    boolean getResponderUseDigestAuthentication() {
      final String p = properties.getProperty("responderusedigestauthentication", "true");
      return Boolean.parseBoolean(p);
    }
    String getResponderAuthenticationRealm() {
      return properties.getProperty("responderauthenticationrealm", getApplicationName());
    }
    public String getResponderServerDigestSecret() {
      return properties.getProperty("responderserverdigestsecret");
    }
    Duration getResponderNonceTimeTolerance() {
      final String p = properties.getProperty("respondernoncetimetolerance", "pt10s");
      return Duration.parse(p);
    }
  }
}
