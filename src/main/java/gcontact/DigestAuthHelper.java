package gcontact;

import java.security.SecureRandom;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Adapted from https://gist.github.com/usamadar/2912088
 * see http://usamadar.com/2012/06/11/implementing-http-digest-authentication-in-java/
 * But I've changed it so much that it's hardly recognizable. Still, credit where
 * credit is due. It gave me a good starting point.
 */
class DigestAuthHelper {
  private static final String CN = DigestAuthHelper.class.getName();
  private static final Logger logger = Logger.getLogger(DigestAuthHelper.CN);

  private final String realm;
  private final String sharedSecret;
  private final String digestSecret;
  private final Duration timeTolerance;

  private final SecureRandom random = Utils.getRandom();

  DigestAuthHelper(String realm, String sharedSecret, String digestSecret, Duration timeTolerance) {
    this.realm = realm;
    this.sharedSecret = sharedSecret;
    if (digestSecret == null) {
      final byte[] digestSecretBytes = new byte[50];
      random.nextBytes(digestSecretBytes);
      this.digestSecret = Utils.hashToHex(digestSecretBytes);
    } else {
      this.digestSecret = digestSecret;
    }
    this.timeTolerance = timeTolerance;
  }

  String authenticate(String authHeader, String httpMethod) {
    final Map<String, String> clientProvidedItems = parseClientResponseContents(authHeader);
    final String userName = clientProvidedItems.get("username");
    final String nonce = clientProvidedItems.get("nonce");
    final String opaque = clientProvidedItems.get("opaque");

    if (!validateOpaqueAndNonce(opaque, nonce)) {
      final String s = "opaque/nonce fails validation, userName " + userName;
      throw new IllegalStateException(s);
    } else {
      DigestAuthHelper.logger.fine("opaque/nonce passed validation, userName " + userName);
    }
    final String qop = clientProvidedItems.get("qop");
    if (qop == null  ||  qop.length() == 0) { // with GuzzleHttp, this doesn't happen
      final String s = "Client response included no qop value, userName " + userName;
      throw new IllegalStateException(s);
    }
    final String clientResponseRealm = clientProvidedItems.get("realm");
    if (!realm.equals(clientResponseRealm)) {
      final String s = "Response realm did not match, realm '" + realm + "', userName " + userName;
      throw new IllegalStateException(s);
    }
    // Digest auth spec says that hex representation should be in lowercase,
    // which matters for these composit hashes.
    final String nonceCount = clientProvidedItems.get("nc");
    final String clientNonce = clientProvidedItems.get("cnonce");
    final String reqURI = clientProvidedItems.get("uri");
    final String ha1 = Utils.hashToHex(userName + ":" + realm + ":" + sharedSecret).toLowerCase();
    final String ha2 = Utils.hashToHex(httpMethod + ":" + reqURI).toLowerCase();
    final String serverComputationInput = ha1 + ":" + nonce + ":" + nonceCount + ":" + clientNonce + ":" + qop + ":" + ha2;
    final String serverComputation = Utils.hashToHex(serverComputationInput);
    final String clientResponse = clientProvidedItems.get("response");
    if (!serverComputation.equalsIgnoreCase(clientResponse)) {
      final String s = "Client response did not match server expectation '" + serverComputation + "', userName " + userName;
      throw new IllegalStateException(s);
    }
    return userName;
  }

  /**
   * Gets the Authorization header string minus the "AuthType" and returns a
   * hashMap of keys and values
   */
  private Map<String, String> parseClientResponseContents(String headerString) {
    if (headerString == null  ||  headerString.length() == 0) {
      final String s = "Authorization header was missing or empty.";
      throw new IllegalStateException(s);
    }
    if (!headerString.startsWith("Digest ")) {
      final String s = "Unexpected Authorization header contents: " + headerString;
      throw new IllegalStateException(s);
    }
    // separate out the part of the string which tells you which Auth scheme is it
    // TODO: some sanity checking?
    final String headerStringWithoutScheme = headerString.substring(headerString.indexOf(" ") + 1).trim();
    final Map<String, String> values = new HashMap<String, String>();
    final String keyValueArray[] = headerStringWithoutScheme.split(",");
    for (final String keyval : keyValueArray) {
      if (keyval.contains("=")) {
        final String key = keyval.substring(0, keyval.indexOf("="));
        final String value = keyval.substring(keyval.indexOf("=") + 1);
        values.put(key.trim(), value.replaceAll("\"", "").trim());
      }
    }
    DigestAuthHelper.logger.exiting(DigestAuthHelper.CN, "parseClientResponseContents" + values);
    return values;
  }

  String createServerHeaderContents() {
    final String nonce = createNonce();
    final String opaque = calculateOpaqueForNonce(nonce);
    final String header = "Digest algorithm=SHA-256,qop=auth,"
        + "realm=\"" + realm + "\","
        + "nonce=\"" + nonce + "\","
        + "opaque=\"" + opaque + "\"";

    DigestAuthHelper.logger.exiting(DigestAuthHelper.CN, "createServerHeaderContents" + header);
    return header;
  }

  private String createNonce() {
    final byte[] randomBytes = new byte[50];
    random.nextBytes(randomBytes);
    final String randomString = Utils.hashToHex(randomBytes);
    final long stamp = System.currentTimeMillis();
    final String nonce = randomString + ":" + stamp;
    DigestAuthHelper.logger.exiting(DigestAuthHelper.CN, "createNonce" + nonce);
    return nonce;
  }

  /**
   * We have a secret value not known to clients. Our opaque value is a hash that includes
   * that secret value and the nonce. Therefore, a client cannot forge a server nonce. We
   * check that by repeating the calculation with the client-returned server nonce.
   */
  private String calculateOpaqueForNonce(String nonce) {
    return Utils.hashToHex(digestSecret + realm + nonce);
  }

  private boolean validateOpaqueAndNonce(String clientOpaque, String nonce) {
    final String expectedOpaque = calculateOpaqueForNonce(nonce);
    if (!expectedOpaque.equals(clientOpaque)) {
      DigestAuthHelper.logger.fine("Expected opaque '" + expectedOpaque + "', response opaque '" + clientOpaque + "'");
      return false;
    }
    final String[] nonceSplit = nonce.split(":");
    if (nonceSplit.length != 2) {
      DigestAuthHelper.logger.fine("Malformed response nonce '" + nonce + "'");
      return false;
    }
    long stamp;
    try {
      stamp = Long.parseLong(nonceSplit[1]);
    } catch (final NumberFormatException e) {
      DigestAuthHelper.logger.fine("Malformed response nonce stamp '" + nonceSplit[1] + "'");
      return false;
    }
    final Duration staleness = Duration.ofMillis(System.currentTimeMillis() - stamp);
    DigestAuthHelper.logger.fine("Response nonce returned after " + staleness.toString().toLowerCase());
    if (timeTolerance.minus(staleness).isNegative()) {
      DigestAuthHelper.logger.warning("Response had stale nonce, " + staleness.toString().toLowerCase());
      return false;
    }
    return true;
  }
}
