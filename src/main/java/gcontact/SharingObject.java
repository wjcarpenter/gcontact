package gcontact;

/*
 * https://gitlab.com/wjcarpenter/gcontact
 * See LICENSE file or https://gitlab.com/wjcarpenter/gcontact/blob/master/LICENSE
 * for license details.
 */

import java.util.Set;
import java.util.TreeSet;

/**
 * Information about sharing of contact groups is kind of complex, so
 * it's wrapped up in this class. For a shared-to sharing, there can
 * be multiple sharePartners. For a shared-from, there will be only one.
 * share-to acceptance is only relevant for shared-from cases. It
 * represents the receiving user's opt-in.
 *
 * The equals() and hashCode() methods are based on only a subset
 * of the information. It's perfectly valid for the current use
 * cases, but be careful when expanding the use cases.
 *
 * It's impossible to use the terms "share to" and "share from" in a
 * way that is intuitive in all the places they crop up. C'est la vie.
 */
class SharingObject {
  private String contactGroupId;
  private String contactGroupName;
  private String contactGroupFormattedName;
  private int contactGroupMemberCount;
  private final Set<String> sharePartners = new TreeSet<>();
  private boolean isSharedTo;
  private boolean isSharedToAccepted = false;

  public String getContactGroupId() {
    return contactGroupId;
  }

  public void setContactGroupId(String contactGroupId) {
    this.contactGroupId = contactGroupId;
  }

  public String getContactGroupName() {
    return contactGroupName;
  }

  public void setContactGroupName(String contactGroupName) {
    this.contactGroupName = contactGroupName;
  }

  public String getContactGroupFormattedName() {
    return contactGroupFormattedName;
  }

  public void setContactGroupFormattedName(String contactGroupFormattedName) {
    this.contactGroupFormattedName = contactGroupFormattedName;
  }

  public Set<String> getSharePartners() {
    return sharePartners;
  }

  void addSharePartner(String sharedTo) {
    this.sharePartners.add(sharedTo);
  }

  void clearSharePartners() {
    this.sharePartners.clear();
  }

  public int getContactGroupMemberCount() {
    return contactGroupMemberCount;
  }

  public void setContactGroupMemberCount(int contactGroupMemberCount) {
    this.contactGroupMemberCount = contactGroupMemberCount;
  }

  public boolean isSharedTo() {
    return isSharedTo;
  }

  public void setSharedTo(boolean isSharedTo) {
    this.isSharedTo = isSharedTo;
  }

  public boolean isSharedToAccepted() {
    return isSharedToAccepted;
  }

  public void setSharedToAccepted(boolean isSharedToAccepted) {
    this.isSharedToAccepted = isSharedToAccepted;
  }

  private String getSingleSharePartner() {
    String fsp = null;
    for (final String sharePartner : sharePartners) {
      fsp = sharePartner;
      break;
    }
    return fsp;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    final String thisSingleSharePartner = getSingleSharePartner();
    result = prime * result + ((contactGroupId == null) ? 0 : contactGroupId.hashCode());
    result = prime * result + ((thisSingleSharePartner == null) ? 0 : thisSingleSharePartner.hashCode());
    result = prime * result + (isSharedTo ? 1231 : 1237);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final SharingObject other = (SharingObject) obj;
    if (contactGroupId == null) {
      if (other.contactGroupId != null)
        return false;
    } else if (!contactGroupId.equals(other.contactGroupId))
      return false;
    final String thisSingleSharePartner = getSingleSharePartner();
    final String otherSingleSharePartner = other.getSingleSharePartner();
    if (thisSingleSharePartner == null) {
      if (otherSingleSharePartner != null)
        return false;
    } else if (!thisSingleSharePartner.equals(otherSingleSharePartner))
      return false;
    if (isSharedTo != other.isSharedTo)
      return false;
    return true;
  }
}
