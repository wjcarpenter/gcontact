# Installation for _gcontact_

## Introduction

This is a long document because there are a lot of steps.
Each step is straightforward, but it's something I couldn't readily reduce to a simple click-next install wizard.
There are a lot of different ways that things could be put together, 
and if you are an industrious and clever person, you could just use this document as a "getting started" guide.
For everyone else, I'll lead you through the steps of how I installed things, 
which is a logical order that will lead to success without too much wasted effort.

## Location, location, location

I probably update my _gcontact_ stuff more than anybody. 
Here is the system I use to keep from clobbering local customizations (again :innocent:):

* Everything is stored outside of my webserver document root, 
  except for a PHP page (not part of _gcontact_) that PHP-includes `gcontact-auth.php`.
  (For my convenience, I have a symlink into my configuration directory, 
  but PHP resolves the symlink to figure out where it really lives.)
* The _MyVirtualDirectory_ package is stored under `/usr/local/lib/myvd/myvd-server-1.0.6/`.
* The _gcontact_ package is stored under `/usr/local/lib/gcontact/gcontact-x.y.z/`,
  and I make a symlink from there to `/usr/local/lib/gcontact/gcontact/`.
* I have a directory, `/etc/gcontact/` where I collect all of the configuration and customizations.
* `gcontact-auth.cfg` is stored in that config directory.
  I have a symlink from there to `/usr/local/lib/gcontact/gcontact/php/gcontact-auth.cfg`.
* `myvd.conf` is stored in that config directory.
  I have a symlink from there to `/usr/local/lib/myvd/myvd-server-1.0.6/conf/myvd.conf`.
* I customized the _MyVirtualDirectory_ logging configuration, so `log4j2.xml` is stored in that config directory.
  I have a symlink from there to `/usr/local/lib/myvd/myvd-server-1.0.6/conf/log4j2.xml`.
* The config directory also holds `gcontact-client-secrets.json`, `gcontact.logging.properties`, and `gcontact.properties`,
  all without any symlinks because they are all referenced directly.
* I have symlinks from all of the Java JAR files in `/usr/local/lib/gcontact/gcontact/lib/` to
  `/usr/local/lib/myvd/myvd-server-1.0.6/lib/`.
* When I install a new version of _gcontact_,
  I update the symlink `/usr/local/lib/gcontact/gcontact/` to reference the new location.
  Then I delete the symlinks from `/usr/local/lib/myvd/myvd-server-1.0.6/lib/` and remake them.
* I also remake the symlink from `/etc/gcontact/gcontact-auth.cfg` 
  to `/usr/local/lib/gcontact/gcontact/php/gcontact-auth.cfg`. 

## Development

Unless you are lucky and happen to have an environment like mine, 
you will have one or two small development tasks to do.

* Authentication in PHP.
You must provide a PHP function that gives back the calling user's local identity as a string.
* Authentication in _MyVirtualDirectory_.
You must provide a _MyVirtualDirectory_ insert that authenticates the calling user,
given their LDAP distinguished name.
I provide a configuration that authenticates against an email server,
and _MyVirtualDirectory_ provides some others.
If you can't use one of those, you will have to write your own.

## Dependencies

Here are the dependencies for _gcontact_. More details are provided later in this document.

* The _gcontact_ package itself. It is written mostly in Java. 
You can download the sources and build it yourself,
or you can download the latest pre-built release bundle.
The advantage of downloading the release bundle is that it also includes most of the dependent JARs.
* Java. I built it with OpenJDK 11 and tested it with OpenJDK Java 8, but it should work fine with any later Java release.
* MySQL. You will need both a MySQL server and the MySQL JDBC driver for Java.
Any recent MySQL server release should work.
If you decide to use some other database server, you might have some porting work to do.
* A PHP server, which is needed for the interactive OAuth2 handshake.
* Google client API for the People Service for PHP.
* Google client API for the People Service for Java.
* _MyVirtualDirectory_ LDAP server.
You can get the source for that from here:
https://github.com/TremoloSecurity/MyVirtualDirectory.

* _WJC MyVD Custom Inserts_ collection. 
You can get both source and the pre-built JAR for that from here:
https://gitlab.com/wjcarpenter/wjc-myvd-custom-inserts.

## Access control

This is optional, but recommended.
It's how I set things up for my environment.

* Find out what userid will be used to run the PHP script on your system. 
For Debian-derived systems, for example, it's `www-data`.
* Figure out what userid will be used to run _MyVirtualDirectory_ (described below).
* Create a new group. `gcontact` would be a good name, but it's arbitrary.
* Add the userids from the previous bullets to that group in `/etc/group` 
(not needed for a userid of `root`).
* For the sensitive files mentioned later, provide group read access to the group you just created.
Provide _no_ access to "other". 
The owner should be some privileged system user or a trusted user with which you might edit those files.
For example, you might set those files with owner `root`, group `gcontact`, and permissions of `640` (`-rw-r-----`).  

## Java

Install Java 8 or later.
If you don't know how to get Java, I suggest installing pre-built binaries from https://adoptopenjdk.net/.
To run things, you will only need the JRE.
For _MyVirtualDirectory_, you will need to build it with the JDK
(you can do that on a different machine and copy the built artifacts to your server if you don't want to have the JDK on your server).
You will also need the JDK if you choose to build _gcontact_ or _WJC MyVD Custom Inserts_ collection,
though pre-built binaries are available for both of those.

## MySQL

Install MySQL server.
If you don't know how to get MySQL, you can find it here: https://www.mysql.com/.
MySQL Community Edition from https://www.mysql.com/products/community/ will work.
Any recent release of MySQL should work fine.
If you don't have much experience with MySQL, 
I recommend you also get and use `phpMyAdmin` (https://www.phpmyadmin.net/) 
to simplify many mundane tasks and keep track of many details for you.

_gcontact_ uses a single database.
You can call the database anything you want, 
but for the purposes of discussion, I'll assume you will call it `gcontact`.
You will find the schema for the database in `sql/gcontact.sql`.
You can import it directly into MySQL.
It has some metadata specific to `phpMyAdmin`;
if you have not installed `phpMyAdmin`,
you'll want to trim those parts out before importing.

Of course, you will need some credentials for accessing the database.
I recommend creating a userid/password specifically for this use
rather than using some set of common credentials.
In fact, an even better practice would be to create a "read-only" userid
(for use by _MyVirtualDirectory_)
and a separate "read-write" userid (for use by the `gcontact` Java application).
In any case, the userid only needs access to do the standard CRUD data operations
(create, retrieve, update, delete).
No metadata permissions are needed.
If you create a "read-only" userid, it only needs retrieve permissions.

You will need the MySQL JDBC driver, 
which you will find included in the pre-built _gcontact_ bundle.
You can download it from https://www.mysql.com/products/connector/.
In that case, keep track of the location where the driver JAR file is placed.
You will need it later.

## The _gcontact_ Java application

_gcontact_ is structured as a `gradle` application.
If you choose to build it from sources and are already familiar with `gradle`,
you should have no trouble finding things.
If you are not familiar with `gradle`, it's still pretty simple.
Of course, you must install `gradle`, which you can find at https://gradle.org/.
From the _gcontact_ top level directory,
run `./gradlew build` (for Unix-like systems) or `.\gradlew.bat build` (for Windows).
`gradle` will automatically find and download the dependencies.
That includes the Google client APIs for Java for the People Service
(but not the corresponding PHP API).
The `build` step will produce, among other things, TAR and ZIP archives of _gcontact_ and its dependencies.
You will find those under `gcontact/build/distributions/`.
If you are instead downloading one of the pre-built bundles, that is exactly what you are getting.

If you decide to build _gcontact_ yourself but do not use `gradle`,
you will have to track down the dependencies yourself.
(One easy way to do that would be to take those JARs from the pre-built bundles.)

Unpack the TAR or ZIP file in any convenient permanent location.
You will find that it includes a `lib/` directory with all of the interesting JAR files,
and there is a `bin/` directory with scripts for both Unix-like systems and Windows
to do the work to put everything on the Java classpath and so on.

The _gcontact_ PHP script invokes the _gcontact_ Java application.
Therefore, all of the files in the unpacked bundle must be readable by the user under which PHP runs.
The scripts must be executable by that user.
There is no security issue with making them world-readable and world-executable.
Refer to the _Access control_ section above.

The unpacked bundle also includes a sample configuration file, `gcontact.properties`.
You will definitely need to customize that configuration file,
and you will find comments within it describing what you need to supply.
The _gcontact_ Java application will try to find `gcontact.properties` along
the Java classpath, but it will not find the supplied sample file.
The best thing to do would be to copy the sample to some convenient location,
and explicitly supply that location by invoking the application as
`gcontact --config /some/path/to/gcontact.properties`.
Since it will contain the password for the "read-write" database user,
you want to restrict permissions on it so that it can only be read by the user under which PHP runs
and the user under which _MyVirtualDirectory_ runs.
Refer to the _Access control_ section above.
(To find out what that PHP user is, consult your web server documentation.
The _MyVirtualDirectory_ user will be covered in a later section.)

Logging in the _gcontact_ Java application is done via `java.util.logging`.
If you are not familiar with that, you can read about it here:
https://docs.oracle.com/javase/8/docs/api/java/util/logging/package-summary.html.
An internet search will provide many useful tutorials for actually configuring it.

You might have to revisit `gcontact.properties` a few times to update it as you go through other installation steps.
Don't forget to check its owner, group, and permissions when you are done; 
it's easy to modify them without realizing it.

## PHP

_gcontact_ uses a single PHP script for orchestrating the OAuth2 handshake with Google.
The script also does a few other administrative things for the user.
You can find that script under `php/gcontact-auth.php`.
You will need a PHP server, 
which is typically installed as an add-on to a web server like Apache httpd or nginx.
PHP has not had a completely smooth history of upward compatibility,
but if you install any recent version (7.x or later) of PHP it should be fine.
The PHP script used by _gcontact_ is not especially tricky.

It's up to you to decide how to make the _gcontact_ PHP script available to users.
If you are using a web content framework like _Joomla!_ or _Drupal_,
you may have to research a way to expose a standalone (or nearly standalone) PHP script.
In any case, there are two important things to consider in this decision:

* You will want to have a way to authenticate the user or prove the user is already authenticated.
You want this because you are associating the user's Google information with the user's local identity.
You don't want a user to be able to see another user's contacts.
* The script is written in the common style of redirecting back to itself as it goes through stages of processing.
One of those stages is a redirection back from Google during the interactive OAuth2 handshake.
That redirection URI must be registered with Google and be an exact match.

As an example, here is how set it up in my environment, where the web site runs on _Joomla!_:

* I installed a plugin called _DirectPHP_ that, as the name suggests, allows direct use of PHP in a _Joomla!_ page.
(There are a handful of such plugins available. I chose that one because it was simple to use, and my needs were also simple.)
* I put a copy of the PHP script at `/some/path/to/gcontact-auth.php`.
(Right beside it, I put the config file `gcontact-auth.cfg`. More about that below.)
* I created a _Joomla!_ article whose only content is the single line `<?php include '/some/path/to/gcontact-auth.php';?>`.
* In the _Joomla!_ menu system, I made an entry pointing to that article and gave it the alias `gcontact`.
* I used that URI (my site plus the path `/gcontact`) when I registered my application with Google.   

The _gcontact_ PHP script needs some configuration.
You can find a sample at `php/gcontact-auth.cfg`.
You will definitely need to customize some of those settings.
You will find all of the settings documented in the main `php/gcontact-auth.php` script,
but you want to make your changes in the `cfg` companion, which will override the defaults.
Both the PHP script and the configuration file must be readable by the user under which PHP runs.
Because the configuration file contains a secret string shared between the PHP script and the Java application,
you will want to restrict access so that only the PHP user can read it.
Refer to the _Access control_ section above.

You will have to provide the implementation for a PHP function
that returns a string giving the user identity of the calling user.
A very simple example is given in the _gcontact_ PHP script and the sample config file,
where the user identity is obtained from the _Joomla!_ framework.
If you are not using _Joomla!_, you will have to obtain that information in some other site-specific way.
For testing purposes, you could return some constant string to indicate your test user.

Logging for the _gcontact_ PHP script is sent to the PHP `error_log` function,
so it will show up wherever other PHP error logs go.
That's likely to be wherever your web server error logging goes.

You might have to revisit `gcontact-auth.cfg` a few times to update it as you go through other installation steps.
Don't forget to check its owner, group, and permissions when you are done; 
it's easy to modify them without realizing it.

### Visible content sections

There is a lot of visible text for users rendered by the PHP script.
I tried to make it generic enough so that it will seem natural to your users
with only simple customizations in `gcontact-auth.cfg`.
However, it's possible to make much grander changes to that content without modifying `gcontact-auth.php`.

Have a look at the array `$gcfConfig["section"]`.
It includes direct elements for each section of the tabbed web page.
Nested within each of those elements is another array element `"content"`.
The `"content"` element is an array of items to be rendered when the tabbed section is displayed.
You can read the comments for `$gcfConfig["section"]` to see what kinds of things you can have.
You can replace individual items or the entire `"content"` array.
It's up to you to make sure the combinations and orderings are sensible, of course.

The whole scheme of multiply nested arrays can be a little confusing,
so have a look at the overall structure of `$gcfConfig["section"]`.
Then look further down in the actual PHP code section to see how individual items can be replaced.

### Other short strings

A variety of very small strings that show up on the web page are listed in the `$gcfConfig["strings"]` array.
I don't think you will need to change any of those, but that's where they are.
My goal is that you shouldn't be tempted to modify `gcontact-auth.php` for small changes.
Instead, you should be able to customize things via `gcontact-auth.cfg`.

### Styles

I can't know the visual styling for the rest of your site.
Additionally, I don't know the mechanics of how you are invoking `gcontact-auth.php`.
In a generic situation, you would do whatever you wanted with your own CSS.
In my case, and perhaps in yours, that's not possible.
(I'm strictly within the HTML `<body>` section in my environment.)

Consequently, I have used inline style attributes for many items.
Have a look at the array `$gcfConfig["cssStyles"]`.
You can replace individual items in your `gcontact-auth.cfg`.
Or, if you are able to use a real CSS, you can empty out the whole `$gcfConfig["cssStyles"]` array.

### Images

There are a handful of images referenced from the PHP script.
For maximum portability, they are read by the PHP script and embeeded at `data:` URIs in the generated HTML.
For most of the images, both JPG and PNG versions are provided.
The PNGs generally look better, but the JPGs are smaller (which could matter for `data:` URIs).
You could make either of these changes if you wanted to 
(via the `$gcfConfig["section"]` array `"content"` mechanism described earlier):

* Switch from JPG to PNG, leaving the `data:` URI mechanism in place.
That's a very simple change.

* Copy the images to some location on your server where they could be
served directly, and replace the `data:` URI mechanism with normal `<a href>` references.

## Google client APIs for the People Service for PHP

Google provides client APIs for many different languages and platforms.
Because they serve many and varied needs, 
they are layered in such a way that you might have a little difficulty following the chain of dependencies.
Fortunately, for PHP in particular, there is an easy way to install what you need
with a tool called `Composer`.
Follow the installation instructions from here: https://developers.google.com/people/quickstart/php.
That will leave you with an autoload file wherever you did the install,
probably named `vendor/autoload.php`.
You will supply the path to that autoload file in the PHP script configuration item `$gcfConfig["autoload_php"]`.

If you decide to not use the `Composer` tool for installing the Google PHP dependencies, 
well ... good luck!
Google does provide alternative installation instructions at the page mentioned,
but it will be up to you to make sure those dependencies are found by the _gcontact_ PHP script.

## Register your application with Google

Once you know what the URI for the _gcontact_ PHP script will be,
you can register your application with Google.
(Actually, you can change your mind about the URI later.)
You _must_ register your application with Google for two reasons.
First, you must obtain a Google-provided clientId and client secret
that must be provided with Google API calls.
Second, the redirect URI that you use must exactly match one of the
redirection URIs that are part of your application metadata on Google.

To be quite honest, the Google documentation about this process might leave you confused,
though creation of a new project does lead you through some of the necessary steps.
You want to register your application by going to the Google developer console
at https://console.developers.google.com/apis/dashboard,
but you might find this non-Google explanation helpful in getting started: 
https://stackoverflow.com/questions/21786797/google-developers-console-how-do-i-register-a-new-application.
Once you have registered your application, be sure to do these things:

* Domain verification for the server where you will host the _gcontact_ PHP script.
This is a fairly quick process and just verifies that you control the domain.
* Create at least one set of application credentials.
You want to create an OAuth2 client ID for a web application,
which consists of a client ID and client secret. 
Use the download link to obtain the JSON file containing those credentials.
The configuration for the _gcontact_ PHP script has the path of that JSON file,
and the configuration for the _gcontact_ Java application has settings for the two individual pieces of the credential.
The JSON file should have permissions that allow reading only by the user under which PHP runs.
Refer to the _Access control_ section above.

* Customize the OAuth2 consent screen.
An optional part of that process is submitting your application to Google for verification.
The verification can take a couple of months, or it can take only a couple of days.
Until the process is completed, the interactive consent process will contain a warning that the application has not been verified by Google.
The user can ignore the warning by clicking the "advanced" button.

Google rightly considers even read-only access to a user's contacts information to be particularly sensitive,
so the verification process will almost certainly involve some extra steps and explanations.
If you choose to not do the validation process, 
your users will just continue to see the "not been verified by Google" warning.

If asked, the Google scopes to select are `email`, `profile`, `openid`, 
and the People API `contacts.readonly` scope.

## Test the _gcontact_ PHP script

Once your application is registered with Google
(even if verification is still pending) and you have done the PHP configuration,
you can test the _gcontact_ PHP script.

The script will allow you to go through the end-user interactive OAuth2 consent process with Google,
and it will let you manually download your contacts information into the database.
You can use `phpMyAdmin` or another database tool to verify that the records have been populated.

## _MyVirtualDirectory_ LDAP server

Download _MyVirtualDirectory_ from https://github.com/TremoloSecurity/MyVirtualDirectory.
_MyVirtualDirectory_ does not provide pre-built bundles, so you must build it from source code.
You can either clone the `git` repository or download the most recent sources from the releases page.
(I developed and tested with a clone of the `git` repository.
I don't know how that compares with the source code snapshots on the releases page.)

_MyVirtualDirectory_ is written in Java and structured as a `maven` project.
Once you have installed `maven`, 
the build is straightforward following the instructions provided by _MyVirtualDirectory_.
After building, you will find a directory with a name similar to
`MyVirtualDirectory/target/myvd-server-1.0.6-myvd/myvd-server-1.0.6/`.
You can move that innermost directory to a convenient permanent location.
 
You can sanity test your build of _MyVirtualDirectory_ using one of the sample
configurations provided with it.
I recommend doing that before applying _gcontact_ customizations.
If you use a high-numbered port (>1024) instead of the standard LDAP ports,
you can run _MyVirtualDirectory_ as an ordinary user.
If you use the standard LDAP ports (389 or 636),
then your operating system will probably require you to start _MyVirtualDirectory_
as a privileged user authorized to listen on those ports.

Start or stop _MyVirtualDirectory_ by using one of the scripts in 
its `bin/` directory; for example, `myvd.sh start` and `myvd.sh stop`.

NOTE: Logs for _MyVirtualDirectory_ will show an important-looking warning 
about the need to change the default admin password for _Apache Directory Server_.
The _MyVirtualDirectory_ author says it's safe to ignore that warning
(https://github.com/TremoloSecurity/MyVirtualDirectory/issues/80).

Apply the _gcontact_ customizations as follows:

* Copy, link, or symlink each of the JAR files from the _gcontact_ `lib/` directory
into the _MyVirtualDirectory_ `lib/` directory.
The _MyVirtualDirectory_ startup script will automatically find them.
* Copy, link, or symlink the JAR file from _WJC MyVD Custom Inserts_ into the
_MyVirtualDirectory_ `lib/` directory.
* The _gcontact_ `myvd/` directory contains a `myvd.conf` file with suitable entries.
Copy it to the _MyVirtualDirectory_ `conf/` directory.
You will need to modify it, but comments inside should lead you through it.
The `myvd.conf` file is read as a Java properties file,
so you must observe those formatting and syntax conventions.
If you are not already familiar with that, you can read about it here:
https://docs.oracle.com/javase/8/docs/api/java/util/Properties.html#load-java.io.Reader-

  - You will need to supply database credentials in a couple of places.
Those can be the same credentials, and they can be a read-only database user.
  - If you have used a different database name (not `gcontact`)
or if your database server host is not `localhost`,
you will have to modify the JDBC connection strings.
  - If you choose to use different LDAP naming contexts for
contacts, contact groups, or the bind user DN,
you will have to modify those items.
(The bind user DN is something you will probably want to modify,
but the others are mostly invisible to users after a one-time configuration step, 
so perhaps you will leave them as-is.)
  - Look for properties `server.DBgcontact.DBc.config.sqlX` and `server.DBgcontact.DBc.config.sqlY`.
Those are two different queries for approximately the same thing. 
Choose the one you want and remove the `X` or `Y` from the end of the name.
Comments near the queries can guide your decision.
One uses fully normalized tables and JOINs.
The other uses some redundantly stored (denormalized) attribute values.
The normalized form will generally be less performant than the denormalized form.
You can even craft your own query that is a blend of the two by JOINing 
more or fewer tables, using the existing queries as a guideline for how to do it.

Changes you make for LDAP configuration should also be reflected in the 
`$gcfConfig["ldap_client_config_info"]` array in the PHP script
(customized in `gcontact-auth.cfg`).
In particular, notice that the default information says that non-secure 
LDAP connections are not supported, but secure LDAP connections are. 

The sample `myvd.conf` provides authentication by parsing the calling user's
distinguished name and logging into an email server.
If that is appropriate for your environment, 
you merely need to supply the applicable configuration values.
If you need some other way to authenticate, you will have to provide it.
You may be able to use the _MyVirtualDirectory_ inserts that I use directly
or as a pattern for developing your own.
_MyVirtualDirectory_ custom inserts are written in Java.

## Test _MyVirtualDirectory_ LDAP

It's easy to make mistakes when editing the `myvd.conf` file.
Not every application that uses LDAP does a great job at reporting problems.
I suggest using a generic LDAP tool for doing initial testing.
If you don't already have a favorite, I suggest using _Apache Directory Studio_,
https://directory.apache.org/studio/,
which is simple to install and provides a very flexible
user interface for doing arbitrary LDAP operations.
(Unfortunately, it has bugs so that it doesn't deal well with `jpegPhoto` attributes.
Even if you save the binary data to a file, it will have been corrupted.)
Other interesting generic clients include JXplorer, http://www.jxplorer.org,
and _Jarek Gawor's LDAP Browser/Editor_, which, unforunately, 
you will have to find via a web search.
As a reminder, LDAP access here is read-only, 
so you can't update anything even if those clients imply that you can.

By default, _MyVirtualDirectory_ logs appear in its `logs/myvd.log` file.
Management of _MyVirtualDirectory_ logging is controlled via `conf/log4j2.xml`.
In the `myvd.conf` provided with _gcontact_, there is a line
`server.globalChain=LogAllTransactions`
and related configuration that dumps a lot of useful information into the log file for every operation.
Once you have things working properly, you will probably want to turn that off by
commenting it out.
You will find a commented out line
`server.globalChain=AccessLog`
that instead logs limited information about actual LDAP access.
You may wish to uncomment that line.
(If you want to use both of these sets of logging, then
instead uncomment the line
`server.globalChain=AccessLog,LogAllTransactions`.)

When you find a configuration problem, correct `myvd.conf`
and restart _MyVirtualDirectory_ to continue testing.

After testing to your satisfaction with a generic LDAP client,
you should try using a client that your users are likely to use.
Most applications that can use LDAP will ask for the following configuration details:

* LDAP server name and port number.
* Whether to use a secure connection.
They might call it "SSL", "TLS", "LDAPS", or something else entirely.
Of course, you can only use secure connections if you have configured it in
the _MyVirtualDirectory_ configuration.
* The base distinguished name relative to which all searches will be performed.
They might call it "Base DN", "Naming context", or something else entirely.
If you have not changed the namespaces for this in the `myvd.conf` provided with _gcontact_,
then you want to use these values:

  - `ou=contacts,o=gcontact` for contacts
  - `ou=contactgroups,o=gcontact` for contact groups

* Credentials for the calling user.
They might call this "Bind DN", "user ID", or something else entirely.
Following the example of the namespace for this in the `myvd.conf` provided with _gcontact_,
users would use the value
`mail=someuser@example.com,ou=users,dc=example,dc=com`
where `someuser@example.com` is the calling user's email address.
You have probably changed that configuration item, so adapt the above similarly.
You must also provide the password for that user, but the way of doing that
varies with the application.
Some will ask you to enter it in a form, 
and others will collect it interactively the first time you make an LDAP call.

Assuming good network connectivity 
(between the user and your server, and between your server and Google's servers) 
and a reasonable server,
LDAP searches should be pretty fast.
Most users will consider them to be instantaneous,
but there are a couple of exceptions.

* If the user has gone through the authorization process but not yet
fetched any contacts information, there can be a pause of a couple of seconds
while that fetching is done by the server.
The time is roughly proportional to the number of Google contacts the user has.
(Users can avoid this initial pause if they do the manual fetch via the _gcontact_ PHP script.)
* If contacts have already been fetched, 
but a certain amount of time has elapsed since that fetch,
then there can be a slight pause while the server refreshes any changes.
The default amount of time is 36 hours, but you can change that in the _gcontact_ configuration.
The refresh is incremental, so only changes are fetched.
That will usually make the pause much shorter than the initial full fetch.

## Final touches

When you are happy with your testing, it's time to finalize your setup.

* If you have not already done so, 
I strongly recommend that you configure _MyVirtualDirectory_ to use secure connections (port 636)
and disable the use of non-secure connections (port 389).
This is particularly important if you are sending plaintext passwords in your LDAP requests 
(the usual case).
Consult _MyVirtualDirectory_ documentation for how to configure this.
If you already have a security certificate for your web server
(and, if you don't, hurry over to https://letsencrypt.org/ and get one),
you can reuse it for your secure LDAP connection.
Do a web search for "pem to jks",
and you will find many descriptions of how to do the necessary conversion.
I use the following script (customize the first few variables to your environment).
I set its permissions to 700 and run it from a nightly cron job.
Since LetsEncrypt provides a new certificate about a month before the old one expires,
that's plenty of time to convert the new PEM to JKS file used by _MyVirtualDirectory_.
You have to restart _MyVirtualDirectory_ to pick up the new certificate.
```
#!/bin/bash

LEDIR=/etc/letsencrypt/live/example.com
GCDIR=/etc/gcontact
export PIN="somePEMpassword"
# the $POUT value is also needed in myvd.conf
export POUT="someJKSpassword"

LEPEM=$LEDIR/fullchain.pem
LEJKS=$GCDIR/letsencrypt.jks
TMPP12=/tmp/letsencrypt.$$.p12

if [ $LEJKS -nt $LEPEM ]; then exit 0; fi

(echo "$PIN"; echo "$POUT") |
    openssl pkcs12 -export \
        -passin stdin \
        -passout stdin \
        -inkey $LEDIR/privkey.pem \
        -in $LEPEM -name gcontact -out $TMPP12

if [ -f $LEJSK ]; then mv $LEJKS ${LEJKS}-OLD; fi

# $POUT will be temporarily visible in "ps" output on some systems
keytool -importkeystore \
    -srckeystore $TMPP12 \
    -srcstoretype pkcs12 \
    -srcstorepass:env POUT \
    -deststorepass:env POUT \
    -destkeystore $LEJKS

rm -f $TMPP12
exit 1
```
Here's what the nightly cron looks like:
```
/etc/gcontact/pem2jks.sh
if [ "$?" != "0" ]
then
    export MYVD_HOME=/usr/local/lib/myvd/myvd-server-1.0.6
    $MYVD_HOME/bin/myvd.sh stop
    $MYVD_HOME/bin/myvd.sh start
fi
```
* If you have been running _MyVirtualDirectory_ from a temporary location,
or if you have been accessing _gcontact_ from a temporary location,
move them to their permanent locations (of your choosing).
Adjust paths in configuration files accordingly.
Don't forget to adjust any symlinks to JAR files.
* For the user under which _MyVirtualDirectory_ will run, 
define the environment variable `MYVD_HOME` pointing to the _MyVirtualDirectory_ top level directory.
* _MyVirtualDirectory_ logs by default to `logs/myvd.log`.
You may wish to modify `conf/log4j2.xml` so that it logs to some central logging location.
* Here is a recap of the sensitive files mentioned earlier in this document.
Refer to the _Access control_ section above.
You will want to make sure that you have restricted access permissions to them.
It doesn't hurt to double-check at this point.

  - `gcontact.properties`. Should be readable only by the user (or group) under which PHP runs and the user (or group) under which _MyVirtualDirectory_ runs.
  - `gcontact-auth.cfg`. Should be readable only by the user (or group) under which PHP runs.
  - `myvd.conf`. Should be readable only by the user (or group) under which _MyVirtualDirectory_ runs.
  - The JSON file containing your Google client credentials should be readable only by the user (or group) under which PHP runs and the user (or group) under which _MyVirtualDirectory_ runs.
  - If you made a Java Keystore for use with secure LDAP connections, 
you will have protected it with a password that is then part of your `myvd.conf`. 
It's still a good idea to change permissions so that the Java Keystore file is only readble by the user (or group) under which _MyVirtualDirectory_ runs.

* Once started from the command line, 
_MyVirtualDirectory_ should continue to run indefinitely.
However, that won't help if your server is rebooted.
You will want to integrate it with your server start-up process so that
_MyVirtualDirectory_ starts automatically when the server starts.
_MyVirtualDirectory_ provides some scripts in its `bin/` directory to help with that.
* Make the _gcontact_ PHP script available to your users and advertise the feature.

As with any new application, you will want to keep a closer eye on its health
over the first few days as more and more users access it.
_MyVirtualDirectory_ and _gcontact_ logs will be good things to watch.
You can also use your database tools to peek into the `gcontact` database to look for anomalies.
Remember, you are looking at your users' private data, so your site's privacy policies apply.

## Usage notes

### Incident logging

Whenever any kind of exception happens, 
either in the PHP script or the Java application,
a unique incident ID is created and logged.
In general, users will not see details of whatever went wrong,
but they are likely to see a message mentioning the incident ID.
You can look for that incident ID in the logs to help you investigate.

The incident IDs for PHP and Java have slightly different formats,
but it's not significant.
Incident IDs are just more or less random strings to help you find things in the logs.
They have no intrinsic meaning.
They are not error codes or anything like that.
